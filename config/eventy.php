<?php


return [
    'base_url_api' => 'http://api.eventy.id/core/public/',
    'base_api_directory' => '../../api/core/public/',
    'no_image' => 'https://via.placeholder.com/400x400?text=no+image',
];