-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 14, 2019 at 11:58 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventy_splash_party`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`id`, `created_at`, `updated_at`, `deleted_at`, `date`, `time_start`, `time_end`, `title`, `location`, `description`) VALUES
(1, '2019-04-09 23:31:07', NULL, NULL, '2019-04-27', '06:00:00', '06:30:00', 'Breakfast', 'Bintang Kuta Hotel', ''),
(2, '2019-04-09 23:31:07', NULL, NULL, '2019-04-27', '06:30:00', '07:00:00', 'Prepare to the beach area', 'Bintang Kuta Hotel', ''),
(3, '2019-04-09 23:31:07', NULL, NULL, '2019-04-28', '06:00:00', '08:00:00', 'Breakfast', 'Discovery Kartika Plaza Hotel', ''),
(4, '2019-04-09 23:31:07', NULL, NULL, '2019-04-28', '08:00:00', '11:00:00', 'Free Time', 'Discovery Kartika Plaza Hotel', '');

-- --------------------------------------------------------

--
-- Table structure for table `agenda_document`
--

CREATE TABLE `agenda_document` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_agenda` int(11) DEFAULT NULL,
  `id_document` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `agenda_speaker`
--

CREATE TABLE `agenda_speaker` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_agenda` int(11) DEFAULT NULL,
  `id_speaker` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth`
--

INSERT INTO `auth` (`id`, `created_at`, `updated_at`, `deleted_at`, `type`, `value`) VALUES
(1, '2018-12-09 11:11:23', '2019-02-22 12:09:50', NULL, 'splashscreen', 'image/setting/splashscreen4.png'),
(2, '2018-12-09 11:11:23', '2019-02-22 12:09:16', NULL, 'logo', 'image/logo_blue.png'),
(3, '2018-12-09 11:11:23', NULL, NULL, 'background_color', '#0064D2'),
(4, '2018-12-09 11:11:23', NULL, NULL, 'button_color', '#FDAB03'),
(5, '2018-12-09 11:11:23', NULL, NULL, 'icon_color_primary', '#0064D2'),
(6, '2018-12-09 11:11:23', NULL, NULL, 'icon_color_secondary', '#FEDD00'),
(7, '2018-12-09 11:11:23', '2019-02-22 10:28:16', NULL, 'event_name', 'Splash Party');

-- --------------------------------------------------------

--
-- Table structure for table `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2019-02-22 03:21:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Setting', 'Route', 'AdminSettingControllerGetIndex', 'normal', 'fa fa-circle-o', 4, 1, 0, 1, 5, '2019-02-22 03:27:10', '2019-02-25 00:12:11'),
(2, 'Participant', 'Route', 'AdminParticipantControllerGetIndex', 'normal', 'fa fa-users', 16, 1, 0, 1, 1, '2019-02-22 04:02:33', '2019-02-25 00:07:46'),
(3, 'Banner', 'Route', 'AdminBannerControllerGetIndex', 'normal', 'fa fa-circle-o', 4, 1, 0, 1, 3, '2019-02-22 05:16:33', '2019-02-25 00:09:47'),
(4, 'Setting', 'URL', 'javascript:void(0)', 'normal', 'fa fa-gears', 0, 1, 0, 1, 10, '2019-02-22 05:17:08', '2019-02-25 00:08:36'),
(5, 'Document', 'Route', 'AdminDocumentControllerGetIndex', 'normal', 'fa fa-file-excel-o', 16, 1, 0, 1, 2, '2019-02-22 05:42:00', '2019-02-25 00:07:58'),
(6, 'Module', 'Route', 'AdminModuleControllerGetIndex', 'normal', 'fa fa-circle-o', 4, 1, 0, 1, 2, '2019-02-22 07:11:22', '2019-02-25 00:09:22'),
(7, 'Agenda', 'Route', 'AdminAgendaControllerGetIndex', 'normal', 'fa fa-calendar', 0, 1, 0, 1, 4, '2019-02-22 20:32:42', '2019-02-25 00:06:10'),
(8, 'Speaker', 'Route', 'AdminSpeakerControllerGetIndex', 'normal', 'fa fa-user-md', 16, 1, 0, 1, 3, '2019-02-22 21:00:52', '2019-02-25 00:08:14'),
(9, 'Survey', 'URL', 'javascript:void(0)', 'normal', 'fa fa-file-text-o', 0, 1, 0, 1, 7, '2019-02-22 21:15:08', '2019-02-25 00:06:47'),
(10, 'Survey Question', 'Route', 'AdminSurveyQuestionControllerGetIndex', 'normal', 'fa fa-circle-o', 9, 1, 0, 1, 2, '2019-02-22 21:16:55', '2019-02-25 00:07:10'),
(11, 'Survey Answer', 'Route', 'AdminSurveyAnswerControllerGetIndex', 'normal', 'fa fa-circle-o', 9, 1, 0, 1, 1, '2019-02-22 21:49:56', '2019-02-25 00:06:59'),
(13, 'QnA', 'Route', 'AdminQnaControllerGetIndex', 'normal', 'fa fa-question-circle', 0, 1, 0, 1, 5, '2019-02-23 02:13:46', '2019-02-25 00:06:25'),
(15, 'Feedback', 'Route', 'AdminFeedbackControllerGetIndex', 'normal', 'fa fa-star-o', 0, 1, 0, 1, 6, '2019-02-23 02:52:11', '2019-02-25 00:06:37'),
(16, 'Master Data', 'URL', 'javascript:void(0)', 'normal', 'fa fa-database', 0, 1, 0, 1, 9, '2019-02-23 02:52:40', '2019-02-25 00:07:26'),
(17, 'Index', 'Route', 'AdminIndexControllerGetIndex', 'normal', 'fa fa-dashboard', 0, 1, 1, 1, 1, '2019-02-23 03:09:29', '2019-02-25 00:04:43'),
(18, 'Notification', 'Route', 'AdminNotificationControllerGetIndex', 'normal', 'fa fa-bullhorn', 0, 1, 0, 1, 3, '2019-02-23 03:15:28', '2019-02-25 00:06:01'),
(19, 'Report', 'URL', 'javascript:void(0)', 'normal', 'fa fa-file-excel-o', 0, 1, 0, 1, 8, '2019-02-23 03:16:34', '2019-02-25 13:39:07'),
(20, 'Check In', 'Route', 'AdminCheckInControllerGetIndex', 'normal', 'fa fa-qrcode', 0, 1, 0, 1, 2, '2019-02-23 10:48:57', '2019-02-25 00:04:54'),
(21, 'Scan Type', 'Route', 'AdminScanTypeControllerGetIndex', 'normal', 'fa fa-circle-o', 4, 1, 0, 1, 1, '2019-02-24 13:35:41', '2019-02-25 00:08:49'),
(22, 'Preference', 'Route', 'AdminPreferenceControllerGetIndex', 'normal', 'fa fa-circle-o', 4, 1, 0, 1, 4, '2019-02-24 13:48:23', '2019-02-25 00:10:00'),
(23, 'Report Survey', 'Route', 'AdminReportSurveyControllerGetIndex', 'normal', 'fa fa-circle-o', 19, 1, 0, 1, 1, '2019-02-25 12:03:23', '2019-02-25 13:39:15'),
(24, 'Report Scan', 'Route', 'AdminReportScanControllerGetIndex', 'normal', 'fa fa-circle-o', 19, 1, 0, 1, 2, '2019-02-25 12:52:24', '2019-02-25 13:39:21'),
(25, 'Report Participant', 'Route', 'AdminReportParticipantControllerGetIndex', 'normal', 'fa fa-circle-o', 19, 1, 0, 1, 3, '2019-02-25 13:10:25', '2019-02-25 13:39:27');

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(18, 12, 1),
(21, 14, 1),
(34, 17, 2),
(35, 17, 1),
(36, 20, 2),
(37, 20, 1),
(38, 18, 2),
(39, 18, 1),
(40, 7, 2),
(41, 7, 1),
(42, 13, 2),
(43, 13, 1),
(44, 15, 2),
(45, 15, 1),
(46, 9, 2),
(47, 9, 1),
(48, 11, 2),
(49, 11, 1),
(50, 10, 2),
(51, 10, 1),
(52, 16, 2),
(53, 16, 1),
(54, 2, 2),
(55, 2, 1),
(56, 5, 2),
(57, 5, 1),
(58, 8, 2),
(59, 8, 1),
(60, 4, 2),
(61, 4, 1),
(62, 21, 2),
(63, 21, 1),
(64, 6, 2),
(65, 6, 1),
(66, 3, 2),
(67, 3, 1),
(68, 22, 2),
(69, 22, 1),
(70, 1, 2),
(71, 1, 1),
(75, 19, 2),
(76, 19, 1),
(77, 23, 2),
(78, 23, 1),
(79, 24, 2),
(80, 24, 1),
(81, 25, 2),
(82, 25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2019-02-22 03:21:17', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(12, 'Setting', 'fa fa-gear', 'setting', 'auth', 'AdminSettingController', 0, 0, '2019-02-22 03:27:09', NULL, NULL),
(13, 'Participant', 'fa fa-users', 'participant', 'member', 'AdminParticipantController', 0, 0, '2019-02-22 04:02:33', NULL, NULL),
(14, 'Banner', 'fa fa-image', 'banner', 'slider', 'AdminBannerController', 0, 0, '2019-02-22 05:16:33', NULL, NULL),
(15, 'Document', 'fa fa-file-excel-o', 'document', 'document', 'AdminDocumentController', 0, 0, '2019-02-22 05:42:00', NULL, NULL),
(16, 'Module', 'fa fa-th', 'module', 'module', 'AdminModuleController', 0, 0, '2019-02-22 07:11:17', NULL, NULL),
(17, 'Agenda', 'fa fa-calendar-o', 'agenda', 'agenda', 'AdminAgendaController', 0, 0, '2019-02-22 20:32:42', NULL, NULL),
(18, 'Speaker', 'fa fa-user-md', 'speaker', 'speaker', 'AdminSpeakerController', 0, 0, '2019-02-22 21:00:52', NULL, NULL),
(19, 'Survey Question', 'fa fa-file-text-o', 'survey_question', 'survey_question', 'AdminSurveyQuestionController', 0, 0, '2019-02-22 21:16:55', NULL, NULL),
(20, 'Survey Answer', 'fa fa-file-text-o', 'survey_answer', 'survey_answer', 'AdminSurveyAnswerController', 0, 0, '2019-02-22 21:49:56', NULL, NULL),
(21, 'QnA', 'fa fa-question-circle', 'question', 'question', 'AdminQuestionController', 0, 0, '2019-02-22 22:33:01', NULL, '2019-02-23 02:12:23'),
(22, 'QnA', 'fa fa-question-circle', 'qna', 'qna', 'AdminQnaController', 0, 0, '2019-02-23 02:13:46', NULL, NULL),
(23, 'QnA Detail', 'fa fa-question-circle', 'qna_detail', 'qna_detail', 'AdminQnaDetailController', 0, 0, '2019-02-23 02:15:36', NULL, '2019-02-23 02:17:08'),
(24, 'Feedback', 'fa fa-star-o', 'feedback', 'feedback', 'AdminFeedbackController', 0, 0, '2019-02-23 02:52:11', NULL, NULL),
(25, 'Index', 'fa fa-dashboard', 'index', 'cms_users', 'AdminIndexController', 0, 0, '2019-02-23 03:09:29', NULL, NULL),
(26, 'Notification', 'fa fa-bullhorn', 'notification', 'notification', 'AdminNotificationController', 0, 0, '2019-02-23 03:15:28', NULL, NULL),
(27, 'Check In', 'fa fa-qrcode', 'check-in', 'scan_member', 'AdminCheckInController', 0, 0, '2019-02-23 10:48:56', NULL, NULL),
(28, 'Scan Type', 'fa fa-gear', 'scan_type', 'scan_type', 'AdminScanTypeController', 0, 0, '2019-02-24 13:35:41', NULL, NULL),
(29, 'Preference', 'fa fa-gear', 'preference', 'setting', 'AdminPreferenceController', 0, 0, '2019-02-24 13:48:23', NULL, NULL),
(30, 'Report Survey', 'fa fa-file-excel-o', 'report-survey', 'survey_answer', 'AdminReportSurveyController', 0, 0, '2019-02-25 12:03:23', NULL, NULL),
(31, 'Report Scan', 'fa fa-file-excel-o', 'report-scan', 'scan_member', 'AdminReportScanController', 0, 0, '2019-02-25 12:52:24', NULL, NULL),
(32, 'Report Participant', 'fa fa-file-excel-o', 'report-participant', 'member', 'AdminReportParticipantController', 0, 0, '2019-02-25 13:10:24', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-black-light', '2019-02-22 03:21:17', NULL),
(2, 'Admin', 0, 'skin-black-light', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, 4, NULL, NULL),
(2, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(3, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(4, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(5, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(6, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(7, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(8, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(9, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(10, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(11, 1, 1, 1, 1, 1, 1, 21, NULL, NULL),
(12, 1, 1, 1, 1, 1, 1, 22, NULL, NULL),
(13, 1, 1, 1, 1, 1, 1, 23, NULL, NULL),
(14, 1, 1, 1, 1, 1, 1, 24, NULL, NULL),
(15, 1, 1, 1, 1, 1, 1, 25, NULL, NULL),
(16, 1, 1, 1, 1, 1, 1, 26, NULL, NULL),
(17, 1, 1, 1, 1, 1, 1, 27, NULL, NULL),
(18, 1, 1, 1, 1, 1, 1, 28, NULL, NULL),
(19, 1, 1, 1, 1, 1, 1, 29, NULL, NULL),
(20, 1, 1, 1, 1, 1, 2, 17, NULL, NULL),
(21, 1, 1, 1, 1, 1, 2, 14, NULL, NULL),
(22, 1, 1, 1, 1, 1, 2, 27, NULL, NULL),
(23, 1, 1, 1, 1, 1, 2, 15, NULL, NULL),
(24, 1, 1, 1, 1, 1, 2, 24, NULL, NULL),
(25, 1, 1, 1, 1, 1, 2, 25, NULL, NULL),
(26, 1, 1, 1, 1, 1, 2, 16, NULL, NULL),
(27, 1, 1, 1, 1, 1, 2, 26, NULL, NULL),
(28, 1, 1, 1, 1, 1, 2, 13, NULL, NULL),
(29, 1, 1, 1, 1, 1, 2, 29, NULL, NULL),
(30, 1, 1, 1, 1, 1, 2, 22, NULL, NULL),
(31, 1, 1, 1, 1, 1, 2, 32, NULL, NULL),
(32, 1, 1, 1, 1, 1, 2, 31, NULL, NULL),
(33, 1, 1, 1, 1, 1, 2, 30, NULL, NULL),
(34, 1, 1, 1, 1, 1, 2, 28, NULL, NULL),
(35, 1, 1, 1, 1, 1, 2, 12, NULL, NULL),
(36, 1, 1, 1, 1, 1, 2, 18, NULL, NULL),
(37, 1, 1, 1, 1, 1, 2, 20, NULL, NULL),
(38, 1, 1, 1, 1, 1, 2, 19, NULL, NULL),
(39, 1, 1, 1, 1, 1, 2, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2019-02-22 03:21:17', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2019-02-22 03:21:17', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', 'uploads/2019-02/30a24bc1995b7a9ac4aefd417ef14cdf.jpg', 'upload_image', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2019-02-22 03:21:17', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2019-02-22 03:21:17', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'Eventy', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2019-02-22 03:21:17', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', 'uploads/2019-02/eab35ea5b907418800b8e70827d193de.png', 'upload_image', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', 'uploads/2019-02/31b04f7307246848ed3cffc1b6e0ac46.jpg', 'upload_image', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', NULL, 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', 'AAAAo2UF2lY:APA91bFnvNWog4_hvRDQj83yGm89JrlMHbYgPsOG9RT6zpTcl2YIuqkymLJMd1G-y1RBSMP6nb8ujxe6NVAIL7oAZzByNMIDiYWal-ts9f-J6QPBKEKNcTZjxjxM1ECutvWTMZClNxp7', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', NULL, 'admin@crudbooster.com', '$2y$10$buPtGUCdsz0iZzGOhZveIOcLvoEuhzyb3ODlsEcte1lsnn.tzGP32', 1, '2019-02-22 03:21:17', NULL, 'Active'),
(2, 'Admin', 'uploads/1/2019-02/profile.jpg', 'admin@eventy.id', '$2y$10$XVSh6k8f0pQpssNSXsDTn.SsGxqg.HtuzS5Io15FRdJMHR7It2WF.', 2, '2019-02-25 00:11:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `committee`
--

CREATE TABLE `committee` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `job_title` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `committee`
--

INSERT INTO `committee` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`, `job_title`, `phone`, `email`, `sort`) VALUES
(1, '2019-04-03 00:29:11', NULL, NULL, 'Annastalia', 'Public Relation', '08123456789', 'annastalia@crocodic.com', 1),
(2, '2019-04-03 00:29:11', NULL, NULL, 'Putra Arin', 'Documentation', '08987654321', 'putra@crocodic.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `filesize` float DEFAULT NULL,
  `extension` varchar(5) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `created_at`, `updated_at`, `deleted_at`, `file`, `filename`, `filesize`, `extension`, `sort`) VALUES
(1, '2019-04-09 23:31:07', NULL, NULL, 'document/document1.docx', 'document1.docx', 34217, 'docx', NULL),
(2, '2019-04-09 23:31:07', NULL, NULL, 'document/document2.pdf', 'document2.pdf', 34217, 'pdf', NULL),
(3, '2019-04-09 23:31:07', NULL, NULL, 'document/document3.pptx', 'document3.pptx', 34217, 'pptx', NULL),
(4, '2019-04-09 23:31:07', NULL, NULL, 'document/document4.xlsx', 'document4.xlsx', 34217, 'xlxs', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `ratting` int(1) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `ratting`, `description`) VALUES
(1, '2019-04-09 10:38:01', NULL, NULL, 1, 5, 'Keren');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_date`
--

CREATE TABLE `gallery_date` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_date`
--

INSERT INTO `gallery_date` (`id`, `created_at`, `updated_at`, `deleted_at`, `date`) VALUES
(1, '2019-02-09 06:14:12', NULL, NULL, '2019-04-27'),
(2, '2019-02-09 06:14:12', NULL, NULL, '2019-04-28');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image`
--

CREATE TABLE `gallery_image` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_gallery_panel` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `type` varchar(5) DEFAULT NULL,
  `placeholder` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_image`
--

INSERT INTO `gallery_image` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_gallery_panel`, `file`, `filename`, `type`, `placeholder`) VALUES
(1, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival1.jpg', 'arrival1.jpg', 'image', 'image/setting/arrival/arrival1.jpg'),
(2, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival2.jpg', 'arrival2.jpg', 'image', 'image/setting/arrival/arrival2.jpg'),
(3, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival3.jpg', 'arrival3.jpg', 'image', 'image/setting/arrival/arrival3.jpg'),
(4, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival4.jpg', 'arrival4.jpg', 'image', 'image/setting/arrival/arrival4.jpg'),
(5, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival5.jpg', 'arrival5.jpg', 'image', 'image/setting/arrival/arrival5.jpg'),
(6, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival6.jpg', 'arrival6.jpg', 'image', 'image/setting/arrival/arrival6.jpg'),
(7, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival7.jpg', 'arrival7.jpg', 'image', 'image/setting/arrival/arrival7.jpg'),
(8, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival8.jpg', 'arrival8.jpg', 'image', 'image/setting/arrival/arrival8.jpg'),
(9, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival9.jpg', 'arrival9.jpg', 'image', 'image/setting/arrival/arrival9.jpg'),
(10, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival10.jpg', 'arrival10.jpg', 'image', 'image/setting/arrival/arrival10.jpg'),
(11, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner1.jpg', 'wellcoming_dinner1.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner1.jpg'),
(12, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner2.jpg', 'wellcoming_dinner2.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner2.jpg'),
(13, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner3.jpg', 'wellcoming_dinner3.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner3.jpg'),
(14, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner4.jpg', 'wellcoming_dinner4.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner4.jpg'),
(15, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner5.jpg', 'wellcoming_dinner5.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner5.jpg'),
(16, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner6.jpg', 'wellcoming_dinner6.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner6.jpg'),
(17, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner7.jpg', 'wellcoming_dinner7.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner7.jpg'),
(18, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner8.jpg', 'wellcoming_dinner8.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner8.jpg'),
(19, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner9.jpg', 'wellcoming_dinner9.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner9.jpg'),
(20, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner10.jpg', 'wellcoming_dinner10.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner10.jpg'),
(21, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference1.jpg', 'conference1.jpg', 'image', 'image/setting/conference/conference1.jpg'),
(22, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference2.jpg', 'conference2.jpg', 'image', 'image/setting/conference/conference2.jpg'),
(23, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference3.jpg', 'conference3.jpg', 'image', 'image/setting/conference/conference3.jpg'),
(24, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference4.jpg', 'conference4.jpg', 'image', 'image/setting/conference/conference4.jpg'),
(25, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference5.jpg', 'conference5.jpg', 'image', 'image/setting/conference/conference5.jpg'),
(26, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference6.jpg', 'conference6.jpg', 'image', 'image/setting/conference/conference6.jpg'),
(27, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference7.jpg', 'conference7.jpg', 'image', 'image/setting/conference/conference7.jpg'),
(28, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference8.jpg', 'conference8.jpg', 'image', 'image/setting/conference/conference8.jpg'),
(29, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference9.jpg', 'conference9.jpg', 'image', 'image/setting/conference/conference9.jpg'),
(30, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference10.jpg', 'conference10.jpg', 'image', 'image/setting/conference/conference10.jpg'),
(31, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night1.jpg', 'appreciation_night1.jpg', 'image', 'image/setting/appreciation_night/appreciation_night1.jpg'),
(32, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night2.jpg', 'appreciation_night2.jpg', 'image', 'image/setting/appreciation_night/appreciation_night2.jpg'),
(33, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night3.jpg', 'appreciation_night3.jpg', 'image', 'image/setting/appreciation_night/appreciation_night3.jpg'),
(34, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night4.jpg', 'appreciation_night4.jpg', 'image', 'image/setting/appreciation_night/appreciation_night4.jpg'),
(35, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night5.jpg', 'appreciation_night5.jpg', 'image', 'image/setting/appreciation_night/appreciation_night5.jpg'),
(36, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night6.jpg', 'appreciation_night6.jpg', 'image', 'image/setting/appreciation_night/appreciation_night6.jpg'),
(37, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night7.jpg', 'appreciation_night7.jpg', 'image', 'image/setting/appreciation_night/appreciation_night7.jpg'),
(38, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night8.jpg', 'appreciation_night8.jpg', 'image', 'image/setting/appreciation_night/appreciation_night8.jpg'),
(39, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night9.jpg', 'appreciation_night9.jpg', 'image', 'image/setting/appreciation_night/appreciation_night9.jpg'),
(40, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night10.jpg', 'appreciation_night10.jpg', 'image', 'image/setting/appreciation_night/appreciation_night10.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_panel`
--

CREATE TABLE `gallery_panel` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_gallery_date` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_panel`
--

INSERT INTO `gallery_panel` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_gallery_date`, `name`, `sort`) VALUES
(1, '2019-02-09 06:14:12', NULL, NULL, 1, 'Arrival', 1),
(2, '2019-02-09 06:14:12', NULL, NULL, 1, 'Wellcoming Dinner', 2),
(3, '2019-02-09 06:14:12', NULL, NULL, 2, 'Conference', 1),
(4, '2019-02-09 06:14:12', NULL, NULL, 2, 'Appreciation Night', 2);

-- --------------------------------------------------------

--
-- Table structure for table `maps`
--

CREATE TABLE `maps` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8mb4_unicode_ci,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `maps`
--

INSERT INTO `maps` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`, `image`, `location`, `latitude`, `longitude`) VALUES
(1, '2019-04-09 08:21:21', NULL, NULL, 'Bintang Kuta Hotel', 'image/maps/bintang_kuta_hotel.jpeg', 'Jalan Kartika Plaza, Kuta, Kabupaten Badung, Bali 80361', '-8.7337188', '115.1647533'),
(2, '2019-04-09 08:21:21', NULL, NULL, 'Pantai Bintang Kuta Hotel', 'image/maps/pantai_kuta.jpg', 'Jl. Pantai Kuta, Kuta, Kabupaten Badung, Bali 80361', '-8.7202445', '115.1669875'),
(3, '2019-04-09 08:21:21', NULL, NULL, 'Discovery Kartika Plaza Hotel', 'image/maps/kartika_plaza_hotel.jpg', 'Jl. Pantai Kuta, Kuta, Kabupaten Badung, Bali 80361', '-8.7295252', '115.1647909'),
(4, '2019-04-09 08:21:21', NULL, NULL, 'Wanaku Resto', 'image/maps/wanaku_resto.jpg', 'Jl. Kediri No.45A, Tuban, Kuta, Kabupaten Badung, Bali 80361', '-8.7386838', '115.1703691');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgot_password` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `created_at`, `updated_at`, `deleted_at`, `code`, `image`, `name`, `email`, `phone`, `password`, `company`, `departement`, `id_number`, `forgot_password`, `last_login`) VALUES
(1, '2018-08-25 06:03:15', '2019-04-14 21:13:47', NULL, 'EVENTYID', 'image/s1554799636.jpg', 'Demo Account', 'demo@eventy.id', '08123456789', '$2y$10$pXjiYGFOkqUlzQM3K03UFuLFnIhqmOPeANyX1xxRSEkX0rtsI.Ehu', 'Eventy', 'management', 'EVT-0001', NULL, '2019-04-14 21:13:47');

-- --------------------------------------------------------

--
-- Table structure for table `member_regid`
--

CREATE TABLE `member_regid` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member_regid`
--

INSERT INTO `member_regid` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `token`) VALUES
(1, '2019-04-09 09:39:19', NULL, NULL, 1, 'dBkLbrBHQbI:APA91bGvde0fn3G4bpaGww4PvnWwPpjksfsZ-xzT3ATTd7rrC5c9haJryqNBAj02mW2xSq5sVUb8JM3PsydGF6RPdWubMerCTTtEo33FXXOXd3KSirtnZULWlg1gnWaHEBTCqk7EWjsy'),
(2, '2019-04-09 09:39:37', '2019-04-09 17:46:48', NULL, 1, 'f5_nqbb7jRs:APA91bFD3EUvEh2VJD0usyh82xMo_MOzhgw2jI7UY_-kJ31gIgPfq2s12oHOXUFgpQgMA4FHotOB0ruEZ6cO1cc96ny9vto6LxFOR4l0b3yFfQdd0JbIQiBdX6kSwoptzgSKiXa42zHM'),
(3, '2019-04-09 09:39:46', NULL, NULL, 1, 'eRugs1qkz0g:APA91bFwpg3tfJWvLLa2pWqtKBNGwDrhfIniQUZ9C_YqkPtIOMTpGPcfWM5n6-shpN5WUInvo9McyrXKekgvwUoYVbluThhRCKBkDYl1bZhCpYrvqjsxihxaiEhNfF9nGpYt8xGNMfUz'),
(4, '2019-04-09 09:50:28', NULL, NULL, 1, 'fxxIYv9VuXg:APA91bG-HSpos1fwXFqTuMvahUaAxE4XdE1pA20vM4461CYryw890-M9TuGb3K9-mxn5fnC_svpk-5QjikUrhiaBr9yHbNucTZGln4GeTOy96fME46NVBDVkXXUw0yZ9SPMR9U-HKjW4'),
(5, '2019-04-09 10:04:06', NULL, NULL, 1, 'dmfyJb6MQbM:APA91bHubFMJqPckUzPtnIXxcUEh972zFTTB4tF6e4Qu4-M5WdZdb-yClwyszZeQ6JvxvnhT2bLgoSpDhz694rH7dMSGHj9mL0JB-t2shlkQBCWUcliOs1XsvYdOPqeQChWOVE0wuzgH'),
(6, '2019-04-09 10:07:42', NULL, NULL, 1, 'e-xMkaUr-Gs:APA91bE1tXfI_okf4HYNxv_Ao8x3Rp0Gz8quEHS2d-65LSAnuT3M42TyBXfLifH2VH3yLJBjv5xHz1j3bFbamIGj_YHZSNX0Xzxw4cw_cneoPniaQUIUi0KYPETgenqWIyEFBNm3sBns'),
(7, '2019-04-09 10:15:45', NULL, NULL, 1, 'cndX5k_a58k:APA91bGdM-O5Tz8LG_g30uVrMXhF1FM82ZQq-9ZqWan6w1k1_sAX229HfpqIQmlGlVM9aWx7FX3l11uj4DlqosoznPBG_fHzR4-I0-vaINek4MfEm9mdmVNbN_gIN-AwAeAS5TPOUFQt'),
(8, '2019-04-09 10:24:37', NULL, NULL, 1, 'dVOjMI4aXus:APA91bFyVyhAs9uc0TT84Q3jOKSMI-YgYnfdH_J_r2mtN0S-Md61merhtHGEnhYH0kaxnIUQDpKOqLCFXp6fS-W0OBfj5WvVz2Epx0rBR9i5MyOTmD7DONhXSKBS4N_PhjLj9LSPQDrr'),
(9, '2019-04-09 10:31:19', NULL, NULL, 1, 'ckswlg03bSI:APA91bEN6SAlgJ6e-pz0UldF9Hm53EqDw3zVbqR4s_nYeIn0GsAbLGpKuGp98xYEjC3mRfDnZuLj_RVaSbVzqmFd5goJu9AgtdUpf5Smz4CwBMLLnpjIbL8bNsc_v1us9FL2MX8TOKL-'),
(10, '2019-04-09 10:39:36', NULL, NULL, 1, 'c8W610r-z1s:APA91bF7U_ipta8Tdi5nRkw0VRG3mz0fVPRFe6DAasDSmfzMZSHFH_-z3nw7mVkUPrZXt0cyOB-ddcCFNo6gncFx8MiwKejnWbESfR2usAte4hwUomxrqkyY80I0TrGmP98VA4umf_sf'),
(11, '2019-04-09 12:07:30', NULL, NULL, 1, 'ffWvrxwIX-c:APA91bHFVVQSMrpanRSYsr4fu9b2_dGwUSBOTFqb8w39BB1Akhglcwi_ciXIlHTcodwweLjRg1JtGc_7s7Kd4Nxkdx6nIb1Z0fJ2uDmLIEVgnryX8gvHEEX8DQpx9WUoWqWCfm0_bPEx'),
(12, '2019-04-09 13:50:57', NULL, NULL, 1, 'fMeG4C2BI0s:APA91bFCnC12rGD5p3AFfmbp_gEe81-hm8lckTbG2HlV-ccTrU8vLc7kph0a1hQVorQO8S1eA_yuf_M9P-KdTpO6PZVPyl7LURvyMqjfXOmfeHJwe4Ok69UHZJraCL9Q89pItbL_KJJP'),
(13, '2019-04-09 16:19:09', NULL, NULL, 1, 'd9wLBxtzOcg:APA91bFZUCd_3espB_jzvv8DoGClEFhSm2oABn-4p4QI0Qu1w3m_5A1SnOzvOexjmPv0kFYBlGNv_Dj9jh4ywjE07Vf2dNW_srIcUCDcfd3eLqFRe1bDgS3fV4ykIyArEyxoM20jhQn4'),
(14, '2019-04-09 16:19:09', '2019-04-09 16:46:44', NULL, 1, 'cDn6yAz39J0:APA91bEQoCIgzS93_64aMl7uvenkyx6yV2_r5tk_zrN28i7kvEo0ap9x5omqruHORmIm_TW6OmhI5PjHTyonNuJMOk-pjoVSqlwjDdlqiaGftPWg4XJzJ6P5C4dvI-5rf4xWcc3m0qNt'),
(15, '2019-04-09 16:28:41', NULL, NULL, 1, 'eZ7hjwiNCgI:APA91bGUySkn7xfIrOwVnExva2SADs97KlmOx798i6bZMnjE2QhGbnuJkSo78HiC1FQ_wr5eXAihqR18Ee9HqzyYKkL9AHiXPlDe_OjjolVUt94ATaXQp7asM_UAu_H05e1DHJXOR7ua'),
(16, '2019-04-09 16:50:10', NULL, NULL, 1, 'cAbla-cBkOs:APA91bHLBDDxsqCW8bvlvg1CBnZdlY6vqfjXiNwnyRG48-GuJ57QVdh3MmLj5K1dRVFxXIU3_fK6K5vSJgb8hAujY9MwdwzoK923cw8ByLxFkOJ0484MOpKO8pZ7JjldTXfB03MpNcQv'),
(17, '2019-04-09 17:02:58', NULL, NULL, 1, 'dqm9FkBmIz8:APA91bEOlu3_XAMIFuMod1EXLf_2bdJY2fkXBVx9YDH8H7OVLiEeWcIIjbRz4WXxUltUktKZeBFEZENpKtNoJPW4Fh5WEn79lwsCtsJmRMFWsfPNSYfVCJAurkj1YT2-1m2mRiBhsWpE'),
(18, '2019-04-09 17:31:05', '2019-04-09 17:32:54', NULL, 1, 'dw9WgI0yZtM:APA91bGHXOe799jGPvmeXvLgvwKZvnZdkR1ZAd-qx0LK0lwRYNdgZc6oNFlVS_fKBT4Fvt5ZQlEc1Vn-jE0bIOO8eoHF8yO7Y9xHm3IUNzc2teKeJ8m2fmuoVYZO6fsjaXi7MpNOU6of'),
(19, '2019-04-09 22:32:20', NULL, NULL, 1, 'exr_kU0FSEc:APA91bHxb2qeH85hoLYL-DhSZYuAls7J00hm3pGy7teXbd45KBdq9mV9AWNMVMMLV9glDDvHY00oU7KNXapaA3bNGwpA6dtS6bhTMMyPI0Hbn6g7P9Phcs518MDqfSHgOFkOZjEeabpV'),
(20, '2019-04-10 12:12:10', NULL, NULL, 1, 'eV5vxXZLHXc:APA91bFhrgMLT3U5qExZQJ8V9AhYLNI_crAYO7w1GSLqX-82KoSp1TcYxU7y_08Y_CIemkk6NuC5HBBJeEyM3904b1uio32DhBBsoDfmsboP0hxsDFkI1m4suw4NnsDqwTjNzEIEcQhx'),
(21, '2019-04-10 12:26:22', NULL, NULL, 1, 'fbQTmtQTT8g:APA91bF-If3gVtpHc8F0RdKfUOMcWnEPpjBO1G2J0ABOyZVg3R8EJVKJaq3mmVO3Zuo57zZYCxIav9smPfi1WW8AAt0EbU51bDYTUztoB74Lo6L_ECkLfalIgkEVD-F0HVkm09M1qO36'),
(22, '2019-04-10 12:47:40', '2019-04-10 14:50:15', NULL, 1, 'dfSgW-8bmyI:APA91bGZQ4r4twXYgFfMQu2Y_NQNuQk7q6ShEUevGZx83q93LLYFdUiQNAtWWFQncOpacg6Nuhm5UHOqWz5y7YyHTPt5NB31HLgTD1ddY4QDFR2I1plilTY6oo34KhVeQ-GpssJdYhUv'),
(23, '2019-04-10 14:58:50', '2019-04-10 15:02:51', NULL, 1, 'fTAxQ-nhXDo:APA91bGcs5sa8a08HXPPNthOO_ftMORnrxajwz7rl8Sx59oF9R2tkbKli3dGOaTZRpfO0TrU20KQWlUDfJKu9TS7IC3Hzdj4k5bRVMXs3ugXAGN42jCQdgFPXLDZZ-k0UNpVrn81fC_P'),
(24, '2019-04-10 15:04:00', NULL, NULL, 1, 'fCAVhNvFEOM:APA91bE9rN8brpOfGgdX8N2gk3bAj8qNIA2sca0AckivQkYw0dq-yR_Eq3UX-2Wibn7JuOGYgJHoW_2aUi2e40yZexFxbqIJrIAjeqGKAcuscbN2x3KagBRDzvb576mec2hhlp0nxupQ'),
(25, '2019-04-10 15:26:58', NULL, NULL, 1, 'd60JZC-Eb50:APA91bF6bV3-Op1CyMKWmHsw9keemXX48inyrjntxvNhZ6q09dQQ2kglF9VkuBnw4Nl6V2pXXDEgp-MSgxZxpwqTy8EUkocDetDFv_b7BJYh5rh1S7StURfPhjEO03UJijUpdmVXSQe9'),
(26, '2019-04-10 16:05:17', '2019-04-10 16:05:47', NULL, 1, 'c52509zFz4M:APA91bGU2JZXtDqabD2E9UZpvlJbLwa-wZxr2coircizcZHfmu-ayOx1JRvDgnGlawMc1pnti6nEpuM_wq5xt6_zlErvgcuvCgWZD9AvLCdZ6R0sITJt6rU6D-slPzw9yzjqPULV35Ff'),
(27, '2019-04-10 16:06:36', NULL, NULL, 1, 'cE8PK7-9q6c:APA91bHgv1QxfIKIRotjLNObVk1TrSYNX0ozIeALYJ3DJIuic5u1pQvQN25X9rBG5pmfC8tggA5bOLXHt1FWDEZPAZdaDSfP2l4z0zTcwg_lesTjtzds4cmPLuUQEzW-aXRG6dwDA6Yz'),
(28, '2019-04-10 16:06:59', '2019-04-10 16:07:08', NULL, 1, 'cvWvm5QA0mw:APA91bFuqgvBXDR0H_9hTl3ofc6AjO0g3-ZABocHDgcLhmI_10zwXpgGJF9qfWb9aKq3SDPiWpAEh9K-p9Q8aki2WIg22C1aO9xJtuBFk2q2kRq_xeBPsRaNAtvx70XvcjLAxY7f5F1H'),
(29, '2019-04-10 16:08:44', NULL, NULL, 1, 'cC1W3yGh5xE:APA91bEX3MQHYh8gzPRAy2HEglZja4GmlDJzr8zKF_1BtWIO3FSmjG9FIuzcB7-pSOc8pQDynTVd5ulBbR-00-pIoH-8h7MiyuXTJNcGs3O7PEcmuf-cobsXe3Wy83Y_HAlIdST-JbBh'),
(30, '2019-04-10 16:12:17', NULL, NULL, 1, 'dPLYCvZJkd8:APA91bEoaepiijnwOCIY9UFfE3Lp2jQB6z8uG1ur33OJkPYbQqO2B82Xsas1kQXeiKujG7vVQaEWDBGGadbEr7DKLjLlrbfBzVLgEQM_8zcNPqsck-mnfkf7MwVkRdIk8gmNVULh2kvi'),
(31, '2019-04-10 16:38:23', NULL, NULL, 1, 'di_AcPduMMw:APA91bEZl9BHwaYa4-9sACLKAquNANWrx4t9GvHYK8B1dbtVK-EpduNKfmnvWz0NE_8rnLkqgndskSIuqLb45sYDGUzgnWmppEsWW9Bucn47ml_tEjAteWNe_WPK3RPmEZa-ZSkZBRsJ'),
(32, '2019-04-10 16:39:17', '2019-04-10 16:39:45', NULL, 1, 'cg5F54agclM:APA91bGL47njChmZxcKKnRZ1mnh_KPYFoXgcVS3n3BVpOUQp0X2BDkuNeYv2SOtoqFCLTsRfu7sfirNCsUAbndvVWY3VcpANki1COBsP76oAogl38v1CLlXf3sNmcuuJ1Dpc1Rv_zkz7'),
(33, '2019-04-11 09:10:01', '2019-04-12 17:01:17', NULL, 1, 'testing'),
(34, '2019-04-11 16:25:18', NULL, NULL, 1, 'fBSZcjXHKtA:APA91bGK7ajaRgwkN3o1tf6bExWSF25PSOLGUkAxcQLPvNA9dPupemL7Yo9-XdjtDP8bkBYupTDF59qA322m2UFK4HwBIxMcbTYe9BiZTfszXY0IjE5nD2ZkROxh1zf4hHcBc7rEyD6v'),
(35, '2019-04-12 10:34:14', '2019-04-12 10:50:37', NULL, 1, 'dKqN4xJI8aY:APA91bFEPRn62ulDMizpzDYOCNxXZh84YDmr1GUzNA1mdbWelrDLV01oVPYYWeBlJxj1YPwUG7fj8Z-3fZ-qDKDVBI6kHTocsuTyQQV7Wlobqymjm3L-yhs1FfH3uQpCP9mGaJtnRMRo'),
(36, '2019-04-12 10:51:02', '2019-04-12 16:01:48', NULL, 1, 'epuxgrUdHtg:APA91bHlgN9xvKwBROu3-22HnVxKVpj_d8htYfCbt_7doa7PlCV4ekH0JIq886IYAXJK3fI8sQl95UsZmZ3ALdETWvbgQF5moiZpwq45jRR6cpuK7g-TIjOC-l_rk6F-ZeGyUwZzL7LT'),
(37, '2019-04-14 21:13:47', NULL, NULL, 1, 'cF1iIim7vHU:APA91bHOssgywY5TSLRjGF6ZGmoTAErQvVY_gUVpn6aeT13iZY2KG5l1bMrs9OZcd0I3A90L109lL0AoUTW71WtYHwF80yEH2zBbNrN_rlJYvNqe19eLKvv60uJgvWeyl_dUsXPLmeVg');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `type` varchar(6) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `is_read` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `type`, `message`, `is_read`) VALUES
(0, '2019-03-29 19:38:31', NULL, NULL, 1, 'member', 'Sayur', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  `admin_status` varchar(3) DEFAULT 'Yes',
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `created_at`, `updated_at`, `deleted_at`, `type`, `sorting`, `code`, `name`, `status`, `admin_status`, `message`) VALUES
(1, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'forgot_password', 'FORGOT PASSWORD', 'Yes', 'Yes', NULL),
(2, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'notification', 'NOTIFICATION', 'Yes', 'Yes', NULL),
(3, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'edit_password', 'EDIT PASSWORD', 'Yes', 'Yes', NULL),
(4, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'scan_history', 'SCAN HISTORY', 'Yes', 'Yes', NULL),
(5, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'message_to_admin', 'MESSAGE TO ADMIN', 'No', 'Yes', NULL),
(6, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'profile', 'PROFILE', 'Yes', 'Yes', NULL),
(7, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'user_scan', 'SCAN QR CODE', 'Yes', 'No', NULL),
(101, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 1, 'qr_code', 'MY QR', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(102, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 2, 'agenda', 'AGENDA', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(103, '2018-12-09 14:18:17', NULL, NULL, 'modular', 4, 'maps', 'MAPS', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(104, '2018-12-09 14:18:17', NULL, NULL, 'modular', 5, 'gallery', 'GALLERY', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(105, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 6, 'document', 'SLIDES', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(106, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 10, 'survey', 'SURVEY', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(107, '2018-12-09 14:18:17', NULL, NULL, 'modular', 7, 'quiz', 'QUIZ', 'No', 'No', 'Sorry, you cannot access this right now'),
(108, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 9, 'feedback', 'FEEDBACK', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(109, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', NULL, 'speaker', 'MEMBER', 'Yes', 'No', 'Sorry, you cannot access this right now'),
(110, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 3, 'qna', 'QnA', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(111, '2018-12-09 14:18:17', NULL, NULL, 'modular', 12, 'help', 'HELP', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(112, '2018-12-09 14:18:17', NULL, NULL, 'modular', 11, 'about', 'ABOUT', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(113, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 8, 'souvenir', 'SOUVENIR', 'No', 'No', 'Sorry, you cannot access this right now');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `created_at`, `updated_at`, `deleted_at`, `title`, `content`) VALUES
(1, '2019-04-09 08:21:21', NULL, NULL, 'Welcome to Splash Party', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.');

-- --------------------------------------------------------

--
-- Table structure for table `notification_member`
--

CREATE TABLE `notification_member` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_notification` int(11) DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `action_type` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification_member`
--

INSERT INTO `notification_member` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_notification`, `id_member`, `action_type`) VALUES
(1, '2019-04-09 09:38:55', NULL, NULL, 1, 1, 'Read');

-- --------------------------------------------------------

--
-- Table structure for table `qna`
--

CREATE TABLE `qna` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_speaker` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qna`
--

INSERT INTO `qna` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `id_speaker`) VALUES
(1, '2019-04-09 09:11:40', '2019-04-09 09:11:40', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `qna_detail`
--

CREATE TABLE `qna_detail` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_qna` int(11) DEFAULT NULL,
  `message` text,
  `type` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qna_detail`
--

INSERT INTO `qna_detail` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_qna`, `message`, `type`) VALUES
(1, '2019-04-09 09:11:44', NULL, NULL, 1, 'testing', 'Member'),
(2, '2019-04-09 16:35:55', NULL, NULL, 1, 'hshhdd', 'Member'),
(3, '2019-04-09 16:36:37', NULL, NULL, 1, 'hi', 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answer`
--

CREATE TABLE `quiz_answer` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_quiz_question` int(11) DEFAULT NULL,
  `answer` varchar(1) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `datetime_start` varchar(50) DEFAULT NULL,
  `datetime_end` varchar(50) DEFAULT NULL,
  `second` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz_answer`
--

INSERT INTO `quiz_answer` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `id_quiz_question`, `answer`, `point`, `datetime_start`, `datetime_end`, `second`) VALUES
(1, '2019-04-04 09:40:08', NULL, NULL, 1, 1, 'A', 0, '2018-12-18 12:33:53.1231', '2018-12-18 12:34:10.1523', 17.0292),
(2, '2019-04-04 09:40:08', NULL, NULL, 1, 2, 'A', 10, '2018-12-18 12:35:53.1231', '2018-12-18 12:36:10.1523', 17.0292);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_question`
--

CREATE TABLE `quiz_question` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `option_a` varchar(50) DEFAULT NULL,
  `option_b` varchar(50) DEFAULT NULL,
  `option_c` varchar(50) DEFAULT NULL,
  `option_d` varchar(50) DEFAULT NULL,
  `answer` varchar(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scan_member`
--

CREATE TABLE `scan_member` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_scan_type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scan_member`
--

INSERT INTO `scan_member` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `id_scan_type`) VALUES
(1, '2019-04-09 22:07:56', NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `scan_type`
--

CREATE TABLE `scan_type` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scan_type`
--

INSERT INTO `scan_type` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`) VALUES
(1, '2019-02-24 23:31:07', NULL, NULL, 'Check In Gate - Day 1'),
(2, '2019-02-24 23:31:07', NULL, NULL, 'Check In Gate - Day 2');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `created_at`, `updated_at`, `deleted_at`, `type`, `value`) VALUES
(1, '2018-12-09 14:18:17', NULL, NULL, 'api-key', 'eventydemo'),
(2, '2018-12-09 14:18:17', NULL, NULL, 'profile_image', 'Yes'),
(3, '2018-12-09 14:18:17', NULL, NULL, 'profile_name', 'Yes'),
(4, '2018-12-09 14:18:17', NULL, NULL, 'profile_email', 'No'),
(5, '2018-12-09 14:18:17', NULL, NULL, 'profile_phone', 'Yes'),
(6, '2018-12-09 14:18:17', NULL, NULL, 'profile_company', 'Yes'),
(7, '2018-12-09 14:18:17', NULL, NULL, 'profile_department', 'Yes'),
(8, '2018-12-09 14:18:17', NULL, NULL, 'profile_id_number', 'Yes'),
(9, '2018-12-09 14:18:17', NULL, NULL, 'event_date_start', '2019-04-27'),
(10, '2018-12-09 14:18:17', NULL, NULL, 'event_date_finish', '2019-04-28'),
(11, '2018-12-09 14:18:17', NULL, NULL, 'about_event_image', 'image/setting/banner1.png'),
(12, '2018-12-09 14:18:17', NULL, NULL, 'about_event_content', '<h2>What is Eventy?</h2>\n\n<p>Eventy is SOLUSI for event organizers in helping to solve difficult problems MONITORING and MEASURING Corporate Event participants\' participation in real-time.</p>\n\n<p>DATA results of monitoring and measurement are used as INSIGHT by EO in making improvements so that the corporate event objectives can be achieved.</p>'),
(13, '2018-12-09 14:18:17', NULL, NULL, 'application_name', 'Splash Party'),
(14, '2018-12-09 14:18:17', NULL, NULL, 'application_date_start', '2019-02-24'),
(15, '2018-12-09 14:18:17', NULL, NULL, 'application_date_end', '2019-02-27'),
(16, '2018-12-09 14:18:17', NULL, NULL, 'feedback_ratting', 'What is the ratting of Eventy Application after your using this application?'),
(17, '2018-12-09 14:18:17', NULL, NULL, 'feedback_description', 'How do you feel after using Eventy Application?'),
(18, '2018-12-09 14:18:17', NULL, NULL, 'quiz_point', '10'),
(19, '2018-12-09 14:18:17', NULL, NULL, 'quiz_coundown', '30');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `created_at`, `updated_at`, `deleted_at`, `image`) VALUES
(1, '2018-12-10 09:22:50', NULL, NULL, 'image/setting/banner2.png');

-- --------------------------------------------------------

--
-- Table structure for table `speaker`
--

CREATE TABLE `speaker` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `speaker`
--

INSERT INTO `speaker` (`id`, `created_at`, `updated_at`, `deleted_at`, `image`, `name`, `title`, `description`) VALUES
(1, '2019-02-23 04:11:48', '2019-02-23 09:14:18', NULL, 'uploads/1/2019-02/fav_icon.jpg', 'Admin', 'Public Relation', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `survey_answer`
--

CREATE TABLE `survey_answer` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_survey_question` int(11) DEFAULT NULL,
  `id_survey_option` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `survey_answer`
--

INSERT INTO `survey_answer` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `id_survey_question`, `id_survey_option`) VALUES
(1, '2019-04-09 08:04:04', NULL, NULL, 1, 1, 1),
(2, '2019-04-09 16:42:51', NULL, NULL, 1, 2, 7);

-- --------------------------------------------------------

--
-- Table structure for table `survey_option`
--

CREATE TABLE `survey_option` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_survey_question` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `survey_option`
--

INSERT INTO `survey_option` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_survey_question`, `value`, `sort`) VALUES
(1, NULL, NULL, NULL, 1, 'Answer 1.1', 1),
(2, NULL, NULL, NULL, 1, 'Answer 1.2', 2),
(3, NULL, NULL, NULL, 1, 'Answer 1.3', 3),
(4, NULL, NULL, NULL, 1, 'Answer 1.4', 4),
(5, NULL, NULL, NULL, 2, 'Answer 2.1', 1),
(6, NULL, NULL, NULL, 2, 'Answer 2.2', 2),
(7, NULL, NULL, NULL, 2, 'Answer 2.3', 3),
(8, NULL, NULL, NULL, 2, 'Answer 2.4', 4);

-- --------------------------------------------------------

--
-- Table structure for table `survey_question`
--

CREATE TABLE `survey_question` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `open` varchar(3) DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `survey_question`
--

INSERT INTO `survey_question` (`id`, `created_at`, `updated_at`, `deleted_at`, `value`, `sort`, `open`) VALUES
(1, '2019-04-09 11:00:39', '2019-04-09 11:05:30', NULL, 'Question 1', 1, 'No'),
(2, '2019-04-09 19:43:13', '2019-04-03 11:24:25', NULL, 'Question 2', 2, 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agenda_document`
--
ALTER TABLE `agenda_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agenda_speaker`
--
ALTER TABLE `agenda_speaker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `committee`
--
ALTER TABLE `committee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_date`
--
ALTER TABLE `gallery_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_image`
--
ALTER TABLE `gallery_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_panel`
--
ALTER TABLE `gallery_panel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maps`
--
ALTER TABLE `maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_regid`
--
ALTER TABLE `member_regid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `is_read` (`is_read`),
  ADD KEY `id_member` (`id_member`),
  ADD KEY `id_member_2` (`id_member`,`type`),
  ADD KEY `id_member_3` (`id_member`,`type`,`is_read`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deleted_at` (`deleted_at`,`status`,`admin_status`),
  ADD KEY `deleted_at_2` (`deleted_at`),
  ADD KEY `status` (`status`),
  ADD KEY `admin_status` (`admin_status`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `notification_member`
--
ALTER TABLE `notification_member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_member` (`id_member`),
  ADD KEY `id_notification` (`id_notification`),
  ADD KEY `id_notification_2` (`id_notification`,`id_member`);

--
-- Indexes for table `qna`
--
ALTER TABLE `qna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qna_detail`
--
ALTER TABLE `qna_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_question`
--
ALTER TABLE `quiz_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scan_member`
--
ALTER TABLE `scan_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scan_type`
--
ALTER TABLE `scan_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `speaker`
--
ALTER TABLE `speaker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey_answer`
--
ALTER TABLE `survey_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey_option`
--
ALTER TABLE `survey_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey_question`
--
ALTER TABLE `survey_question`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `agenda_document`
--
ALTER TABLE `agenda_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `agenda_speaker`
--
ALTER TABLE `agenda_speaker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `committee`
--
ALTER TABLE `committee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gallery_date`
--
ALTER TABLE `gallery_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `gallery_image`
--
ALTER TABLE `gallery_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `gallery_panel`
--
ALTER TABLE `gallery_panel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `maps`
--
ALTER TABLE `maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member_regid`
--
ALTER TABLE `member_regid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notification_member`
--
ALTER TABLE `notification_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `qna`
--
ALTER TABLE `qna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `qna_detail`
--
ALTER TABLE `qna_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `quiz_question`
--
ALTER TABLE `quiz_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scan_member`
--
ALTER TABLE `scan_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `scan_type`
--
ALTER TABLE `scan_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `speaker`
--
ALTER TABLE `speaker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `survey_answer`
--
ALTER TABLE `survey_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `survey_option`
--
ALTER TABLE `survey_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `survey_question`
--
ALTER TABLE `survey_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
