-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 14, 2019 at 11:56 PM
-- Server version: 5.6.42
-- PHP Version: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventy_splash_party`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`id`, `created_at`, `updated_at`, `deleted_at`, `date`, `time_start`, `time_end`, `title`, `location`, `description`) VALUES
(1, '2019-04-09 23:31:07', '2019-04-14 15:35:01', NULL, '2019-04-27', '06:00:00', '06:30:00', 'Breakfast', 'Bintang Kuta Hotel', 'Enjoy Kuta, Bali premier location accompanied by our new modern minimalist architecture and comfortable interior design. We have 158 Deluxe Rooms a 32 sqm, which feature a King Size or Twin Bed with the option of a rollaway bed.\r\n\r\nBintang Kuta Hotel has 4 Suite Rooms a 64 sqm. Featuring with a king sized bed with a luxurious living room as well as full entertainment system to create unforgettable memories for you and your family. And also, we have 6 Family Rooms a 40sqm, which feature a king sized bed and twin bed that are comfortable and convenient for your entire family.\r\n\r\nNever leave your family behind when you come to Bali. Bintang Kuta Hotel proudly welcomes you and your family.'),
(2, '2019-04-09 23:31:07', '2019-04-14 15:20:17', NULL, '2019-04-27', '06:30:00', '07:00:00', 'Prepare to the beach area', 'Bintang Kuta Hotel', 'Jalan Kartika Plaza, Kuta, Kuta, Badung, Bali, Indonesia, 80361'),
(3, '2019-04-09 23:31:07', NULL, NULL, '2019-04-28', '06:00:00', '08:00:00', 'Breakfast', 'Discovery Kartika Plaza Hotel', ''),
(4, '2019-04-09 23:31:07', NULL, NULL, '2019-04-28', '08:00:00', '11:00:00', 'Free Time', 'Discovery Kartika Plaza Hotel', '');

-- --------------------------------------------------------

--
-- Table structure for table `agenda_document`
--

CREATE TABLE `agenda_document` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_agenda` int(11) DEFAULT NULL,
  `id_document` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `agenda_document`
--

INSERT INTO `agenda_document` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_agenda`, `id_document`) VALUES
(1, NULL, NULL, NULL, 2, 1),
(2, NULL, NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `agenda_speaker`
--

CREATE TABLE `agenda_speaker` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_agenda` int(11) DEFAULT NULL,
  `id_speaker` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `agenda_speaker`
--

INSERT INTO `agenda_speaker` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_agenda`, `id_speaker`) VALUES
(1, NULL, NULL, NULL, 2, 1),
(2, NULL, NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth`
--

INSERT INTO `auth` (`id`, `created_at`, `updated_at`, `deleted_at`, `type`, `value`) VALUES
(1, '2018-12-09 11:11:23', '2019-02-22 12:09:50', NULL, 'splashscreen', 'image/setting/splashscreen4.png'),
(2, '2018-12-09 11:11:23', '2019-02-22 12:09:16', NULL, 'logo', 'image/logo_blue.png'),
(3, '2018-12-09 11:11:23', NULL, NULL, 'background_color', '#0064D2'),
(4, '2018-12-09 11:11:23', NULL, NULL, 'button_color', '#FDAB03'),
(5, '2018-12-09 11:11:23', NULL, NULL, 'icon_color_primary', '#0064D2'),
(6, '2018-12-09 11:11:23', NULL, NULL, 'icon_color_secondary', '#FEDD00'),
(7, '2018-12-09 11:11:23', '2019-02-22 10:28:16', NULL, 'event_name', 'Splash Party');

-- --------------------------------------------------------

--
-- Table structure for table `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2019-02-22 03:21:17', NULL),
(2, 'Email Template Forgot Password Apps', 'forgot_password_apps', 'Forgot Password Splash Party', '<h4><font face=\"Arial\"><b>Hello!</b></font></h4>\r\n<p><font face=\"Arial\">You are receiving this email because we received a password request for your account.\r\nPlease login with your new password in app.</font></p>\r\n<p><font face=\"Arial\"><br></font></p>\r\n<p style=\"text-align: center;\"><font face=\"Arial\">\r\n</font>\r\n<span style=\"\r\nbackground-color: #3c8dbc;\r\ncursor: default;\r\ncolor: white;\r\npadding: 15px 32px;\r\ntext-align: center;\r\nfont-size: 16px;\r\ntext-decoration: none;\r\ndisplay: inline-block;\r\nmargin: 4px 2px;\">\r\n<font face=\"Arial\">[password]\r\n</font></span>\r\n<font face=\"Arial\"><font face=\"Arial\">\r\n</font></font></p><font face=\"Arial\">\r\n<p><font face=\"Arial\"><br></font></p>\r\n<p><font face=\"Arial\">If you did not request a password reset, no further action is request.</font>\r\n<br><br><font face=\"Arial\">Regards,</font>\r\n<br><font face=\"Arial\">Admin</font></p></font>', '[password]', 'Splash Party', 'system@mail.eventy.id', NULL, '2019-02-22 03:21:17', '2019-04-14 05:05:09');

-- --------------------------------------------------------

--
-- Table structure for table `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'http://eventy_splash.test/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-04-12 06:31:34', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'http://eventy_splash.test/admin/logout', 'admin@crudbooster.com logout', '', 1, '2019-04-12 06:42:20', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'http://eventy_splash.test/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-04-12 06:42:30', NULL),
(4, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'http://eventy_splash.test/admin/logout', 'admin@crudbooster.com logout', '', 1, '2019-04-12 06:43:35', NULL),
(5, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'http://eventy_splash.test/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-04-12 06:44:39', NULL),
(6, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-04-14 03:25:53', NULL),
(7, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/email_templates/edit-save/2', 'Update data Email Template Forgot Password Apps at Email Templates', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>subject</td><td></td><td>Forgot Password Splash Party</td></tr><tr><td>from_name</td><td>System</td><td>Splash Party</td></tr><tr><td>from_email</td><td>system@crudbooster.com</td><td>system@mail.eventy.id</td></tr><tr><td>cc_email</td><td></td><td></td></tr></tbody></table>', 1, '2019-04-14 04:45:52', NULL),
(8, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/email_templates/edit-save/2', 'Update data Email Template Forgot Password Apps at Email Templates', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p></td><td><h4><font face=\"Arial\"><b>Hello!</b></font></h4>\r\n        <p><font face=\"Arial\">You are receiving this email because we received a password request for your account.\r\n                Please login with your new password in app.</font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p style=\"text-align: center;\"><font face=\"Arial\">\r\n                <button style=\"background-color: #3c8dbc; /* Green */\r\n                  border: none;\r\n                  color: white;\r\n                  padding: 15px 32px;\r\n                  text-align: center;\r\n                  text-decoration: none;\r\n                  display: inline-block;\r\n                  margin: 4px 2px;\r\n                  cursor: pointer;\">[password]\r\n                </button>\r\n            </font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p><font face=\"Arial\">If you did not request a password reset, no further action is request.</font></p>\r\n        <p><font face=\"Arial\">Regards,</font></p>\r\n        <p><font face=\"Arial\">Admin</font></p></td></tr><tr><td>cc_email</td><td></td><td></td></tr></tbody></table>', 1, '2019-04-14 04:56:37', NULL),
(9, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/email_templates/edit-save/2', 'Update data Email Template Forgot Password Apps at Email Templates', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>cc_email</td><td></td><td></td></tr></tbody></table>', 1, '2019-04-14 04:57:22', NULL),
(10, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/email_templates/edit-save/2', 'Update data Email Template Forgot Password Apps at Email Templates', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><h4><font face=\"Arial\"><b>Hello!</b></font></h4>\r\n        <p><font face=\"Arial\">You are receiving this email because we received a password request for your account.\r\n                Please login with your new password in app.</font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p style=\"text-align: center;\"><font face=\"Arial\">\r\n                <button style=\"background-color: #3c8dbc; /* Green */\r\n                  border: none;\r\n                  color: white;\r\n                  padding: 15px 32px;\r\n                  text-align: center;\r\n                  text-decoration: none;\r\n                  display: inline-block;\r\n                  margin: 4px 2px;\r\n                  cursor: pointer;\">[password]\r\n                </button>\r\n            </font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p><font face=\"Arial\">If you did not request a password reset, no further action is request.</font></p>\r\n        <p><font face=\"Arial\">Regards,</font></p>\r\n        <p><font face=\"Arial\">Admin</font></p></td><td><h4><font face=\"Arial\"><b>Hello!</b></font></h4>\r\n        <p><font face=\"Arial\">You are receiving this email because we received a password request for your account.\r\n                Please login with your new password in app.</font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p style=\"text-align: center;\"><font face=\"Arial\">\r\n                <button type=\"button\" style=\"background-color: #3c8dbc; /* Green */\r\n                  border: none;\r\n                  color: white;\r\n                  padding: 15px 32px;\r\n                  text-align: center;\r\n                  font-size: 16px;\r\n                  text-decoration: none;\r\n                  display: inline-block;\r\n                  margin: 4px 2px;\r\n                  cursor: pointer;\">[password]\r\n                </button>\r\n            </font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p><font face=\"Arial\">If you did not request a password reset, no further action is request.</font></p>\r\n        <p><font face=\"Arial\">Regards,</font></p>\r\n        <p><font face=\"Arial\">Admin</font></p></td></tr><tr><td>cc_email</td><td></td><td></td></tr></tbody></table>', 1, '2019-04-14 04:58:03', NULL),
(11, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/email_templates/edit-save/2', 'Update data Email Template Forgot Password Apps at Email Templates', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><h4><font face=\"Arial\"><b>Hello!</b></font></h4>\r\n        <p><font face=\"Arial\">You are receiving this email because we received a password request for your account.\r\n                Please login with your new password in app.</font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p style=\"text-align: center;\"><font face=\"Arial\">\r\n                <button type=\"button\" style=\"background-color: #3c8dbc; /* Green */\r\n                  border: none;\r\n                  color: white;\r\n                  padding: 15px 32px;\r\n                  text-align: center;\r\n                  font-size: 16px;\r\n                  text-decoration: none;\r\n                  display: inline-block;\r\n                  margin: 4px 2px;\r\n                  cursor: pointer;\">[password]\r\n                </button>\r\n            </font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p><font face=\"Arial\">If you did not request a password reset, no further action is request.</font></p>\r\n        <p><font face=\"Arial\">Regards,</font></p>\r\n        <p><font face=\"Arial\">Admin</font></p></td><td><h4><font face=\"Arial\"><b>Hello!</b></font></h4>\r\n        <p><font face=\"Arial\">You are receiving this email because we received a password request for your account.\r\n                Please login with your new password in app.</font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p style=\"text-align: center;\"><font face=\"Arial\">\r\n                <button type=\"button\" style=\"background-color: #3c8dbc; /* Green */\r\n                  border: none;\r\n                  color: white;\r\n                  padding: 15px 32px;\r\n                  text-align: center;\r\n                  font-size: 16px;\r\n                  text-decoration: none;\r\n                  display: inline-block;\r\n                  margin: 4px 2px;\r\n                  cursor: pointer;\">[password]\r\n                </button>\r\n            </font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p><font face=\"Arial\">If you did not request a password reset, no further action is request.</font>\r\n<br><br><font face=\"Arial\">Regards,</font>\r\n<br><font face=\"Arial\">Admin</font></p></td></tr><tr><td>cc_email</td><td></td><td></td></tr></tbody></table>', 1, '2019-04-14 04:59:28', NULL),
(12, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/email_templates/edit-save/2', 'Update data Email Template Forgot Password Apps at Email Templates', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><h4><font face=\"Arial\"><b>Hello!</b></font></h4>\r\n        <p><font face=\"Arial\">You are receiving this email because we received a password request for your account.\r\n                Please login with your new password in app.</font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p style=\"text-align: center;\"><font face=\"Arial\">\r\n                <button type=\"button\" style=\"background-color: #3c8dbc; /* Green */\r\n                  border: none;\r\n                  color: white;\r\n                  padding: 15px 32px;\r\n                  text-align: center;\r\n                  font-size: 16px;\r\n                  text-decoration: none;\r\n                  display: inline-block;\r\n                  margin: 4px 2px;\r\n                  cursor: pointer;\">[password]\r\n                </button>\r\n            </font></p>\r\n        <p><font face=\"Arial\"><br></font></p>\r\n        <p><font face=\"Arial\">If you did not request a password reset, no further action is request.</font>\r\n<br><br><font face=\"Arial\">Regards,</font>\r\n<br><font face=\"Arial\">Admin</font></p></td><td><h4><font face=\"Arial\"><b>Hello!</b></font></h4>\r\n<p><font face=\"Arial\">You are receiving this email because we received a password request for your account.\r\nPlease login with your new password in app.</font></p>\r\n<p><font face=\"Arial\"><br></font></p>\r\n<p style=\"text-align: center;\"><font face=\"Arial\">\r\n</font>\r\n<span type=\"button\" style=\"\r\nbackground-color: #3c8dbc;\r\ncursor: default;\r\ncolor: white;\r\npadding: 15px 32px;\r\ntext-align: center;\r\nfont-size: 16px;\r\ntext-decoration: none;\r\ndisplay: inline-block;\r\nmargin: 4px 2px;\r\ncursor: pointer;\">\r\n<font face=\"Arial\">[password]\r\n</font></span>\r\n<font face=\"Arial\"><font face=\"Arial\">\r\n</font></font></p><font face=\"Arial\">\r\n<p><font face=\"Arial\"><br></font></p>\r\n<p><font face=\"Arial\">If you did not request a password reset, no further action is request.</font>\r\n<br><br><font face=\"Arial\">Regards,</font>\r\n<br><font face=\"Arial\">Admin</font></p></font></td></tr><tr><td>cc_email</td><td></td><td></td></tr></tbody></table>', 1, '2019-04-14 05:04:04', NULL),
(13, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/email_templates/edit-save/2', 'Update data Email Template Forgot Password Apps at Email Templates', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>content</td><td><h4><font face=\"Arial\"><b>Hello!</b></font></h4>\r\n<p><font face=\"Arial\">You are receiving this email because we received a password request for your account.\r\nPlease login with your new password in app.</font></p>\r\n<p><font face=\"Arial\"><br></font></p>\r\n<p style=\"text-align: center;\"><font face=\"Arial\">\r\n</font>\r\n<span type=\"button\" style=\"\r\nbackground-color: #3c8dbc;\r\ncursor: default;\r\ncolor: white;\r\npadding: 15px 32px;\r\ntext-align: center;\r\nfont-size: 16px;\r\ntext-decoration: none;\r\ndisplay: inline-block;\r\nmargin: 4px 2px;\r\ncursor: pointer;\">\r\n<font face=\"Arial\">[password]\r\n</font></span>\r\n<font face=\"Arial\"><font face=\"Arial\">\r\n</font></font></p><font face=\"Arial\">\r\n<p><font face=\"Arial\"><br></font></p>\r\n<p><font face=\"Arial\">If you did not request a password reset, no further action is request.</font>\r\n<br><br><font face=\"Arial\">Regards,</font>\r\n<br><font face=\"Arial\">Admin</font></p></font></td><td><h4><font face=\"Arial\"><b>Hello!</b></font></h4>\r\n<p><font face=\"Arial\">You are receiving this email because we received a password request for your account.\r\nPlease login with your new password in app.</font></p>\r\n<p><font face=\"Arial\"><br></font></p>\r\n<p style=\"text-align: center;\"><font face=\"Arial\">\r\n</font>\r\n<span style=\"\r\nbackground-color: #3c8dbc;\r\ncursor: default;\r\ncolor: white;\r\npadding: 15px 32px;\r\ntext-align: center;\r\nfont-size: 16px;\r\ntext-decoration: none;\r\ndisplay: inline-block;\r\nmargin: 4px 2px;\">\r\n<font face=\"Arial\">[password]\r\n</font></span>\r\n<font face=\"Arial\"><font face=\"Arial\">\r\n</font></font></p><font face=\"Arial\">\r\n<p><font face=\"Arial\"><br></font></p>\r\n<p><font face=\"Arial\">If you did not request a password reset, no further action is request.</font>\r\n<br><br><font face=\"Arial\">Regards,</font>\r\n<br><font face=\"Arial\">Admin</font></p></font></td></tr><tr><td>cc_email</td><td></td><td></td></tr></tbody></table>', 1, '2019-04-14 05:05:09', NULL),
(14, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/agenda/edit-save/2', 'Update data Prepare to the beach area at Agenda', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>deleted_at</td><td></td><td></td></tr><tr><td>description</td><td></td><td>Jalan Kartika Plaza, Kuta, Kuta, Badung, Bali, Indonesia, 80361</td></tr></tbody></table>', 1, '2019-04-14 08:20:17', NULL),
(15, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/agenda/edit-save/1', 'Update data Breakfast at Agenda', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>deleted_at</td><td></td><td></td></tr><tr><td>description</td><td></td><td></td></tr></tbody></table>', 1, '2019-04-14 08:20:40', NULL),
(16, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/agenda/edit-save/1', 'Update data Breakfast at Agenda', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>deleted_at</td><td></td><td></td></tr><tr><td>description</td><td></td><td></td></tr></tbody></table>', 1, '2019-04-14 08:21:09', NULL),
(17, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/speaker/delete-image', 'Delete the image of Admin at Speaker', '', 1, '2019-04-14 08:24:35', NULL),
(18, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/speaker/edit-save/1', 'Update data Admin at Speaker', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-04-14 08:24:46', NULL),
(19, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/speaker/edit-save/1', 'Update data Admin at Speaker', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>deleted_at</td><td></td><td></td></tr><tr><td>image</td><td></td><td>uploads/1/2019-04/putra.png</td></tr></tbody></table>', 1, '2019-04-14 08:25:25', NULL),
(20, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/agenda/edit-save/1', 'Update data Breakfast at Agenda', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>deleted_at</td><td></td><td></td></tr><tr><td>description</td><td></td><td>Enjoy Kuta, Bali premier location accompanied by our new modern minimalist architecture and comfortable interior design. We have 158 Deluxe Rooms a 32 sqm, which feature a King Size or Twin Bed with the option of a rollaway bed.\r\n\r\nBintang Kuta Hotel has 4 Suite Rooms a 64 sqm. Featuring with a king sized bed with a luxurious living room as well as full entertainment system to create unforgettable memories for you and your family. And also, we have 6 Family Rooms a 40sqm, which feature a king sized bed and twin bed that are comfortable and convenient for your entire family.\r\n\r\nNever leave your family behind when you come to Bali. Bintang Kuta Hotel proudly welcomes you and your family.</td></tr></tbody></table>', 1, '2019-04-14 08:35:01', NULL),
(21, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://eventy_splash.test/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-04-14 18:26:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Setting', 'Route', 'AdminSettingControllerGetIndex', 'normal', 'fa fa-circle-o', 4, 1, 0, 1, 5, '2019-02-22 03:27:10', '2019-02-25 00:12:11'),
(2, 'Participant', 'Route', 'AdminParticipantControllerGetIndex', 'normal', 'fa fa-users', 16, 1, 0, 1, 1, '2019-02-22 04:02:33', '2019-02-25 00:07:46'),
(3, 'Banner', 'Route', 'AdminBannerControllerGetIndex', 'normal', 'fa fa-circle-o', 4, 1, 0, 1, 3, '2019-02-22 05:16:33', '2019-02-25 00:09:47'),
(4, 'Setting', 'URL', 'javascript:void(0)', 'normal', 'fa fa-gears', 0, 1, 0, 1, 10, '2019-02-22 05:17:08', '2019-02-25 00:08:36'),
(5, 'Document', 'Route', 'AdminDocumentControllerGetIndex', 'normal', 'fa fa-file-excel-o', 16, 1, 0, 1, 2, '2019-02-22 05:42:00', '2019-02-25 00:07:58'),
(6, 'Module', 'Route', 'AdminModuleControllerGetIndex', 'normal', 'fa fa-circle-o', 4, 1, 0, 1, 2, '2019-02-22 07:11:22', '2019-02-25 00:09:22'),
(7, 'Agenda', 'Route', 'AdminAgendaControllerGetIndex', 'normal', 'fa fa-calendar', 0, 1, 0, 1, 4, '2019-02-22 20:32:42', '2019-02-25 00:06:10'),
(8, 'Speaker', 'Route', 'AdminSpeakerControllerGetIndex', 'normal', 'fa fa-user-md', 16, 1, 0, 1, 3, '2019-02-22 21:00:52', '2019-02-25 00:08:14'),
(9, 'Survey', 'URL', 'javascript:void(0)', 'normal', 'fa fa-file-text-o', 0, 1, 0, 1, 7, '2019-02-22 21:15:08', '2019-02-25 00:06:47'),
(10, 'Survey Question', 'Route', 'AdminSurveyQuestionControllerGetIndex', 'normal', 'fa fa-circle-o', 9, 1, 0, 1, 2, '2019-02-22 21:16:55', '2019-02-25 00:07:10'),
(11, 'Survey Answer', 'Route', 'AdminSurveyAnswerControllerGetIndex', 'normal', 'fa fa-circle-o', 9, 1, 0, 1, 1, '2019-02-22 21:49:56', '2019-02-25 00:06:59'),
(13, 'QnA', 'Route', 'AdminQnaControllerGetIndex', 'normal', 'fa fa-question-circle', 0, 1, 0, 1, 5, '2019-02-23 02:13:46', '2019-02-25 00:06:25'),
(15, 'Feedback', 'Route', 'AdminFeedbackControllerGetIndex', 'normal', 'fa fa-star-o', 0, 1, 0, 1, 6, '2019-02-23 02:52:11', '2019-02-25 00:06:37'),
(16, 'Master Data', 'URL', 'javascript:void(0)', 'normal', 'fa fa-database', 0, 1, 0, 1, 9, '2019-02-23 02:52:40', '2019-02-25 00:07:26'),
(17, 'Index', 'Route', 'AdminIndexControllerGetIndex', 'normal', 'fa fa-dashboard', 0, 1, 1, 1, 1, '2019-02-23 03:09:29', '2019-02-25 00:04:43'),
(18, 'Notification', 'Route', 'AdminNotificationControllerGetIndex', 'normal', 'fa fa-bullhorn', 0, 1, 0, 1, 3, '2019-02-23 03:15:28', '2019-02-25 00:06:01'),
(19, 'Report', 'URL', 'javascript:void(0)', 'normal', 'fa fa-file-excel-o', 0, 1, 0, 1, 8, '2019-02-23 03:16:34', '2019-02-25 13:39:07'),
(20, 'Check In', 'Route', 'AdminCheckInControllerGetIndex', 'normal', 'fa fa-qrcode', 0, 1, 0, 1, 2, '2019-02-23 10:48:57', '2019-02-25 00:04:54'),
(21, 'Scan Type', 'Route', 'AdminScanTypeControllerGetIndex', 'normal', 'fa fa-circle-o', 4, 1, 0, 1, 1, '2019-02-24 13:35:41', '2019-02-25 00:08:49'),
(22, 'Preference', 'Route', 'AdminPreferenceControllerGetIndex', 'normal', 'fa fa-circle-o', 4, 1, 0, 1, 4, '2019-02-24 13:48:23', '2019-02-25 00:10:00'),
(23, 'Report Survey', 'Route', 'AdminReportSurveyControllerGetIndex', 'normal', 'fa fa-circle-o', 19, 1, 0, 1, 1, '2019-02-25 12:03:23', '2019-02-25 13:39:15'),
(24, 'Report Scan', 'Route', 'AdminReportScanControllerGetIndex', 'normal', 'fa fa-circle-o', 19, 1, 0, 1, 2, '2019-02-25 12:52:24', '2019-02-25 13:39:21'),
(25, 'Report Participant', 'Route', 'AdminReportParticipantControllerGetIndex', 'normal', 'fa fa-circle-o', 19, 1, 0, 1, 3, '2019-02-25 13:10:25', '2019-02-25 13:39:27');

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(18, 12, 1),
(21, 14, 1),
(34, 17, 2),
(35, 17, 1),
(36, 20, 2),
(37, 20, 1),
(38, 18, 2),
(39, 18, 1),
(40, 7, 2),
(41, 7, 1),
(42, 13, 2),
(43, 13, 1),
(44, 15, 2),
(45, 15, 1),
(46, 9, 2),
(47, 9, 1),
(48, 11, 2),
(49, 11, 1),
(50, 10, 2),
(51, 10, 1),
(52, 16, 2),
(53, 16, 1),
(54, 2, 2),
(55, 2, 1),
(56, 5, 2),
(57, 5, 1),
(58, 8, 2),
(59, 8, 1),
(60, 4, 2),
(61, 4, 1),
(62, 21, 2),
(63, 21, 1),
(64, 6, 2),
(65, 6, 1),
(66, 3, 2),
(67, 3, 1),
(68, 22, 2),
(69, 22, 1),
(70, 1, 2),
(71, 1, 1),
(75, 19, 2),
(76, 19, 1),
(77, 23, 2),
(78, 23, 1),
(79, 24, 2),
(80, 24, 1),
(81, 25, 2),
(82, 25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2019-02-22 03:21:17', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2019-02-22 03:21:17', NULL, NULL),
(12, 'Setting', 'fa fa-gear', 'setting', 'auth', 'AdminSettingController', 0, 0, '2019-02-22 03:27:09', NULL, NULL),
(13, 'Participant', 'fa fa-users', 'participant', 'member', 'AdminParticipantController', 0, 0, '2019-02-22 04:02:33', NULL, NULL),
(14, 'Banner', 'fa fa-image', 'banner', 'slider', 'AdminBannerController', 0, 0, '2019-02-22 05:16:33', NULL, NULL),
(15, 'Document', 'fa fa-file-excel-o', 'document', 'document', 'AdminDocumentController', 0, 0, '2019-02-22 05:42:00', NULL, NULL),
(16, 'Module', 'fa fa-th', 'module', 'module', 'AdminModuleController', 0, 0, '2019-02-22 07:11:17', NULL, NULL),
(17, 'Agenda', 'fa fa-calendar-o', 'agenda', 'agenda', 'AdminAgendaController', 0, 0, '2019-02-22 20:32:42', NULL, NULL),
(18, 'Speaker', 'fa fa-user-md', 'speaker', 'speaker', 'AdminSpeakerController', 0, 0, '2019-02-22 21:00:52', NULL, NULL),
(19, 'Survey Question', 'fa fa-file-text-o', 'survey_question', 'survey_question', 'AdminSurveyQuestionController', 0, 0, '2019-02-22 21:16:55', NULL, NULL),
(20, 'Survey Answer', 'fa fa-file-text-o', 'survey_answer', 'survey_answer', 'AdminSurveyAnswerController', 0, 0, '2019-02-22 21:49:56', NULL, NULL),
(21, 'QnA', 'fa fa-question-circle', 'question', 'question', 'AdminQuestionController', 0, 0, '2019-02-22 22:33:01', NULL, '2019-02-23 02:12:23'),
(22, 'QnA', 'fa fa-question-circle', 'qna', 'qna', 'AdminQnaController', 0, 0, '2019-02-23 02:13:46', NULL, NULL),
(23, 'QnA Detail', 'fa fa-question-circle', 'qna_detail', 'qna_detail', 'AdminQnaDetailController', 0, 0, '2019-02-23 02:15:36', NULL, '2019-02-23 02:17:08'),
(24, 'Feedback', 'fa fa-star-o', 'feedback', 'feedback', 'AdminFeedbackController', 0, 0, '2019-02-23 02:52:11', NULL, NULL),
(25, 'Index', 'fa fa-dashboard', 'index', 'cms_users', 'AdminIndexController', 0, 0, '2019-02-23 03:09:29', NULL, NULL),
(26, 'Notification', 'fa fa-bullhorn', 'notification', 'notification', 'AdminNotificationController', 0, 0, '2019-02-23 03:15:28', NULL, NULL),
(27, 'Check In', 'fa fa-qrcode', 'check-in', 'scan_member', 'AdminCheckInController', 0, 0, '2019-02-23 10:48:56', NULL, NULL),
(28, 'Scan Type', 'fa fa-gear', 'scan_type', 'scan_type', 'AdminScanTypeController', 0, 0, '2019-02-24 13:35:41', NULL, NULL),
(29, 'Preference', 'fa fa-gear', 'preference', 'setting', 'AdminPreferenceController', 0, 0, '2019-02-24 13:48:23', NULL, NULL),
(30, 'Report Survey', 'fa fa-file-excel-o', 'report-survey', 'survey_answer', 'AdminReportSurveyController', 0, 0, '2019-02-25 12:03:23', NULL, NULL),
(31, 'Report Scan', 'fa fa-file-excel-o', 'report-scan', 'scan_member', 'AdminReportScanController', 0, 0, '2019-02-25 12:52:24', NULL, NULL),
(32, 'Report Participant', 'fa fa-file-excel-o', 'report-participant', 'member', 'AdminReportParticipantController', 0, 0, '2019-02-25 13:10:24', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-black-light', '2019-02-22 03:21:17', NULL),
(2, 'Admin', 0, 'skin-black-light', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, 4, NULL, NULL),
(2, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(3, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(4, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(5, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(6, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(7, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(8, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(9, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(10, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(11, 1, 1, 1, 1, 1, 1, 21, NULL, NULL),
(12, 1, 1, 1, 1, 1, 1, 22, NULL, NULL),
(13, 1, 1, 1, 1, 1, 1, 23, NULL, NULL),
(14, 1, 1, 1, 1, 1, 1, 24, NULL, NULL),
(15, 1, 1, 1, 1, 1, 1, 25, NULL, NULL),
(16, 1, 1, 1, 1, 1, 1, 26, NULL, NULL),
(17, 1, 1, 1, 1, 1, 1, 27, NULL, NULL),
(18, 1, 1, 1, 1, 1, 1, 28, NULL, NULL),
(19, 1, 1, 1, 1, 1, 1, 29, NULL, NULL),
(20, 1, 1, 1, 1, 1, 2, 17, NULL, NULL),
(21, 1, 1, 1, 1, 1, 2, 14, NULL, NULL),
(22, 1, 1, 1, 1, 1, 2, 27, NULL, NULL),
(23, 1, 1, 1, 1, 1, 2, 15, NULL, NULL),
(24, 1, 1, 1, 1, 1, 2, 24, NULL, NULL),
(25, 1, 1, 1, 1, 1, 2, 25, NULL, NULL),
(26, 1, 1, 1, 1, 1, 2, 16, NULL, NULL),
(27, 1, 1, 1, 1, 1, 2, 26, NULL, NULL),
(28, 1, 1, 1, 1, 1, 2, 13, NULL, NULL),
(29, 1, 1, 1, 1, 1, 2, 29, NULL, NULL),
(30, 1, 1, 1, 1, 1, 2, 22, NULL, NULL),
(31, 1, 1, 1, 1, 1, 2, 32, NULL, NULL),
(32, 1, 1, 1, 1, 1, 2, 31, NULL, NULL),
(33, 1, 1, 1, 1, 1, 2, 30, NULL, NULL),
(34, 1, 1, 1, 1, 1, 2, 28, NULL, NULL),
(35, 1, 1, 1, 1, 1, 2, 12, NULL, NULL),
(36, 1, 1, 1, 1, 1, 2, 18, NULL, NULL),
(37, 1, 1, 1, 1, 1, 2, 20, NULL, NULL),
(38, 1, 1, 1, 1, 1, 2, 19, NULL, NULL),
(39, 1, 1, 1, 1, 1, 2, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2019-02-22 03:21:17', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2019-02-22 03:21:17', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', 'uploads/default/backend/bg_white.jpg', 'upload_image', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'system@mail.eventy.id', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'smtp', 'select', 'smtp,mail,sendmail', NULL, '2019-02-22 03:21:17', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', 'smtp.mailgun.org', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '587', 'text', NULL, 'default 25', '2019-02-22 03:21:17', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', 'system@mail.eventy.id', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', 'akuanakevent', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'Eventy', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2019-02-22 03:21:17', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', 'uploads/default/backend/logo_blue.png', 'upload_image', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', 'uploads/default/backend/logo.jpg', 'upload_image', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', 'AIzaSyDnBs51n9XBhgJIpMvx0KTXFdHBjOVdNOw', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', 'AAAAo2UF2lY:APA91bFnvNWog4_hvRDQj83yGm89JrlMHbYgPsOG9RT6zpTcl2YIuqkymLJMd1G-y1RBSMP6nb8ujxe6NVAIL7oAZzByNMIDiYWal-ts9f-J6QPBKEKNcTZjxjxM1ECutvWTMZClNxp7', 'text', NULL, NULL, '2019-02-22 03:21:17', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', NULL, 'admin@crudbooster.com', '$2y$10$buPtGUCdsz0iZzGOhZveIOcLvoEuhzyb3ODlsEcte1lsnn.tzGP32', 1, '2019-02-22 03:21:17', NULL, 'Active'),
(2, 'Admin', 'uploads/1/2019-02/profile.jpg', 'admin@eventy.id', '$2y$10$XVSh6k8f0pQpssNSXsDTn.SsGxqg.HtuzS5Io15FRdJMHR7It2WF.', 2, '2019-02-25 00:11:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `committee`
--

CREATE TABLE `committee` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `job_title` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `committee`
--

INSERT INTO `committee` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`, `job_title`, `phone`, `email`, `sort`) VALUES
(1, '2019-04-03 00:29:11', NULL, NULL, 'Annastalia', 'Public Relation', '08123456789', 'annastalia@crocodic.com', 1),
(2, '2019-04-03 00:29:11', NULL, NULL, 'Putra Arin', 'Documentation', '08987654321', 'putra@crocodic.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `filesize` float DEFAULT NULL,
  `extension` varchar(5) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `created_at`, `updated_at`, `deleted_at`, `file`, `filename`, `filesize`, `extension`, `sort`) VALUES
(1, '2019-04-09 23:31:07', NULL, NULL, 'document/document1.docx', 'document1.docx', 34217, 'docx', NULL),
(2, '2019-04-09 23:31:07', NULL, NULL, 'document/document2.pdf', 'document2.pdf', 34217, 'pdf', NULL),
(3, '2019-04-09 23:31:07', NULL, NULL, 'document/document3.pptx', 'document3.pptx', 34217, 'pptx', NULL),
(4, '2019-04-09 23:31:07', NULL, NULL, 'document/document4.xlsx', 'document4.xlsx', 34217, 'xlxs', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `ratting` int(1) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `ratting`, `description`) VALUES
(1, '2019-04-15 03:31:55', NULL, NULL, 1, 5, 'Lorem ipsum');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_date`
--

CREATE TABLE `gallery_date` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_date`
--

INSERT INTO `gallery_date` (`id`, `created_at`, `updated_at`, `deleted_at`, `date`) VALUES
(1, '2019-02-09 06:14:12', NULL, NULL, '2019-04-27'),
(2, '2019-02-09 06:14:12', NULL, NULL, '2019-04-28');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image`
--

CREATE TABLE `gallery_image` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_gallery_panel` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `type` varchar(5) DEFAULT NULL,
  `placeholder` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_image`
--

INSERT INTO `gallery_image` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_gallery_panel`, `file`, `filename`, `type`, `placeholder`) VALUES
(1, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival1.jpg', 'arrival1.jpg', 'image', 'image/setting/arrival/arrival1.jpg'),
(2, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival2.jpg', 'arrival2.jpg', 'image', 'image/setting/arrival/arrival2.jpg'),
(3, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival3.jpg', 'arrival3.jpg', 'image', 'image/setting/arrival/arrival3.jpg'),
(4, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival4.jpg', 'arrival4.jpg', 'image', 'image/setting/arrival/arrival4.jpg'),
(5, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival5.jpg', 'arrival5.jpg', 'image', 'image/setting/arrival/arrival5.jpg'),
(6, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival6.jpg', 'arrival6.jpg', 'image', 'image/setting/arrival/arrival6.jpg'),
(7, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival7.jpg', 'arrival7.jpg', 'image', 'image/setting/arrival/arrival7.jpg'),
(8, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival8.jpg', 'arrival8.jpg', 'image', 'image/setting/arrival/arrival8.jpg'),
(9, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival9.jpg', 'arrival9.jpg', 'image', 'image/setting/arrival/arrival9.jpg'),
(10, '2019-04-09 08:21:21', NULL, NULL, 1, 'image/setting/arrival/arrival10.jpg', 'arrival10.jpg', 'image', 'image/setting/arrival/arrival10.jpg'),
(11, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner1.jpg', 'wellcoming_dinner1.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner1.jpg'),
(12, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner2.jpg', 'wellcoming_dinner2.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner2.jpg'),
(13, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner3.jpg', 'wellcoming_dinner3.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner3.jpg'),
(14, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner4.jpg', 'wellcoming_dinner4.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner4.jpg'),
(15, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner5.jpg', 'wellcoming_dinner5.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner5.jpg'),
(16, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner6.jpg', 'wellcoming_dinner6.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner6.jpg'),
(17, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner7.jpg', 'wellcoming_dinner7.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner7.jpg'),
(18, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner8.jpg', 'wellcoming_dinner8.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner8.jpg'),
(19, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner9.jpg', 'wellcoming_dinner9.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner9.jpg'),
(20, '2019-04-09 08:21:21', NULL, NULL, 2, 'image/setting/wellcoming_dinner/wellcoming_dinner10.jpg', 'wellcoming_dinner10.jpg', 'image', 'image/setting/wellcoming_dinner/wellcoming_dinner10.jpg'),
(21, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference1.jpg', 'conference1.jpg', 'image', 'image/setting/conference/conference1.jpg'),
(22, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference2.jpg', 'conference2.jpg', 'image', 'image/setting/conference/conference2.jpg'),
(23, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference3.jpg', 'conference3.jpg', 'image', 'image/setting/conference/conference3.jpg'),
(24, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference4.jpg', 'conference4.jpg', 'image', 'image/setting/conference/conference4.jpg'),
(25, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference5.jpg', 'conference5.jpg', 'image', 'image/setting/conference/conference5.jpg'),
(26, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference6.jpg', 'conference6.jpg', 'image', 'image/setting/conference/conference6.jpg'),
(27, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference7.jpg', 'conference7.jpg', 'image', 'image/setting/conference/conference7.jpg'),
(28, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference8.jpg', 'conference8.jpg', 'image', 'image/setting/conference/conference8.jpg'),
(29, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference9.jpg', 'conference9.jpg', 'image', 'image/setting/conference/conference9.jpg'),
(30, '2019-04-09 08:21:21', NULL, NULL, 3, 'image/setting/conference/conference10.jpg', 'conference10.jpg', 'image', 'image/setting/conference/conference10.jpg'),
(31, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night1.jpg', 'appreciation_night1.jpg', 'image', 'image/setting/appreciation_night/appreciation_night1.jpg'),
(32, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night2.jpg', 'appreciation_night2.jpg', 'image', 'image/setting/appreciation_night/appreciation_night2.jpg'),
(33, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night3.jpg', 'appreciation_night3.jpg', 'image', 'image/setting/appreciation_night/appreciation_night3.jpg'),
(34, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night4.jpg', 'appreciation_night4.jpg', 'image', 'image/setting/appreciation_night/appreciation_night4.jpg'),
(35, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night5.jpg', 'appreciation_night5.jpg', 'image', 'image/setting/appreciation_night/appreciation_night5.jpg'),
(36, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night6.jpg', 'appreciation_night6.jpg', 'image', 'image/setting/appreciation_night/appreciation_night6.jpg'),
(37, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night7.jpg', 'appreciation_night7.jpg', 'image', 'image/setting/appreciation_night/appreciation_night7.jpg'),
(38, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night8.jpg', 'appreciation_night8.jpg', 'image', 'image/setting/appreciation_night/appreciation_night8.jpg'),
(39, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night9.jpg', 'appreciation_night9.jpg', 'image', 'image/setting/appreciation_night/appreciation_night9.jpg'),
(40, '2019-04-09 08:21:21', NULL, NULL, 4, 'image/setting/appreciation_night/appreciation_night10.jpg', 'appreciation_night10.jpg', 'image', 'image/setting/appreciation_night/appreciation_night10.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_panel`
--

CREATE TABLE `gallery_panel` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_gallery_date` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_panel`
--

INSERT INTO `gallery_panel` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_gallery_date`, `name`, `sort`) VALUES
(1, '2019-02-09 06:14:12', NULL, NULL, 1, 'Arrival', 1),
(2, '2019-02-09 06:14:12', NULL, NULL, 1, 'Wellcoming Dinner', 2),
(3, '2019-02-09 06:14:12', NULL, NULL, 2, 'Conference', 1),
(4, '2019-02-09 06:14:12', NULL, NULL, 2, 'Appreciation Night', 2);

-- --------------------------------------------------------

--
-- Table structure for table `maps`
--

CREATE TABLE `maps` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8mb4_unicode_ci,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `maps`
--

INSERT INTO `maps` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`, `image`, `location`, `latitude`, `longitude`) VALUES
(1, '2019-04-09 08:21:21', NULL, NULL, 'Bintang Kuta Hotel', 'image/maps/bintang_kuta_hotel.jpeg', 'Jalan Kartika Plaza, Kuta, Kabupaten Badung, Bali 80361', '-8.7337188', '115.1647533'),
(2, '2019-04-09 08:21:21', NULL, NULL, 'Pantai Bintang Kuta Hotel', 'image/maps/pantai_kuta.jpg', 'Jl. Pantai Kuta, Kuta, Kabupaten Badung, Bali 80361', '-8.7202445', '115.1669875'),
(3, '2019-04-09 08:21:21', NULL, NULL, 'Discovery Kartika Plaza Hotel', 'image/maps/kartika_plaza_hotel.jpg', 'Jl. Pantai Kuta, Kuta, Kabupaten Badung, Bali 80361', '-8.7295252', '115.1647909'),
(4, '2019-04-09 08:21:21', NULL, NULL, 'Wanaku Resto', 'image/maps/wanaku_resto.jpg', 'Jl. Kediri No.45A, Tuban, Kuta, Kabupaten Badung, Bali 80361', '-8.7386838', '115.1703691');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forgot_password` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `created_at`, `updated_at`, `deleted_at`, `code`, `image`, `name`, `email`, `phone`, `password`, `forgot_password`, `company`, `departement`, `id_number`, `last_login`) VALUES
(1, '2018-08-25 06:03:15', '2019-04-14 15:07:05', NULL, 'EVENTYID', 'uploads/image_profile/D1555225624.jpg', 'Demo Eventy', 'demo@eventy.id', '08123456789', '$2y$10$1Bv9lqkvPxq/aNE45yDMwukEFTsNPI6KyvLO4Wbd/ZFJvsB7s/Z/.', 'n5zc74', 'Eventy', 'Management', 'EVT-0001', '2019-04-14 14:48:06');

-- --------------------------------------------------------

--
-- Table structure for table `member_regid`
--

CREATE TABLE `member_regid` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member_regid`
--

INSERT INTO `member_regid` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `token`) VALUES
(1, '2019-04-12 17:47:47', '2019-04-14 14:48:06', NULL, 1, 'testing');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `type` varchar(6) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `is_read` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `type`, `message`, `is_read`) VALUES
(1, '2019-03-29 19:38:31', NULL, NULL, 1, 'member', 'Sayur', 0),
(2, '2019-03-29 19:38:31', '2019-04-14 15:57:04', NULL, 1, 'admin', 'Sayur', 1),
(3, '2019-04-14 16:03:58', NULL, NULL, 1, 'member', 'Halo', 0),
(4, '2019-04-14 16:11:37', NULL, NULL, 1, 'member', 'Halo', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  `admin_status` varchar(3) DEFAULT 'Yes',
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `created_at`, `updated_at`, `deleted_at`, `type`, `sorting`, `code`, `name`, `status`, `admin_status`, `message`) VALUES
(1, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'forgot_password', 'FORGOT PASSWORD', 'Yes', 'Yes', NULL),
(2, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'notification', 'NOTIFICATION', 'Yes', 'Yes', NULL),
(3, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'edit_password', 'EDIT PASSWORD', 'Yes', 'Yes', NULL),
(4, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'scan_history', 'SCAN HISTORY', 'Yes', 'Yes', NULL),
(5, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'message_to_admin', 'MESSAGE TO ADMIN', 'No', 'Yes', NULL),
(6, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'profile', 'PROFILE', 'Yes', 'Yes', NULL),
(7, '2018-12-09 14:18:17', NULL, NULL, 'static', NULL, 'user_scan', 'SCAN QR CODE', 'Yes', 'No', NULL),
(101, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 1, 'qr_code', 'MY QR', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(102, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 2, 'agenda', 'AGENDA', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(103, '2018-12-09 14:18:17', NULL, NULL, 'modular', 4, 'maps', 'MAPS', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(104, '2018-12-09 14:18:17', NULL, NULL, 'modular', 5, 'gallery', 'GALLERY', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(105, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 6, 'document', 'SLIDES', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(106, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 10, 'survey', 'SURVEY', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(107, '2018-12-09 14:18:17', NULL, NULL, 'modular', 7, 'quiz', 'QUIZ', 'No', 'No', 'Sorry, you cannot access this right now'),
(108, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 9, 'feedback', 'FEEDBACK', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(109, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', NULL, 'speaker', 'MEMBER', 'Yes', 'No', 'Sorry, you cannot access this right now'),
(110, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 3, 'qna', 'QnA', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(111, '2018-12-09 14:18:17', NULL, NULL, 'modular', 12, 'help', 'HELP', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(112, '2018-12-09 14:18:17', NULL, NULL, 'modular', 11, 'about', 'ABOUT', 'Yes', 'Yes', 'Sorry, you cannot access this right now'),
(113, '2018-12-09 14:18:17', '2019-02-25 11:07:41', NULL, 'modular', 8, 'souvenir', 'SOUVENIR', 'No', 'No', 'Sorry, you cannot access this right now');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `created_at`, `updated_at`, `deleted_at`, `title`, `content`) VALUES
(1, '2019-04-09 08:21:21', NULL, NULL, 'Welcome to Splash Party', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.');

-- --------------------------------------------------------

--
-- Table structure for table `notification_member`
--

CREATE TABLE `notification_member` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_notification` int(11) DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `action_type` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification_member`
--

INSERT INTO `notification_member` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_notification`, `id_member`, `action_type`) VALUES
(1, '2019-04-14 12:43:01', NULL, NULL, 1, 1, 'Read'),
(2, '2019-04-14 12:47:49', NULL, NULL, 1, 1, 'Delete');

-- --------------------------------------------------------

--
-- Table structure for table `qna`
--

CREATE TABLE `qna` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_speaker` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qna`
--

INSERT INTO `qna` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `id_speaker`) VALUES
(1, '2019-04-09 09:11:40', '2019-04-09 09:11:40', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `qna_detail`
--

CREATE TABLE `qna_detail` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_qna` int(11) DEFAULT NULL,
  `message` text,
  `type` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qna_detail`
--

INSERT INTO `qna_detail` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_qna`, `message`, `type`) VALUES
(1, '2019-04-09 09:11:44', NULL, NULL, 1, 'testing', 'Member'),
(2, '2019-04-14 16:16:16', NULL, NULL, 1, 'testing user', 'Member'),
(3, '2019-04-14 16:17:04', NULL, NULL, 1, 'testing user', 'Member'),
(4, '2019-04-14 16:24:35', NULL, NULL, 1, 'testing', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`, `status`, `sort`) VALUES
(1, '2019-03-18 08:59:31', '2019-03-19 09:59:28', NULL, 'Quiz 1', 'Yes', 1),
(2, '2019-03-19 09:48:49', '2019-03-19 13:31:46', NULL, 'Quiz 2', 'Yes', 2),
(3, '2019-03-19 14:16:26', '2019-03-19 16:11:15', NULL, 'Quiz 3', 'Yes', 3);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answer`
--

CREATE TABLE `quiz_answer` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_quiz` int(11) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `seconds` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz_answer`
--

INSERT INTO `quiz_answer` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `id_quiz`, `point`, `seconds`) VALUES
(1, '2019-04-15 02:46:54', '2019-04-15 03:07:50', NULL, 1, 1, 40, 85.0796);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_answer_detail`
--

CREATE TABLE `quiz_answer_detail` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_quiz_answer` int(11) DEFAULT NULL,
  `id_quiz_question` int(11) DEFAULT NULL,
  `answer` varchar(1) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `second` float DEFAULT NULL,
  `datetime_start` varchar(50) DEFAULT NULL,
  `datetime_end` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz_answer_detail`
--

INSERT INTO `quiz_answer_detail` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_quiz_answer`, `id_quiz_question`, `answer`, `point`, `second`, `datetime_start`, `datetime_end`) VALUES
(1, '2019-04-15 03:07:50', NULL, NULL, 1, 1, 'B', 10, 24.9989, '2019-04-15 01:44:40.0012', '2019-04-15 01:45:05.0001'),
(2, '2019-04-15 03:07:50', NULL, NULL, 1, 2, 'A', 0, 10.4464, '2019-04-15 01:45:50.8271', '2019-04-15 01:46:01.2735'),
(3, '2019-04-15 03:07:50', NULL, NULL, 1, 3, 'D', 10, 15.5613, '2019-04-15 01:46:10.1263', '2019-04-15 01:46:25.6876'),
(4, '2019-04-15 03:07:50', NULL, NULL, 1, 4, 'A', 10, 14.7995, '2019-04-15 01:46:58.8267', '2019-04-15 01:47:13.6262'),
(5, '2019-04-15 03:07:50', NULL, NULL, 1, 5, 'B', 10, 19.2735, '2019-04-15 01:47:50.9891', '2019-04-15 01:48:10.2626');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_question`
--

CREATE TABLE `quiz_question` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_quiz` int(11) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `option_a` varchar(50) DEFAULT NULL,
  `option_b` varchar(50) DEFAULT NULL,
  `option_c` varchar(50) DEFAULT NULL,
  `option_d` varchar(50) DEFAULT NULL,
  `answer` varchar(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz_question`
--

INSERT INTO `quiz_question` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_quiz`, `question`, `option_a`, `option_b`, `option_c`, `option_d`, `answer`, `sort`) VALUES
(1, '2019-04-15 01:44:40', NULL, NULL, 1, 'Question 1.1', 'Option A 1.1', 'Option B 1.1', 'Option C 1.1', 'Option D 1.1', 'B', 1),
(2, '2019-04-15 01:44:40', NULL, NULL, 1, 'Question 1.2', 'Option A 1.2', 'Option B 1.2', 'Option C 1.2', 'Option D 1.2', 'B', 2),
(3, '2019-04-15 01:44:40', NULL, NULL, 1, 'Question 1.3', 'Option A 1.3', 'Option B 1.3', 'Option C 1.3', 'Option D 1.3', 'D', 3),
(4, '2019-04-15 01:44:40', NULL, NULL, 1, 'Question 1.4', 'Option A 1.4', 'Option B 1.4', 'Option C 1.4', 'Option D 1.4', 'A', 4),
(5, '2019-04-15 01:44:40', NULL, NULL, 1, 'Question 1.5', 'Option A 1.5', 'Option B 1.5', 'Option C 1.5', 'Option D 1.5', 'B', 5),
(6, '2019-04-15 01:44:40', NULL, NULL, 2, 'Question 2.1', 'Option A 2.1', 'Option B 2.1', 'Option C 2.1', 'Option D 2.1', 'D', 1),
(7, '2019-04-15 01:44:40', NULL, NULL, 2, 'Question 2.2', 'Option A 2.2', 'Option B 2.2', 'Option C 2.2', 'Option D 2.2', 'C', 2),
(8, '2019-04-15 01:44:40', NULL, NULL, 2, 'Question 2.3', 'Option A 2.3', 'Option B 2.3', 'Option C 2.3', 'Option D 2.3', 'A', 3),
(9, '2019-04-15 01:44:40', NULL, NULL, 2, 'Question 2.4', 'Option A 2.4', 'Option B 2.4', 'Option C 2.4', 'Option D 2.4', 'B', 4),
(10, '2019-04-15 01:44:40', NULL, NULL, 2, 'Question 2.5', 'Option A 2.5', 'Option B 2.5', 'Option C 2.5', 'Option D 2.5', 'C', 5),
(11, '2019-04-15 01:44:40', NULL, NULL, 3, 'Question 3.1', 'Option A 3.1', 'Option B 3.1', 'Option C 3.1', 'Option D 3.1', 'B', 1),
(12, '2019-04-15 01:44:40', NULL, NULL, 3, 'Question 3.2', 'Option A 3.2', 'Option B 3.2', 'Option C 3.2', 'Option D 3.2', 'B', 2),
(13, '2019-04-15 01:44:40', NULL, NULL, 3, 'Question 3.3', 'Option A 3.3', 'Option B 3.3', 'Option C 3.3', 'Option D 3.3', 'C', 3),
(14, '2019-04-15 01:44:40', NULL, NULL, 3, 'Question 3.4', 'Option A 3.4', 'Option B 3.4', 'Option C 3.4', 'Option D 3.4', 'D', 4),
(15, '2019-04-15 01:44:40', NULL, NULL, 3, 'Question 3.5', 'Option A 3.5', 'Option B 3.5', 'Option C 3.5', 'Option D 3.5', 'B', 5);

-- --------------------------------------------------------

--
-- Table structure for table `scan_member`
--

CREATE TABLE `scan_member` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_scan_type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scan_member`
--

INSERT INTO `scan_member` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `id_scan_type`) VALUES
(1, '2019-04-26 07:16:29', NULL, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `scan_type`
--

CREATE TABLE `scan_type` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scan_type`
--

INSERT INTO `scan_type` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`, `date`) VALUES
(1, '2019-02-24 23:31:07', NULL, NULL, 'Check In Gate - Day 1', '2019-04-26'),
(2, '2019-02-24 23:31:07', NULL, NULL, 'Check In Gate - Day 2', '2019-04-27'),
(3, '2019-02-24 23:31:07', NULL, NULL, 'Check In Gate - Day 3', '2019-04-28');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `created_at`, `updated_at`, `deleted_at`, `type`, `value`) VALUES
(1, '2018-12-09 14:18:17', NULL, NULL, 'api-key', 'eventydemo'),
(2, '2018-12-09 14:18:17', NULL, NULL, 'profile_image', 'Yes'),
(3, '2018-12-09 14:18:17', NULL, NULL, 'profile_name', 'Yes'),
(4, '2018-12-09 14:18:17', NULL, NULL, 'profile_email', 'No'),
(5, '2018-12-09 14:18:17', NULL, NULL, 'profile_phone', 'Yes'),
(6, '2018-12-09 14:18:17', NULL, NULL, 'profile_company', 'Yes'),
(7, '2018-12-09 14:18:17', NULL, NULL, 'profile_departement', 'Yes'),
(8, '2018-12-09 14:18:17', NULL, NULL, 'profile_id_number', 'No'),
(9, '2018-12-09 14:18:17', NULL, NULL, 'event_date_start', '2019-04-27'),
(10, '2018-12-09 14:18:17', NULL, NULL, 'event_date_finish', '2019-04-28'),
(11, '2018-12-09 14:18:17', NULL, NULL, 'about_event_image', 'image/setting/banner1.png'),
(12, '2018-12-09 14:18:17', NULL, NULL, 'about_event_content', '<h2>What is Eventy?</h2>\n\n<p>Eventy is SOLUSI for event organizers in helping to solve difficult problems MONITORING and MEASURING Corporate Event participants\' participation in real-time.</p>\n\n<p>DATA results of monitoring and measurement are used as INSIGHT by EO in making improvements so that the corporate event objectives can be achieved.</p>'),
(13, '2018-12-09 14:18:17', NULL, NULL, 'application_name', 'Splash Party'),
(14, '2018-12-09 14:18:17', NULL, NULL, 'application_date_start', '2019-02-24'),
(15, '2018-12-09 14:18:17', NULL, NULL, 'application_date_end', '2019-02-27'),
(16, '2018-12-09 14:18:17', NULL, NULL, 'feedback_ratting', 'What is the ratting of Eventy Application after your using this application?'),
(17, '2018-12-09 14:18:17', NULL, NULL, 'feedback_description', 'How do you feel after using Eventy Application?'),
(18, '2018-12-09 14:18:17', NULL, NULL, 'quiz_point', '10'),
(19, '2018-12-09 14:18:17', NULL, NULL, 'quiz_coundown', '30');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `created_at`, `updated_at`, `deleted_at`, `image`) VALUES
(1, '2018-12-10 09:22:50', NULL, NULL, 'image/setting/banner2.png');

-- --------------------------------------------------------

--
-- Table structure for table `speaker`
--

CREATE TABLE `speaker` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `speaker`
--

INSERT INTO `speaker` (`id`, `created_at`, `updated_at`, `deleted_at`, `image`, `name`, `title`, `description`) VALUES
(1, '2019-02-23 04:11:48', '2019-04-14 15:25:25', NULL, 'uploads/1/2019-04/putra.png', 'Admin', 'Public Relation', 'With Eventy Apps, Now Event Organizer can reach Event goals Faster and Easier\r\n\r\nEvet planners are known for creating incredible experience. We make those experiences trackable, measurable and optimized.');

-- --------------------------------------------------------

--
-- Table structure for table `survey_answer`
--

CREATE TABLE `survey_answer` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_survey_question` int(11) DEFAULT NULL,
  `id_survey_option` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `survey_answer`
--

INSERT INTO `survey_answer` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `id_survey_question`, `id_survey_option`) VALUES
(1, '2019-04-15 01:23:51', NULL, NULL, 1, 1, 1),
(2, '2019-04-15 01:23:51', NULL, NULL, 1, 2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `survey_option`
--

CREATE TABLE `survey_option` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_survey_question` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `survey_option`
--

INSERT INTO `survey_option` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_survey_question`, `value`, `sort`) VALUES
(1, NULL, NULL, NULL, 1, 'Answer 1.1', 1),
(2, NULL, NULL, NULL, 1, 'Answer 1.2', 2),
(3, NULL, NULL, NULL, 1, 'Answer 1.3', 3),
(4, NULL, NULL, NULL, 1, 'Answer 1.4', 4),
(5, NULL, NULL, NULL, 2, 'Answer 2.1', 1),
(6, NULL, NULL, NULL, 2, 'Answer 2.2', 2),
(7, NULL, NULL, NULL, 2, 'Answer 2.3', 3),
(8, NULL, NULL, NULL, 2, 'Answer 2.4', 4);

-- --------------------------------------------------------

--
-- Table structure for table `survey_question`
--

CREATE TABLE `survey_question` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `open` varchar(3) DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `survey_question`
--

INSERT INTO `survey_question` (`id`, `created_at`, `updated_at`, `deleted_at`, `value`, `sort`, `open`) VALUES
(1, '2019-04-09 11:00:39', '2019-04-09 11:05:30', NULL, 'Question 1', 1, 'Yes'),
(2, '2019-04-09 19:43:13', '2019-04-03 11:24:25', NULL, 'Question 2', 2, 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `created_at`, `updated_at`, `deleted_at`, `title`, `image`) VALUES
(1, '2019-04-15 03:12:15', NULL, NULL, 'Sample Voucher', 'uploads/default/coupon_souvenir.png');

-- --------------------------------------------------------

--
-- Table structure for table `voucher_member`
--

CREATE TABLE `voucher_member` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `id_member` int(11) DEFAULT NULL,
  `id_voucher` int(11) DEFAULT NULL,
  `qrcode` varchar(100) DEFAULT NULL,
  `redeem` varchar(3) DEFAULT 'No',
  `datetime_redeem` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voucher_member`
--

INSERT INTO `voucher_member` (`id`, `created_at`, `updated_at`, `deleted_at`, `id_member`, `id_voucher`, `qrcode`, `redeem`, `datetime_redeem`) VALUES
(1, '2019-04-15 03:12:15', NULL, NULL, 1, 1, 'EVTREDEEM', 'No', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agenda_document`
--
ALTER TABLE `agenda_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agenda_speaker`
--
ALTER TABLE `agenda_speaker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `committee`
--
ALTER TABLE `committee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_date`
--
ALTER TABLE `gallery_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_image`
--
ALTER TABLE `gallery_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_panel`
--
ALTER TABLE `gallery_panel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maps`
--
ALTER TABLE `maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_regid`
--
ALTER TABLE `member_regid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `is_read` (`is_read`),
  ADD KEY `id_member` (`id_member`),
  ADD KEY `id_member_2` (`id_member`,`type`),
  ADD KEY `id_member_3` (`id_member`,`type`,`is_read`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deleted_at` (`deleted_at`,`status`,`admin_status`),
  ADD KEY `deleted_at_2` (`deleted_at`),
  ADD KEY `status` (`status`),
  ADD KEY `admin_status` (`admin_status`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `notification_member`
--
ALTER TABLE `notification_member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_member` (`id_member`),
  ADD KEY `id_notification` (`id_notification`),
  ADD KEY `id_notification_2` (`id_notification`,`id_member`);

--
-- Indexes for table `qna`
--
ALTER TABLE `qna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qna_detail`
--
ALTER TABLE `qna_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_answer_detail`
--
ALTER TABLE `quiz_answer_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_question`
--
ALTER TABLE `quiz_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scan_member`
--
ALTER TABLE `scan_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scan_type`
--
ALTER TABLE `scan_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `speaker`
--
ALTER TABLE `speaker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey_answer`
--
ALTER TABLE `survey_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey_option`
--
ALTER TABLE `survey_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey_question`
--
ALTER TABLE `survey_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `voucher_member`
--
ALTER TABLE `voucher_member`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `agenda_document`
--
ALTER TABLE `agenda_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `agenda_speaker`
--
ALTER TABLE `agenda_speaker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `committee`
--
ALTER TABLE `committee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gallery_date`
--
ALTER TABLE `gallery_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gallery_image`
--
ALTER TABLE `gallery_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `gallery_panel`
--
ALTER TABLE `gallery_panel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `maps`
--
ALTER TABLE `maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `member_regid`
--
ALTER TABLE `member_regid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notification_member`
--
ALTER TABLE `notification_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `qna`
--
ALTER TABLE `qna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `qna_detail`
--
ALTER TABLE `qna_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quiz_answer`
--
ALTER TABLE `quiz_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quiz_answer_detail`
--
ALTER TABLE `quiz_answer_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `quiz_question`
--
ALTER TABLE `quiz_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `scan_member`
--
ALTER TABLE `scan_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scan_type`
--
ALTER TABLE `scan_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `speaker`
--
ALTER TABLE `speaker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `survey_answer`
--
ALTER TABLE `survey_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `survey_option`
--
ALTER TABLE `survey_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `survey_question`
--
ALTER TABLE `survey_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `voucher_member`
--
ALTER TABLE `voucher_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
