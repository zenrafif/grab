<table border="1">

    @foreach($question as $row)
        <?php
        $option = $row->option;
        $answer = $row->answer;
        $total_option = count($option);
        ?>
        <tr>
            <td rowspan="2">No</td>
            <td align="center">Question</td>
            <td colspan="{{$total_option}}" align="center">Jawaban</td>
        </tr>
        <tr>
            <td>{{$row->value}}</td>
            @foreach($option as $xrow)
                <td>{{$xrow->value}}</td>
            @endforeach
        </tr>

        <?php
        $no = 1;
        ?>
        @foreach($answer as $xrow)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$xrow->member_name}}</td>
                @foreach($option as $yrow)
                    <?php
                    if ($xrow->id_survey_option == $yrow->id) {
                        $yrow->total++;
                        $text = 'v';
                    } else {
                        $text = '';
                    }
                    ?>
                    <td>{{$text}}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td colspan="2">Total</td>
            @foreach($option as $xrow)
                <td>{{$xrow->total}}</td>
            @endforeach
        </tr>

        <tr>
            <td colspan="{{$total_option+2}}"></td>
        </tr>
    @endforeach
</table>

