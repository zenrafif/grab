@extends('crudbooster::admin_template')
@section('content')
    <style>
        .export-btn {
            text-align: center;
        }

        .checkbox-form {
            padding: 20px 40px;
        }

        input[type='checkbox'], input[type='radio'] {
            cursor: pointer;
        }

        #loading {
            display: none;
        }

        .lds-spinner {
            color: official;
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
        }

        .lds-spinner div {
            transform-origin: 32px 32px;
            animation: lds-spinner 1.2s linear infinite;
        }

        .lds-spinner div:after {
            content: " ";
            display: block;
            position: absolute;
            top: 3px;
            left: 29px;
            width: 5px;
            height: 14px;
            border-radius: 20%;
            background: #000;
        }

        .lds-spinner div:nth-child(1) {
            transform: rotate(0deg);
            animation-delay: -1.1s;
        }

        .lds-spinner div:nth-child(2) {
            transform: rotate(30deg);
            animation-delay: -1s;
        }

        .lds-spinner div:nth-child(3) {
            transform: rotate(60deg);
            animation-delay: -0.9s;
        }

        .lds-spinner div:nth-child(4) {
            transform: rotate(90deg);
            animation-delay: -0.8s;
        }

        .lds-spinner div:nth-child(5) {
            transform: rotate(120deg);
            animation-delay: -0.7s;
        }

        .lds-spinner div:nth-child(6) {
            transform: rotate(150deg);
            animation-delay: -0.6s;
        }

        .lds-spinner div:nth-child(7) {
            transform: rotate(180deg);
            animation-delay: -0.5s;
        }

        .lds-spinner div:nth-child(8) {
            transform: rotate(210deg);
            animation-delay: -0.4s;
        }

        .lds-spinner div:nth-child(9) {
            transform: rotate(240deg);
            animation-delay: -0.3s;
        }

        .lds-spinner div:nth-child(10) {
            transform: rotate(270deg);
            animation-delay: -0.2s;
        }

        .lds-spinner div:nth-child(11) {
            transform: rotate(300deg);
            animation-delay: -0.1s;
        }

        .lds-spinner div:nth-child(12) {
            transform: rotate(330deg);
            animation-delay: 0s;
        }

        @keyframes lds-spinner {
            0% {
                opacity: 1;
            }
            100% {
                opacity: 0;
            }
        }
    </style>

    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-file-o"></i> Export Data From System
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <label for="company">Company</label>
                    <select name="company" id="company" class="form-control">
                        <option value="">All Company</option>
                        @foreach($company as $row)
                            <option value="{{$row->company}}">{{$row->company}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-6">
                    <label for="departement">Departement</label>
                    <select name="departement" id="departement" class="form-control">
                        <option value="">Please select a Company First</option>
                    </select>
                </div>
                <div class="col-sm-6" style="padding-top: 20px;">
                    <label for="login">Login</label>
                    <div>
                        <label class="radio-inline"><input type="radio" name="login" checked value="All">All</label>
                        <label class="radio-inline"><input type="radio" name="login" value="Yes">Yes</label>
                        <label class="radio-inline"><input type="radio" name="login" value="No">No</label>
                    </div>
                </div>
                <div class="col-sm-6" style="padding-top: 20px;">
                    <label for="send_email">Send Email</label>
                    <div>
                        <label class="radio-inline"><input type="radio" name="send_email" checked
                                                           value="All">All</label>
                        <label class="radio-inline"><input type="radio" name="send_email" value="Yes">Yes</label>
                        <label class="radio-inline"><input type="radio" name="send_email" value="No">No</label>
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                <div class="col-sm-12">
                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#checkBox">Form Format</a>
                                    <span class="pull-right">
                                            <input id="allCheckForm" type="checkbox" value="" style="margin-top: 0;">
                                        </span>
                                </h4>
                            </div>
                            <div id="checkBox" class="panel-collapse collapse in">
                                <div class="row">
                                    <div class="col-sm-12 checkbox-form">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="form" checked value="qr_code">QR Code
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="form" checked value="image">Image
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="form" checked value="name">Name
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="form" checked value="email">Email
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="form" checked value="phone">Phone
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="form" checked value="company">Company
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="form" checked value="departement">Departement
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="form" checked value="last_login">Last Login
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="form" checked value="send_email">Total Send
                                            Email
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="form" checked value="last_send_email">Last
                                            Send Email
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <center>
                <div id="loading" class="lds-spinner">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </center>
            <div class="row">
                <div class="col-sm-4">
                    <button id="exportExcel" class="btn btn-block btn-social btn-success export-btn" value="Excel">
                        <i class="fa fa-file-excel-o"></i> Export to EXCEL
                    </button>
                </div>
                <div class="col-sm-4">
                    <button id="exportCsv" class="btn btn-block btn-social btn-info export-btn" value="CSV">
                        <i class="fa fa-file-text-o"></i> Export to CSV
                    </button>
                </div>
                <div class="col-sm-4">
                    <button id="exportPdf" class="btn btn-block btn-social btn-danger export-btn" value="PDF">
                        <i class="fa fa-file-pdf-o"></i> Export to PDF
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('bottom')
    <script>
        $(document).ready(function () {
            /**
             * ajax departement
             */
            $('#company').on('change', function () {
                let value = $(this).val();

                if (value == '') {
                    $('#departement').html('<option value="">Please select a Company First</option>')
                } else {
                    $('#departement').html('<option value="">Please Wait ...</option>');

                    let request = $.ajax({
                        url: "{{CRUDBooster::mainpath('load-departement')}}",
                        cache: false,
                        data: {
                            company: value
                        }
                    });

                    request.done(function (json) {
                        let html = '<option value="">All Departement</option>';
                        $.each(json, function (i, v) {
                            html += '<option value="">' + v.departement + '</option>';
                        });

                        $('#departement').html(html)
                    });

                    request.fail(function (jqXHR, textStatus) {
                        $('#departement').html('<option value="">Please select a Company First</option>')
                    });
                }
            })

            /**
             * checkbox
             */
            $('#allCheckForm').on('change', function () {
                let value = $(this).is(':checked');
                console.log(value);

                if (value) {
                    $('input[name="form"]').prop('checked', true);
                }
            })

            /**
             * export button
             */
            $('#exportExcel, #exportCsv, #exportPdf').on('click', function () {
                let value = $(this).val();
                let company = $('#company').val();
                let departement = $('#departement').val();
                let login = $('input[name="login"]:checked').val();
                let sendEmail = $('input[name="send_email"]:checked').val();
                let form = [];
                $.each($("input[name='form']:checked"), function () {
                    form.push($(this).val());
                });
                buttonDisabled();

                var settings = {
                    "url": "{{CRUDBooster::mainpath('export-excel')}}",
                    "method": "GET",
                    "data": {
                        company: company,
                        departement: departement,
                        login: login,
                        send_email: sendEmail,
                        type: value,
                        form: form,
                    }
                }

                $.ajax(settings).done(function (response) {
                    if (response.api_status == 1) {
                        window.open(response.callback);
                    }
                    $('#loading').hide()
                    buttonActive();
                });

                $('#loading').show(0)
            })

            function buttonActive() {
                $('#exportExcel').attr('disabled', false);
                $('#exportCsv').attr('disabled', false);
                $('#exportPdf').attr('disabled', false);
            }

            function buttonDisabled() {
                $('#exportExcel').attr('disabled', true);
                $('#exportCsv').attr('disabled', true);
                $('#exportPdf').attr('disabled', true);
            }
        })
    </script>

@endpush