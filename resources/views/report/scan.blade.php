<table border="1">
    @foreach($scan_type as $row)
        <tr>
            <td colspan="3">
                {{$row->name}}
            </td>
        </tr>
        <tr>
            <td>No</td>
            <td>Name</td>
            <td>Datetime</td>
        </tr>
        <?php
        $scan_member = $row->scan_member;
        $no = 1;
        ?>
        @foreach($scan_member as $xrow)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$xrow->member_name}}</td>
                <td>{{$xrow->created_at}}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="3" height="10px"></td>
        </tr>
    @endforeach
</table>