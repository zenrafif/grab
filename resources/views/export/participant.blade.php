<table border="1" style="border-collapse: collapse;">
    <tr>
        @if(in_array('qr_code',$form))
            <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">QR Code</td>
        @endif
        @if(in_array('image',$form))
            <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">Image</td>
        @endif
        @if(in_array('name',$form))
            <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">Name</td>
        @endif
        @if(in_array('email',$form))
            <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">Email</td>
        @endif
        @if(in_array('phone',$form))
            <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">Phone</td>
        @endif
        @if(in_array('company',$form))
            <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">Company</td>
        @endif
        @if(in_array('departement',$form))
            <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">Departement</td>
        @endif
        @if(in_array('last_login',$form))
            <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">Last Login</td>
        @endif
        @if(in_array('send_email',$form))
            <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">Total Send Email</td>
            <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">Last Send Email</td>
        @endif
    </tr>

    @foreach($member as $row)
        <tr>
            @if(in_array('qr_code',$form))
                <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">{!! $row->code !!}</td>
            @endif
            @if(in_array('image',$form))
                <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">{!! $row->image !!}</td>
            @endif
            @if(in_array('name',$form))
                <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">{!! $row->name !!}</td>
            @endif
            @if(in_array('email',$form))
                <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">{!! $row->email !!}</td>
            @endif
            @if(in_array('phone',$form))
                <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">{!! $row->phone !!}</td>
            @endif
            @if(in_array('company',$form))
                <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">{!! $row->company !!}</td>
            @endif
            @if(in_array('departement',$form))
                <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">{!! $row->departement !!}</td>
            @endif
            @if(in_array('last_login',$form))
                <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">{!! $row->last_login !!}</td>
            @endif
            @if(in_array('send_email',$form))
                <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">{!! $row->send_email !!}</td>
                <td style="border: 1px solid #999999;padding: 0.5rem;text-align: left;">{!! $row->last_send_email !!}</td>
            @endif
        </tr>
    @endforeach
</table>