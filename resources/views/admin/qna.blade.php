<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
    <div>
        <p><a title="Return" href="{{CRUDBooster::mainpath()}}"><i class="fa fa-chevron-circle-left "></i> &nbsp; Back
                To List</a></p>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- DIRECT CHAT PRIMARY -->
            <div class="box box-default direct-chat direct-chat-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List Message</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <!-- Conversations are loaded here -->
                    <div id="div1" class="direct-chat-messages" style="height: 750px;" onload="">

                    @foreach($messages as $row)
                        @if($row->type == 'Member')
                            <!-- Message. Default to the left -->
                                <div class="direct-chat-msg">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-left">{{$row->name}}</span>
                                        <span class="direct-chat-timestamp pull-right">
                                            {{date('d M H:i A',strtotime($row->created_at))}}
                                        </span>
                                    </div>
                                    <!-- /.direct-chat-info -->
                                    <a data-lightbox="roadtrip" rel="group_{d_users}" title="Photo: test"
                                       href="{{$row->image}}">
                                        <img class="direct-chat-img" src="{{$row->image}}" alt="Message User Image">
                                        <!-- /.direct-chat-img -->
                                    </a>
                                    <div class="direct-chat-text">
                                        {{$row->message}}
                                    </div>
                                    <!-- /.direct-chat-text -->
                                </div>
                                <!-- /.direct-chat-msg -->

                        @else

                            <!-- Message to the right -->
                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-info clearfix">
                                        <span class="direct-chat-name pull-right">{{$row->name}}</span>
                                        <span class="direct-chat-timestamp pull-left">
                                            {{date('d M H:i A',strtotime($row->created_at))}}
                                        </span>
                                    </div>
                                    <!-- /.direct-chat-info -->
                                    <a data-lightbox="roadtrip" rel="group_{d_users}" title="Photo: test"
                                       href="{{$row->image}}">
                                        <img class="direct-chat-img" src="{{$row->image}}" alt="Message User Image">
                                        <!-- /.direct-chat-img -->
                                    </a>
                                    <div class="direct-chat-text">
                                        {{$row->message}}
                                    </div>
                                    <!-- /.direct-chat-text -->
                                </div>
                                <!-- /.direct-chat-msg -->
                            @endif
                        @endforeach

                    </div>
                    <!--/.direct-chat-messages-->
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    <form action="{{CRUDBooster::mainpath('add-messages/'.Request::segment(4))}}" method="post">
                        <div class="input-group">
                            <input type="text" name="message" placeholder="Type Message ..." class="form-control"
                                   required="">
                            <span class="input-group-btn">
                <button type="submit" class="btn btn-primary btn-flat">Send</button>
              </span>
                        </div>
                        <input type="hidden" name="_token" value={{csrf_token()}}>
                    </form>
                </div>
                <!-- /.box-footer-->
            </div>
            <!--/.direct-chat -->
        </div>
    </div>
@endsection


@push('bottom')
    <script>
        $('#div1').scrollTop($('#div1')[0].scrollHeight);
    </script>
@endpush

