@extends('crudbooster::admin_template')
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <i class="fa fa-file-zip-o"></i> Download All Files Document and Image
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Type</th>
                            <th>Filename</th>
                            <th>File Size</th>
                            <th>Last Update</th>
                            <th style="text-align: right" width="70px">Download</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $row)
                            <?php if($row == null) continue; ?>
                            <tr>
                                <td>{{$row->type}}</td>
                                <td>{{$row->filename}}</td>
                                <td>{{$row->filesize}}</td>
                                <td>{{$row->last_update}}</td>
                                <td class="pull-right">
                                    <a href="{{$row->url}}" target="_blank" class="btn btn-primary btn-xs">
                                        <i class="fa fa-download"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('bottom')
@endpush