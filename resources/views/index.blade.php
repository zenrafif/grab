<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
    <style type="text/css">
        .lds-ellipsis {
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
        }

        .lds-ellipsis div {
            position: absolute;
            top: 27px;
            width: 11px;
            height: 11px;
            border-radius: 50%;
            background: #0073b7;
            animation-timing-function: cubic-bezier(0, 1, 1, 0);
        }

        .lds-ellipsis div:nth-child(1) {
            left: 6px;
            animation: lds-ellipsis1 0.6s infinite;
        }

        .lds-ellipsis div:nth-child(2) {
            left: 6px;
            animation: lds-ellipsis2 0.6s infinite;
        }

        .lds-ellipsis div:nth-child(3) {
            left: 26px;
            animation: lds-ellipsis2 0.6s infinite;
        }

        .lds-ellipsis div:nth-child(4) {
            left: 45px;
            animation: lds-ellipsis3 0.6s infinite;
        }

        @keyframes lds-ellipsis1 {
            0% {
                transform: scale(0);
            }
            100% {
                transform: scale(1);
            }
        }

        @keyframes lds-ellipsis3 {
            0% {
                transform: scale(1);
            }
            100% {
                transform: scale(0);
            }
        }

        @keyframes lds-ellipsis2 {
            0% {
                transform: translate(0, 0);
            }
            100% {
                transform: translate(19px, 0);
            }
        }

        table tr {
            cursor: pointer;;
        }
    </style>

    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Total Member</span>
                    <span class="info-box-number">{{$total_member}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-check-square-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Total Login</span>
                    <span class="info-box-number">{{$total_login}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-star"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Ratting Feedback</span>
                    <span class="info-box-number">{{$ratting_feedback}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <form>
                    <div class="box-header with-border">
                        <h3 class="box-title">Grafik Check In</h3>
                        <div class="input-group pull-right" style="margin-right: 5px;">
                            {{--<select id="selectCheckIn" name="month" class="form-control" style="height: 20px;">--}}
                            {{--<option value="all" selected="">All</option>--}}
                            {{--@foreach($list_date as $key => $value)--}}
                            {{--<option value="{{$value}}">{{$value}}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                        </div>
                    </div>
                </form>
                <!-- /.box-header -->

                <div class="box-body">
                    <div align="center">
                        <div id="loadingCheckIn" class="lds-ellipsis">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                    <div id="grafikCheckIn" style="width: 100%;height: 500px;"></div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-sm-12">
            <div class="box box-primary">
                <form action="" method="GET">
                    <div class="box-header with-border">
                        <h3 class="box-title">Grafik Live Poll</h3>
                    </div>
                </form>
                <!-- /.box-header -->

                <div class="box-body">
                    <div align="center">
                        <div id="loadingLivePoll" class="lds-ellipsis">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                    <div id="grafikLivePoll" style="width: 100%;height: 500px;"></div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection

@push('bottom')
    <script src="{{asset('js/highchart/code/highcharts.js')}}"></script>
    <script src="{{asset('js/highchart/code/modules/exporting.js')}}"></script>
    <script src="{{asset('js/highchart/code/modules/offline-exporting.js')}}"></script>
    <script src="{{asset('js/highchart/code/modules/export-data.js')}}"></script>
    <script src="{{asset('js/highchart/code/modules/data.js')}}"></script>
    <script src="{{asset('js/highchart/code/modules/drilldown.js')}}"></script>
    <script>
        $(document).ready(function () {
            /**
             * Check In
             */
            Highcharts.chart('grafikCheckIn', {
                chart: {
                    type: 'column',
                    events: {
                        load: function () {
                            $('#loadingCheckIn').hide()
                        }
                    }
                },
                exporting: {
                    enabled: true
                },
                title: {
                    text: 'Check In Chart All Days'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: 0
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Total Participant :<b> {point.y:.0f}</b>'
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'Check In',
                    data: [
                            @foreach($checkin as $row)
                        ['{{$row->name}}', {{$row->total}}],
                        @endforeach
                    ],
                    dataLabels: {
                        enabled: true,
                        rotation: 0,
                        color: '#FFFFFF',
                        align: 'center',
                        format: '{point.y:.0f}', // one decimal
                        y: 0, // 10 pixels down from the top
                        style: {}
                    }
                }]
            });


            /**
             * Live Poll
             */
            Highcharts.chart('grafikLivePoll', {
                chart: {
                    type: 'column',
                    events: {
                        load: function () {
                            $('#loadingLivePoll').hide()
                        }
                    }
                },
                exporting: {
                    enabled: true
                },
                title: {
                    text: 'Survey Chart {{\App\Helpers\Eventy::getSetting('application_name')}}'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: ''
                    }

                },
                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.0f}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>'
                },

                series: [
                    {
                        name: "Live Poll",
                        colorByPoint: true,
                        data: [
                                @foreach($survey as $row)
                            {
                                name: "{{$row->value}}",
                                y: {{$row->total}},
                                drilldown: "{{$row->id}}"
                            },
                            @endforeach
                        ]
                    }
                ],
                drilldown: {
                    series: [
                            @foreach($survey as $row)
                        {
                            name: "{{$row->value}}",
                            id: "{{$row->id}}",
                            data: [
                                    @foreach($row->option as $xrow)
                                [
                                    "{{$xrow->value}}",
                                    {{$xrow->total}}
                                ],
                                @endforeach
                            ]
                        },
                        @endforeach
                    ]
                }
            });
        })
    </script>
@endpush