<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiMapsController extends CoreController
{
    /**
     * SEARCHING MAPS LIST
     * GET DIRECTION & DISTANCE LOCATION
     * SHOWING IMAGE
     */
    public function postIndex()
    {

        try {
            $validator['latitude'] = 'required|string|min:1|max:100';
            $validator['longitude'] = 'required|string|min:1|max:100';
            API::validator($validator);

            $latitude = Request::input('latitude');
            $longitude = Request::input('longitude');

            #MENCARI DATA MAPS
            $maps = DB::table('maps')
                ->select('id', 'name', 'image', 'location', 'latitude', 'longitude',
                    DB::raw("111.1111 *DEGREES(ACOS(COS(RADIANS(latitude))
				* COS(RADIANS(" . $latitude . "))
				* COS(RADIANS(longitude - " . $longitude . "))
				+ SIN(RADIANS(latitude))
				* SIN(RADIANS(" . $latitude . ")))) AS distance"))
                ->whereNotNull('latitude')
                ->whereNotNull('longitude')
                ->whereNull('deleted_at')
                ->get();
            foreach ($maps as $row) {
                $distance = ($row->distance > 99 ? '+99' : number_format($row->distance, 0, '.', '.'));

                $row->image = API::file($row->image);
                $row->distance = $distance . ' KM';
            }

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['item'] = $maps;

            API::Log('Maps', 'Index : ' . Request::ip());

            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Maps', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}