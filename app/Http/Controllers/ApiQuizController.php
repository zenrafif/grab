<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiQuizController extends CoreController
{

    public function postIndex()
    {
        try {
            $question = DB::table('quiz')
                ->select('id', 'name', 'status')
                ->whereNull('deleted_at')
                ->orderBy('sort', 'ASC')
                ->get();
            foreach ($question as $row) {
                $check = DB::table('quiz_answer')
                    ->whereNull('deleted_at')
                    ->where('id_quiz', $row->id)
                    ->where('id_member', $this->member->id)
                    ->count();

                if ($check) {
                    $row->status = 'History';
                } else {
                    $row->status = ($row->status == 'Yes') ? 'Start' : 'Locked';
                }

                if ($row->id == '1') {
                    $row->status = 'Review';
                } else {
                    $row->status = ($row->id == '2') ? 'Submit' : 'Locked';
                }
            }

            $total_point = DB::table('quiz_answer')
                ->where('id_member', $this->member->id)
                ->whereNull('deleted_at')
                ->sum('point');
            $total_played = DB::table('quiz_answer')
                ->where('id_member', $this->member->id)
                ->whereNull('deleted_at')
                ->count();

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'Success';
            $response['total_point'] = $total_point;
            $response['total_played'] = $total_played;
            $response['item'] = $question;

            API::Log('Quiz', 'Index : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Quiz', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postDetail()
    {
        try {
            $validator['id_quiz'] = 'required|integer';
            API::validator($validator);

            $countdown = API::getSetting('quiz_coundown');
            $id_quiz = Request::input('id_quiz');
            $correct_answer = 0;
            $wrong_answer = 0;

            $quiz = DB::table('quiz')
                ->where('id', $id_quiz)
                ->whereNull('deleted_at')
                ->first();
            if (!$quiz) {
                $response['api_status'] = 0;
                $response['api_title'] = 'Failed';
                $response['api_message'] = 'Quiz not found';
            } elseif ($quiz->status == 'No') {
                $response['api_status'] = 0;
                $response['api_title'] = 'Failed';
                $response['api_message'] = 'Quiz is locked by admin';
            } else {
                $quiz_question = DB::table('quiz_question')
                    ->where('id_quiz', $id_quiz)
                    ->whereNull('deleted_at')
                    ->get();
                if (count($quiz_question) == 0) {
                    $response['api_status'] = 0;
                    $response['api_title'] = 'Failed';
                    $response['api_message'] = 'Question not found';
                } else {
                    $quiz_answer = DB::table('quiz_answer')
                        ->whereNull('deleted_at')
                        ->where('id_quiz', $id_quiz)
                        ->where('id_member', $this->member->id)
                        ->first();
                    $quiz_answer_detail = DB::table('quiz_answer_detail')
                        ->where('id_quiz_answer',$quiz_answer->id)
                        ->whereNull('deleted_at')
                        ->count();
                    if ($quiz_answer_detail == 0) {
                        $question = DB::table('quiz_question')
                            ->select('id', 'question', 'option_a', 'option_b', 'option_c', 'option_d', 'answer')
                            ->where('id_quiz', $id_quiz)
                            ->orderBy('id', 'ASC')
                            ->whereNull('deleted_at')
                            ->get();
                        foreach ($question as $row) {
                            $row->user_answer = '';
                            $row->point = 0;
                        }
                    } else {
                        $question = DB::table('quiz_answer')
                            ->select('quiz_answer_detail.id',
                                'quiz_question.question',
                                'quiz_question.option_a',
                                'quiz_question.option_b',
                                'quiz_question.option_c',
                                'quiz_question.option_d',
                                'quiz_question.answer',
                                'quiz_answer_detail.answer as user_answer',
                                'quiz_answer_detail.point as point')
                            ->join('quiz_answer_detail', 'quiz_answer_detail.id_quiz_answer', '=', 'quiz_answer.id')
                            ->join('quiz_question', 'quiz_question.id', '=', 'quiz_answer_detail.id_quiz_question')
                            ->where('quiz_answer.id_member', $this->member->id)
                            ->where('quiz_answer.id_quiz', $id_quiz)
                            ->whereNull('quiz_answer.deleted_at')
                            ->whereNull('quiz_answer_detail.deleted_at')
                            ->orderBy('quiz_answer_detail.id', 'ASC')
                            ->get();
                        foreach ($question as $row) {
                            $json_answer = strtolower($row->answer);
                            $json_user_answer = strtolower($row->user_answer);
                            if ($json_answer == $json_user_answer) {
                                $correct_answer++;
                            } else {
                                $wrong_answer++;
                            }
                        }
                    }

                    $response['api_status'] = 1;
                    $response['api_title'] = '';
                    $response['api_message'] = 'success';
                    $response['countdown'] = (int)$countdown;
                    $response['type'] = (count($quiz_answer) > 0) ? 'History' : 'Start';
                    $response['correct_answer'] = $correct_answer;
                    $response['wrong_answer'] = $wrong_answer;
                    $response['point'] = $quiz_answer->point;
                    $response['item'] = $question;
                }
            }

            API::Log('Quiz', 'Detail : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Quiz', 'Detail Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postSubmit()
    {
        try {
            $validator['id_quiz'] = 'required|integer';
            $validator['json'] = 'required|string';
            API::validator($validator);

            $id_quiz = Request::input('id_quiz');
            $point_setting = API::getSetting('quiz_point');
            $json = Request::input('json');
            $total_point = 0;
            $total_seconds = 0;

            if (API::isJson($json)) {
                $quiz_question = DB::table('quiz_question')
                    ->where('id_quiz', $id_quiz)
                    ->whereNull('deleted_at')
                    ->pluck('id')
                    ->toArray();
                if (count($quiz_question) == 0) {
                    $response['api_status'] = 0;
                    $response['api_title'] = 'Failed';
                    $response['api_message'] = 'Question not found';
                } else {
                    $json = json_decode(Request::input('json'));
                    $id_question = true;
                    foreach ($json as $row) {
                        if (!in_array($row->id_question, $quiz_question)) {
                            $id_question = false;
                        } elseif ($row->id_question == '' || $row->answer == '' || $row->status == '' ||
                            $row->datetime_start == '' || $row->datetime_end == '') {
                            $id_question = false;
                        }
                    }

                    if (!$id_question) {
                        $response['api_status'] = 0;
                        $response['api_title'] = 'Failed';
                        $response['api_message'] = 'Json answer is invalid';
                    } else {
                        $check = DB::table('quiz_answer')
                            ->where('id_member', $this->member->id)
                            ->where('id_quiz', $id_quiz)
                            ->whereNull('deleted_at')
                            ->first();
                        if (!empty($check)) {
                            $id_quiz_answer = $check->id;
                        } else {
                            $save_answer['created_at'] = API::now();
                            $save_answer['id_member'] = $this->member->id;
                            $save_answer['id_quiz'] = $id_quiz;
                            $save_answer['point'] = 0;
                            $save_answer['seconds'] = 0;
                            $id_quiz_answer = DB::table('quiz_answer')->insertGetId($save_answer);
                        }

                        foreach ($json as $row) {
                            $check = DB::table('quiz_answer_detail')
                                ->where('id_quiz_answer', $id_quiz_answer)
                                ->where('id_quiz_question', $row->id_question)
                                ->whereNull('deleted_at')
                                ->first();
                            if (!$check) {
                                $seconds = API::Seconds($row->datetime_start, $row->datetime_end);
                                $point = (strtolower($row->status) == 'benar') ? $point_setting : 0;

                                $save['created_at'] = API::now();
                                $save['id_quiz_answer'] = $id_quiz_answer;
                                $save['id_quiz_question'] = $row->id_question;
                                $save['answer'] = $row->answer;
                                $save['point'] = $point;
                                $save['second'] = $seconds;
                                $save['datetime_start'] = $row->datetime_start;
                                $save['datetime_end'] = $row->datetime_end;
                                DB::table('quiz_answer_detail')->insert($save);

                            } else {
                                $point = $check->point;
                                $seconds = $check->seconds;
                            }

                            $total_seconds += $seconds;
                            $total_point += $point;
                        }

                        $update_answer['updated_at'] = API::now();
                        $update_answer['point'] = $total_point;
                        $update_answer['seconds'] = $total_seconds;
                        DB::table('quiz_answer')->where('id', $id_quiz_answer)->update($update_answer);

                        $response['api_status'] = 1;
                        $response['code'] = API::ServerCode();
                        $response['api_title'] = 'Quiz is Done!';
                        $response['api_message'] = 'Congratulations, you have completed quiz.';
                    }
                }

            } else {
                $response['api_status'] = 0;
                $response['api_title'] = 'Faied';
                $response['api_message'] = 'Json format is invalid';
            }

            API::Log('Quiz', 'Submit : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Quiz', 'Submit Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}
