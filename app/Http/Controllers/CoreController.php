<?php


namespace App\Http\Controllers;

use App\Helpers\API;
use App\Helpers\Eventy;
use Auth;
use DB;
use Hash;
use Input;
use Mail;
use Request;
use Route;
use Storage;
use Validator;

class CoreController extends Controller
{
    public $member;

    function __construct()
    {
        date_default_timezone_set('Asia/Makassar');


        $validator['key'] = 'required|string|min:1|max:100';
        $validator['time'] = 'required|string|min:1|max:20';
        $validator['platform'] = 'required|in:Android,IOS';
        API::validator($validator);

        $no_validator = ['auth', 'login', 'forgot-password', 'scan'];
        if (!in_array(Request::segment(2), $no_validator)) {
            $validator['id_member'] = 'required|int|min:1|max:9999999999';
            API::validator($validator);

            $id_member = Request::input('id_member');
            $member = DB::table('member')
                ->where('id', $id_member)
                ->whereNull('deleted_at')
                ->first();
            if (empty($member)) {
                $response['api_status'] = 501;
                $response['api_title'] = 'Session Limit';
                $response['api_message'] = 'Please re-login to continue';

                response()->json($response)->send();
                exit();
            }elseif($member->last_login == ''){
                $save['updated_at'] = Eventy::now();
                $save['last_login'] = Eventy::now();
                DB::table('member')->where('id',$member->id)->update($save);
            }
            $this->member = $member;
        }

        if (!API::CheckKey(Request::input('key'), Request::input('time'))) {
            $response['api_status'] = 0;
            $response['api_title'] = 'Something went wrong';
            $response['api_message'] = 'Application Key is not valid';

            response()->json($response)->send();
            exit();
        } elseif (Request::isMethod('get')) {
            $response['api_status'] = 0;
            $response['api_title'] = '405';
            $response['api_message'] = 'Method is not allowed';

            response()->json($response)->send();
            exit();
        }

        API::Log('Core', 'Constructor : ' . Request::input('platform'));
    }
}