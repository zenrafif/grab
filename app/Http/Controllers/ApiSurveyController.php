<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiSurveyController extends CoreController
{
    public function postIndex()
    {
        try {
            $question = DB::table('survey_question')
                ->whereNull('deleted_at')
                ->where('open', 'Yes')
                ->pluck('id')
                ->toArray();
            $check = DB::table('survey_answer')
                ->whereNull('deleted_at')
                ->where('id_member', $this->member->id)
                ->whereIn('id_survey_question', $question)
                ->count();

            if ($check == count($question)) {
                if ($check == 0) {
                    $response['api_status'] = 0;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = 'Empty Quiz';
                    $response['api_message'] = 'Sorry, you cannot access this right now';
                } else {
                    $response['api_status'] = 0;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = 'You have been answer the survey';
                    $response['api_message'] = 'Thanks for your participation';
                }
            } else {
                $item = DB::table('survey_question')
                    ->select('id', 'value as question')
                    ->whereNull('deleted_at')
                    ->where('open', 'Yes')
                    ->orderBy('sort', 'ASC')
                    ->get();
                foreach ($item as $row) {
                    $option = DB::table('survey_option')
                        ->select('id', 'value as option')
                        ->where('id_survey_question', $row->id)
                        ->whereNull('deleted_at')
                        ->orderBy('sort', 'ASC')
                        ->get();

                    $row->option = $option;
                }

                $response['api_status'] = 1;
                $response['code'] = API::ServerCode();
                $response['api_title'] = '';
                $response['api_message'] = (count($item) == 0 ? 'Sorry, you cannot access this right now' : 'success');
                $response['item'] = $item;
            }

            API::Log('Survey', 'Index : ' . json_encode($response));
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Survey', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postSubmit()
    {
        try {
            $validator['json'] = 'required|string';
            API::validator($validator);

            if(API::isJson(Request::input('json'))){
                $json = json_decode(Request::input('json'));
                foreach ($json as $row) {
                    $check = DB::table('survey_answer')
                        ->where('id_member', $this->member->id)
                        ->where('id_survey_question', $row->id_question)
                        ->count();
                    if (!$check) {
                        $save['created_at'] = API::now();
                        $save['id_member'] = $this->member->id;
                        $save['id_survey_question'] = $row->id_question;
                        $save['id_survey_option'] = $row->id_answer;
                        DB::table('survey_answer')->insert($save);
                    }
                }

                $response['api_status'] = 1;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Survey Has Beent Sent';
                $response['api_message'] = 'Thank you for filling out the survey. Have a nice day!';
            }else{
                $response['api_status'] = 0;
                $response['api_title'] = 'Faied';
                $response['api_message'] = 'Json format is invalid';
            }

            API::Log('Survey', 'Submit : ' . json_encode($response));
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Survey', 'Submit Exception : ' . Request::ip());
            return response()->json($response);
        }
    }


}
