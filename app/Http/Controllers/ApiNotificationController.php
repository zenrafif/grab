<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiNotificationController extends CoreController
{
    /**
     * LIST NOTIFICATION WITH PAGINATION
     */
    public function postIndex()
    {
        try {
            #CHCEK DELETED NOTIFICATION
            $notification_delete = DB::table('notification_member')
                ->where('id_member', $this->member->id)
                ->whereNull('deleted_at')
                ->where('action_type', 'Delete')
                ->pluck('id_notification')
                ->toArray();
            $notification_read = DB::table('notification_member')
                ->where('id_member', $this->member->id)
                ->whereNull('deleted_at')
                ->where('action_type', 'Read')
                ->pluck('id_notification')
                ->toArray();
            $notification = DB::table('notification')
                ->whereNotIn('id', $notification_delete)
                ->where('created_at', '>', $this->member->created_at)
                ->whereNull('deleted_at')
                ->where('is_send', 1)
                ->orderBy('id', 'DESC')
                ->paginate(20);

            #MAKE JSON LIST FOR NOTIFICATION
            $item = [];
            foreach ($notification as $row) {
                $rest['id'] = $row->id;
                $rest['created_at'] = date('d F Y, H:iA', strtotime($row->created_at));
                $rest['title'] = $row->title;
                $rest['content'] = $row->content;

                if (!in_array($row->id, $notification_read)) {
                    $save['created_at'] = API::now();
                    $save['id_notification'] = $row->id;
                    $save['id_member'] = $this->member->id;
                    $save['action_type'] = 'Read';
                    $act = DB::table('notification_member')->insert($save);
                    if ($act) {
                        API::Log('Notification', 'Read : ' . Request::ip());
                    }

                    $rest['badge'] = 'Yes';
                } else {
                    $rest['badge'] = 'No';
                }

                $item[] = $rest; //push array
            }

            if (!empty($item)) {
                $response['api_status'] = 1;
                $response['code'] = API::ServerCode();
                $response['api_title'] = '';
                $response['api_message'] = 'success';
                $response['item'] = $item;

            } else {
                $response['api_status'] = 1;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Notification is empty';
                $response['api_message'] = 'You don\'t have any notification right now';
                $response['item'] = $item;
            }

            API::Log('Notification', 'Index : ' . Request::ip());
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Notification', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postDelete()
    {
        try {
            $validator['id_notification'] = 'required|integer|min:1|max:9999999999';
            API::validator($validator);

            $id_notification = Request::input('id_notification');

            $check = DB::table('notification_member')
                ->where('id_notification', $id_notification)
                ->where('id_member', $this->member->id)
                ->where('action_type', 'Delete')
                ->whereNull('deleted_at')
                ->first();
            if (empty($check)) {
                $save['created_at'] = API::now();
                $save['id_notification'] = $id_notification;
                $save['id_member'] = $this->member->id;
                $save['action_type'] = 'Delete';
                $act = DB::table('notification_member')->insert($save);
            } else {
                $save['updated_at'] = API::now();
                $act = DB::table('notification_member')->where('id', $check->id)->update($save);
            }

            if ($act) {
                $response['api_status'] = 1;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Success';
                $response['api_message'] = 'Notification succesfully deleted';
            } else {
                $response['api_status'] = 0;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Failed';
                $response['api_message'] = 'Please try again';
            }

            API::Log('Notification', 'Delete : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e);
            API::Log('Notification', 'Delete Exception : ' . Request::ip());
            return response()->json($response);
        }
    }


    public function postRead()
    {
        try {
            $validator['id_notification'] = 'required|integer|min:1|max:9999999999';
            API::validator($validator);

            $id_notification = Request::input('id_notification');

            $check = DB::table('notification_member')
                ->where('id_notification', $id_notification)
                ->where('id_member', $this->member->id)
                ->where('action_type', 'Read')
                ->whereNull('deleted_at')
                ->first();
            if (empty($check)) {
                $save['created_at'] = API::now();
                $save['id_notification'] = $id_notification;
                $save['id_member'] = $this->member->id;
                $save['action_type'] = 'Read';
                $act = DB::table('notification_member')->insert($save);
            } else {
                $save['updated_at'] = API::now();
                $act = DB::table('notification_member')->where('id', $check->id)->update($save);
            }

            if ($act) {
                $response['api_status'] = 1;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Success';
                $response['api_message'] = 'Notification succesfully read';
            } else {
                $response['api_status'] = 0;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Failed';
                $response['api_message'] = 'Please try again';
            }

            API::Log('Notification', 'Delete : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e);
            API::Log('Notification', 'Delete Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}