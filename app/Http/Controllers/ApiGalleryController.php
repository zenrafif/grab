<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiGalleryController extends CoreController
{
    public function postIndex()
    {
        try {
            $date = DB::table('gallery_date')
                ->select('id', 'date')
                ->whereNull('deleted_at')
                ->orderBy('date', 'ASC')
                ->get();
            $i = 1;
            foreach ($date as $row) {
                $panel = DB::table('gallery_panel')
                    ->where('id_gallery_date', $row->id)
                    ->select('id', 'name')
                    ->whereNull('deleted_at')
                    ->orderBy('sort', 'ASC')
                    ->get();
                $row->panel = $panel;
                $row->id = $i++;
            }

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['item'] = $date;

            API::Log('Gallery', 'Index : ' . json_encode($response));
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Gallery', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postDetail()
    {
        try {
            $validator['id_panel'] = 'required|integer';
            $validator['page'] = 'required|integer';
            API::validator($validator);

            #VARIABLE FOR PAGINATION
            $total_image = DB::table('gallery_image')
                ->where('id_gallery_panel', Request::input('id_panel'))
                ->whereNull('deleted_at')
                ->count();

            $limit = (Request::input('platform') == 'IOS' ? $total_image : 15);
            $skip = (Request::input('page') - 1) * $limit;
            $i = 1;
            $total = DB::table('gallery_image')
                ->where('id_gallery_panel', Request::input('id_panel'))
                ->whereNull('deleted_at')
                ->count();

            #GET IMAGE
            $image = DB::table('gallery_image')
                ->where('id_gallery_panel', Request::input('id_panel'))
                ->whereNull('deleted_at')
                ->orderBy('id', 'ASC')
                ->skip($skip)
                ->take($total)
                ->get();
            $item = [];
            foreach ($image as $row) {
                $rest['id'] = $row->id;
                $rest['file'] = API::file($row->file);
                $rest['filename'] = $row->filename;
                $rest['type'] = $row->type;
                $rest['placeholder'] = API::file($row->placeholder);

                if ($i > $limit) break;
                if ($rest['file'] == '' || $rest['filename'] == '') continue;
                $item[] = $rest;

                $i++;
            }

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['item'] = $item;

            API::Log('Gallery', 'Detail : ' . json_encode($response));
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Gallery', 'detail Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}