<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiCommitteeController extends CoreController
{
    public function postIndex()
    {
        try {
            $committee = DB::table('committee')
                ->select('id', 'name', 'job_title', 'email', 'phone')
                ->whereNull('deleted_at')
                ->orderBy('sort', 'ASC')
                ->get();

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['title'] = 'Need Help? Please Contact Us!';
            $response['item'] = $committee;

            API::Log('Committee', 'Index : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Document', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}