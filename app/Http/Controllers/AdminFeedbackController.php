<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Config;
use App\Helpers\Eventy;

class AdminFeedbackController extends \crocodicstudio\crudbooster\controllers\CBController
{

    public function postUpdateQuestion()
    {
        $feedback_ratting = Request::input('feedback_ratting');
        $feedback_description = Request::input('feedback_description');

        /**
         * update ratting
         */
        $save['updated_at'] = Eventy::now();
        $save['value'] = $feedback_ratting;
        DB::table('setting')->where('type','feedback_ratting')->update($save);

        /**
         * update description
         */
        $save['updated_at'] = Eventy::now();
        $save['value'] = $feedback_description;
        DB::table('setting')->where('type','feedback_description')->update($save);

        CRUDBooster::redirect(CRUDBooster::mainpath(),'Question has been saved','success');
    }

    public function cbInit()
    {
        $segment = Request::segment(3);

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->table = "feedback";
        $this->title_field = "id";
        $this->limit = 20;
        $this->orderby = "id,desc";
        $this->show_numbering = FALSE;
        $this->global_privilege = FALSE;
        $this->button_table_action = CRUDBooster::isSuperadmin() ? TRUE : FALSE;
        $this->button_action_style = "button_icon";
        $this->button_add = FALSE;
        $this->button_delete = CRUDBooster::isSuperadmin() ? TRUE : FALSE;
        $this->button_edit = FALSE;
        $this->button_detail = FALSE;
        $this->button_show = TRUE;
        $this->button_filter = FALSE;
        $this->button_export = FALSE;
        $this->button_import = FALSE;
        $this->button_bulk_action = FALSE;
        $this->sidebar_mode = "normal"; //normal,mini,collapse,collapse-mini
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = array("label" => "Member", "name" => "id_member", "join" => "member,name", "callback" => function ($row) {
            $url = CRUDBooster::adminPath('participant/detail/' . $row->id_member);
            return '<a href="' . $url . '" target="_blank">' . $row->member_name . '</a>';
        });
        $this->col[] = array("label" => "Ratting", "name" => "ratting");
        $this->col[] = array("label" => "Description", "name" => "description");

        # END COLUMNS DO NOT REMOVE THIS LINE
        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ["label" => "Member", "name" => "id_member", "type" => "select2", "required" => TRUE, "validation" => "required|integer|min:0", "datatable" => "member,name"];
        $this->form[] = ["label" => "Ratting", "name" => "ratting", "type" => "number", "required" => TRUE, "validation" => "required|integer|min:0"];
        $this->form[] = ["label" => "Description", "name" => "description", "type" => "textarea", "required" => TRUE, "validation" => "required|string|min:5|max:5000"];
        # END FORM DO NOT REMOVE THIS LINE

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();
        if ($segment == '' && Request::get('type') == '') {
            $this->index_button[] = ['label' => 'Setting Question', 'url' => CRUDBooster::mainpath("?type=question"), "icon" => "fa fa-comment-o"];
        }


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = '
            $(document).ready(function(){
                $("#setting-question").addClass("btn-success");
                $("#setting-question").removeClass("btn-primary");
            })
        ';


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $feedback_stars = Eventy::getSetting('feedback_ratting');
        $feedback_description = Eventy::getSetting('feedback_description');
        $pre_html = '';
        if(Request::get('type') == 'question'){
            $pre_html .= '
            <form method="POST" action="' . CRUDBooster::mainpath('update-question') . '" class="box box-primary">
                <div class="box-header with-border">
                    Setting Question of Feedback
                </div>
                <div class="box-body">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="feedback_ratting"><i class="fa fa-star-o"></i> Ratting Question</label>
                            <textarea name="feedback_ratting" class="form-control" style="resize: none;" rows="5" required="">' . $feedback_stars . '</textarea>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="feedback_description"><i class="fa fa-comment-o"></i> Description Question</label>
                            <textarea name="feedback_description" class="form-control" style="resize: none;" rows="5" required="">' . $feedback_description . '</textarea>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="'.CRUDBooster::mainpath().'" class="btn btn-default">Cancel</a>
                            <button type="submit" class="btn btn-success pull-right">Submit</button>
                        </div>
                    </div>
                </div>
                
                <input type="hidden" name="_token" value="' . csrf_token() . '">
            </form>';
        }

        $this->pre_index_html = '
        
        '.$pre_html;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $css = '';
        if(Request::get('type') == 'question'){
            $css .= '
                #content_section > div.box{
                    display:none;
                }
            ';
        }
        $this->style_css = '
        
        '.$css;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}