<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Config;
use App\Helpers\Eventy;
use Excel;
use PDF;
use App;
use Storage;

class AdminReportParticipantController extends \crocodicstudio\crudbooster\controllers\CBController
{

    public function getIndex()
    {
        $company = DB::table('member')
            ->select('company')
            ->whereNull('deleted_at')
            ->whereNotNull('company')
            ->where('company', '!=', '')
            ->groupBy('company')
            ->get();

        $arr = [];
        $arr['company'] = $company;
        return view('report.participant', $arr);
    }

    public function getLoadDepartement()
    {
        $company = Request::input('company');

        $departement = DB::table('member')
            ->select('departement')
            ->whereNull('deleted_at')
            ->where('company', $company)
            ->where('departement', '!=', '')
            ->groupBy('departement')
            ->get();

        return $departement;
    }

    public function getExportExcel()
    {
        set_time_limit(60 * 60 * 24);

        $company = Request::input('company');
        $departement = Request::input('departement');
        $login = Request::input('login');
        $send_email = Request::input('send_email');
        $form = Request::input('form');
        $filename = 'Report Data Participant ' . date('d F Y');
        $type = Request::input('type');

        $member = DB::table('member')
            ->whereNull('deleted_at')
            ->where(function ($query) use ($company, $departement, $login, $send_email) {
                if ($company) {
                    $query->where('company', $company);
                    if ($departement) {
                        $query->where('departement', $departement);
                    }
                }
                if ($login == 'Yes') {
                    $query->whereNotNull('last_login');
                } elseif ($login == 'No') {
                    $query->whereNull('last_login');
                }
                if ($send_email == 'Yes') {
                    $query->where('send_email', '>', 0);
                } elseif ($send_email == 'No') {
                    $query->where('send_email', 0);
                }
            })
            ->limit(100)
            ->get();
        foreach ($member as $row) {
            $code = Eventy::QRCode($row->code);
            $image = Eventy::file($row->image);

            $row->code = $code ? '<a href="' . $code . '">Link</a>' : '';
            $row->image = $image ? '<a href="' . $image . '">Link</a>' : '';

            $row->last_login = ($row->last_login) ? date('d F Y, H:i', strtotime($row->last_login)) : '';
            $row->last_send_email = ($row->last_send_email) ? date('d F Y, H:i', strtotime($row->last_send_email)) : '';
        }

        $rest['member'] = $member;
        $rest['form'] = $form;

        try {
            switch ($type) {
                case 'PDF':
                    $view = view('export.participant', $rest)->render();
                    $pdf = App::make('dompdf.wrapper');
                    $pdf->loadHTML($view);
                    $pdf->setPaper('A4', 'landscape');
                    $pdf->stream($filename . '.pdf');

                    Storage::put('uploads/export/' . $filename . '.pdf', $pdf->output());

                    $response['api_status'] = 1;
                    $response['api_message'] = 'success';
                    $response['callback'] = url('uploads/export/' . $filename . '.pdf');
                    break;

                case 'Excel':
                    Excel::create($filename, function ($excel) use ($rest, $filename) {
                        $excel->setTitle('Data Participant')->setCreator("Eventy.ID")->setCompany(CRUDBooster::getSetting('appname'));
                        $excel->sheet('Data Participant', function ($sheet) use ($rest) {
                            $sheet->setOrientation('landscape');
                            $sheet->loadview('export.participant', $rest);
                        });
                    })->save('xls', storage_path('app/uploads/export'));

                    $response['api_status'] = 1;
                    $response['api_message'] = 'success';
                    $response['callback'] = url('uploads/export/' . $filename . '.xls');
                    break;

                case 'CSV':
                    Excel::create($filename, function ($excel) use ($rest, $filename) {
                        $excel->setTitle('Data Participant')->setCreator("Eventy.ID")->setCompany(CRUDBooster::getSetting('appname'));
                        $excel->sheet('Data Participant', function ($sheet) use ($rest) {
                            $sheet->setOrientation('landscape');
                            $sheet->loadview('export.participant', $rest);
                        });
                    })->save('csv', storage_path('app/uploads/export'));

                    $response['api_status'] = 1;
                    $response['api_message'] = 'success';
                    $response['callback'] = url('uploads/export/' . $filename . '.csv?download=true');
                    break;

                default:
                    $response['api_status'] = 0;
                    $response['api_message'] = 'Type not found';
                    break;
            }
        } catch (\Exception $e) {
            $response['api_status'] = 1;
            $response['api_message'] = $e->getCode() . ' : ' . $e->getMessage();
        }

        return response()->json($response);
    }

    public function cbInit()
    {
        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->table = "member";
        $this->title_field = "name";
        $this->limit = 20;
        $this->orderby = "id,desc";
        $this->show_numbering = FALSE;
        $this->global_privilege = FALSE;
        $this->button_table_action = TRUE;
        $this->button_action_style = "button_icon";
        $this->button_add = TRUE;
        $this->button_delete = TRUE;
        $this->button_edit = TRUE;
        $this->button_detail = TRUE;
        $this->button_show = TRUE;
        $this->button_filter = TRUE;
        $this->button_export = FALSE;
        $this->button_import = FALSE;
        $this->button_bulk_action = TRUE;
        $this->sidebar_mode = "normal"; //normal,mini,collapse,collapse-mini
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = array("label" => "Code", "name" => "code");
        $this->col[] = array("label" => "Image", "name" => "image", "image" => true);
        $this->col[] = array("label" => "Name", "name" => "name");
        $this->col[] = array("label" => "Email", "name" => "email");

        # END COLUMNS DO NOT REMOVE THIS LINE
        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ["label" => "Code", "name" => "code", "type" => "text", "required" => TRUE, "validation" => "required|min:1|max:255"];
        $this->form[] = ["label" => "Image", "name" => "image", "type" => "upload", "required" => TRUE, "validation" => "required|image|max:3000", "help" => "File types support : JPG, JPEG, PNG, GIF, BMP"];
        $this->form[] = ["label" => "Name", "name" => "name", "type" => "text", "required" => TRUE, "validation" => "required|string|min:3|max:70", "placeholder" => "You can only enter the letter only"];
        $this->form[] = ["label" => "Email", "name" => "email", "type" => "email", "required" => TRUE, "validation" => "required|min:1|max:255|email|unique:member", "placeholder" => "Please enter a valid email address"];
        $this->form[] = ["label" => "Phone", "name" => "phone", "type" => "number", "required" => TRUE, "validation" => "required|numeric", "placeholder" => "You can only enter the number only"];
        $this->form[] = ["label" => "Password", "name" => "password", "type" => "password", "required" => TRUE, "validation" => "min:3|max:32", "help" => "Minimum 5 characters. Please leave empty if you did not change the password."];
        $this->form[] = ["label" => "Company", "name" => "company", "type" => "text", "required" => TRUE, "validation" => "required|min:1|max:255"];
        $this->form[] = ["label" => "Departement", "name" => "departement", "type" => "text", "required" => TRUE, "validation" => "required|min:1|max:255"];
        $this->form[] = ["label" => "Number", "name" => "id_number", "type" => "select2", "required" => TRUE, "validation" => "required|min:1|max:255", "datatable" => "number,id"];
        $this->form[] = ["label" => "Forgot Password", "name" => "forgot_password", "type" => "text", "required" => TRUE, "validation" => "required|min:1|max:255"];
        $this->form[] = ["label" => "Last Login", "name" => "last_login", "type" => "datetime", "required" => TRUE, "validation" => "required|date_format:Y-m-d H:i:s"];

        # END FORM DO NOT REMOVE THIS LINE

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}