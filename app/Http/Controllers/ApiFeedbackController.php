<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiFeedbackController extends CoreController
{
    public function postIndex()
    {
        try {
            $ratting = API::getSetting('feedback_ratting');
            $description = API::getSetting('feedback_description');

            $check = DB::table('feedback')
                ->where('id_member', $this->member->id)
                ->whereNull('deleted_at')
                ->count();
            if ($check) {
                $response['api_status'] = 0;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'You have been sent feedback';
                $response['api_message'] = 'Thanks for your participation';
            } else {
                $response['api_status'] = 1;
                $response['code'] = API::ServerCode();
                $response['api_title'] = '';
                $response['api_message'] = 'Success';
                $response['ratting'] = $ratting;
                $response['description'] = $description;
            }

            API::Log('Feedback', 'Index : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Feedback', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postSubmit()
    {
        try {
            $validator['ratting'] = 'required';
            API::validator($validator);

            $check = DB::table('feedback')
                ->where('id_member', $this->member->id)
                ->whereNull('deleted_at')
                ->count();

            if (!$check) {
                $save['created_at'] = API::now();
                $save['id_member'] = $this->member->id;
                $save['ratting'] = Request::input('ratting');
                $save['description'] = Request::input('description');
                $act = DB::table('feedback')->insert($save);
                if ($act) {
                    $response['api_status'] = 1;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = 'Feedback Has Beent Sent';
                    $response['api_message'] = 'Thank you for giving feedback. Have a nice day!';
                } else {
                    $response['api_status'] = 0;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = 'Sent feedback failed';
                    $response['api_message'] = 'Please try again';
                }
            } else {
                $response['api_status'] = 1;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Feedback Has Beent Sent';
                $response['api_message'] = 'Thank you for giving feedback. Have a nice day!';
            }

            API::Log('Feedback', 'Submit : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Feedback', 'Submit Exception : ' . Request::ip());
            return response()->json($response);
        }
    }


}
