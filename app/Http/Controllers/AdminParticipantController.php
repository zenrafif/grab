<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Config;
use Cache;
use Excel;
use App\Helpers\Eventy;
use Crypt;

class AdminParticipantController extends \crocodicstudio\crudbooster\controllers\CBController
{
    public function getSendEmail($id)
    {
        try {
            $id = Crypt::decrypt($id);
            $member = DB::table('member')
                ->whereNull('deleted_at')
                ->where('id', $id)
                ->first();
            if (!$member) {
                CRUDBooster::redirectBack('Participant is invalid, please try again');
            } elseif ($member->email == '' || !filter_var($member->email, FILTER_VALIDATE_EMAIL)) {
                CRUDBooster::redirectBack('Participant email is invalid, please make sure email participant before sending an email');
            } elseif ($member->forgot_password == '') {
                CRUDBooster::redirectBack('Sorry, you can\'t send email after participant logged in');
            } else {
                /**
                 * data email
                 */
                $data['name'] = $member->name;
                $data['password'] = $member->forgot_password;
                $data['qrcode'] = '<a href="http://chart.googleapis.com/chart?chs=500x500&cht=qr&chl='.$member->code.'"><img style="width="200px; height="200x; margin:auto;" src="http://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.$member->code.'"></a>';

                /**
                 * create queue
                 */
                $config['to'] = $member->email;
                $config['data'] = $data;
                $config['template'] = 'send_password_user';
                $config['attachments'] = [];
                $act = CRUDBooster::sendEmail($config);
                if ($act) {
                    /**
                     * update total send_email
                     */
                    $send_email = $member->send_email + 1;
                    $last_send_email = Eventy::now();

                    $save['updated_at'] = $last_send_email;
                    $save['last_send_email'] = $last_send_email;
                    $save['send_email'] = $send_email;
                    DB::table('member')->where('id', $member->id)->update($save);

                    CRUDBooster::redirectBack('Email has been sent', 'success');
                } else {
                    CRUDBooster::redirectBack('Failed, please try again');
                }
            }

        } catch (\Exception $e) {
            CRUDBooster::redirectBack($e->getCode() . ' => ' . $e->getMessage());
        }

    }

    public function getBlastEmail()
    {
        $total_blast = DB::table('member')->sum('send_email');
        if ($total_blast > 1000) {
            CRUDBooster::redirectBack('Max limti blast email is 1000, your blast now');
        }

        $member = DB::table('member')
            ->whereNull('deleted_at')
            ->whereNotNull('email')
            ->whereNotNull('forgot_password')
            ->where('send_email', 0)
            ->whereNull('last_login')
            ->get();

        foreach ($member as $row) {
            if (!filter_var($row->email, FILTER_VALIDATE_EMAIL)) continue;

            /**
             * data email
             */
            $data['name'] = $row->name;
            $data['password'] = $row->forgot_password;

            /**
             * create queue
             */
            $config['to'] = $row->email;
            $config['data'] = $data;
            $config['template'] = 'send_password_user';
            $config['attachments'] = [];
            $config['send_at'] = date('Y-m-d H:i:s');
            $act = CRUDBooster::sendEmail($config);

            if ($act) {
                /**
                 * update total send_email
                 */
                $send_email = $row->send_email + 1;
                $last_send_email = Eventy::now();

                $save['updated_at'] = $last_send_email;
                $save['last_send_email'] = $last_send_email;
                $save['send_email'] = $send_email;

                DB::table('member')->where('id', $row->id)->update($save);
            }
        }

        CRUDBooster::redirectBack('Email has been registered in the queue. the system will send an email automatically', 'success');
    }

    public function postDoImportChunk()
    {
        if (Request::input('resume') == '') {
            set_time_limit(60 * 60); //1 hour
            ini_set('max_execution_time', 60 * 60); //1 hour

            $list_email = DB::table('member')
                ->whereNull('deleted_at')
                ->pluck('email')
                ->toArray();

            $list_code = DB::table('member')
                ->whereNull('deleted_at')
                ->pluck('code')
                ->toArray();

            $this->cbLoader();
            $file_md5 = md5(Request::get('file'));

            $select_column = Session::get('select_column');
            $select_column = array_filter($select_column);
            $table_columns = DB::getSchemaBuilder()->getColumnListing($this->table);
            $file = base64_decode(Request::get('file'));
            $file = storage_path('app/' . $file);

            $rows = Excel::load($file, function ($reader) {
            })->get();

            $data_import_column = [];
            foreach ($rows as $value) {
                $a = [];
                $skip = false;
                $err = '';

                foreach ($select_column as $sk => $s) {
                    $colname = $table_columns[$sk];

                    if ($colname == 'email') {
                        if (!filter_var($value->$s, FILTER_VALIDATE_EMAIL)) {
                            $skip = true;
                            $err .= ' | email : not email => ' . $value->$s;
                        } else if (str_replace(' ', '', $value->$s) == '') {
                            $skip = true;
                            $err .= ' | email : empty => ' . $value->$s;
                        } else if (in_array($value->$s, $list_email)) {
                            $skip = true;
                            $err .= ' | email : in array => ' . $value->$s;
                        } else {
                            $a[$colname] = $value->$s;
                            $list_email[] = $value->$s;
                        }
                    } elseif ($colname == 'name') {
                        if (str_replace(' ', '', $value->$s) == '') {
                            $skip = true;
                            $err .= ' | name => empaty ' . $value->$s;
                        } else {
                            $a[$colname] = $value->$s;
                        }
                    } else {
                        $a[$colname] = $value->$s;
                    }
                }

                if (!$skip) {
                    try {
                        $code = Eventy::RandomCode(true, 6, $list_code);
                        $list_code[] = $code;

                        $a['code'] = $code;
                        $a['created_at'] = Eventy::now();
                        $a['forgot_password'] = $code;
                        $a['send_email'] = 0;
                        $data_import_column[] = $a;

                        DB::table('member')->insert($a);
                        Cache::increment('success_' . $file_md5);
                    } catch (\Exception $e) {
                        $e = (string)$e;
                        Cache::put('error_' . $file_md5, $e, 500);
                    }
                }
            }

            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => true]);
        }
    }

    public function cbInit()
    {
        $id = Request::segment(4);
        $segment = Request::segment(3);

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->table = "member";
        $this->title_field = "name";
        $this->limit = 20;
        $this->orderby = "id,desc";
        $this->show_numbering = FALSE;
        $this->global_privilege = FALSE;
        $this->button_table_action = TRUE;
        $this->button_action_style = "button_icon";
        $this->button_add = TRUE;
        $this->button_delete = TRUE;
        $this->button_edit = TRUE;
        $this->button_detail = TRUE;
        $this->button_show = TRUE;
        $this->button_filter = FALSE;
        $this->button_export = FALSE;
        $this->button_import = TRUE;
        $this->button_bulk_action = TRUE;
        $this->sidebar_mode = "normal"; //normal,mini,collapse,collapse-mini
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = array("label" => "QrCode", "name" => "code", "callback" => function ($row) {
            $url = 'http://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=' . $row->code;
            $row->key = Crypt::encrypt($row->id);
            return '<a data-lightbox="roadtrip" rel="group_{qrcode}" title="Photo: Super Admin" href="' . $url . '">
                        <img width="auto" height="40px" src="' . $url . '"></a>';
        });
        $this->col[] = array("label" => "Image", "name" => "image", "image" => true);
        $this->col[] = array("label" => "Name", "name" => "name");
        $this->col[] = array("label" => "Email", "name" => "email");
        $this->col[] = array("label" => "Last Login", "name" => "last_login");
        $this->col[] = array("label" => "Total Send Email", "name" => "send_email");
        if (CRUDBooster::isSuperadmin()) {
            $this->col[] = array("label" => "Latest Password", "name" => "forgot_password");
        }
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $password = (Request::segment(3) == 'edit' || Request::segment(3) == 'edit-save' ? FALSE : TRUE);
        $this->form = [];
        $this->form[] = ["label" => "Image", "name" => "image", "type" => "upload", "validation" => "image|max:3000",
            "help" => "File types support : JPG, JPEG, PNG, GIF, BMP"];
        $this->form[] = ["label" => "Name", "name" => "name", "type" => "text", "required" => TRUE, "validation" => "required|string|min:3|max:70"];
        $this->form[] = ["label" => "Email", "name" => "email", "type" => "email", "required" => TRUE, "validation" => "required|min:1|max:255|email|unique:member,id," . $id . ",deleted_at,NULL"];
        $this->form[] = ["label" => "Phone", "name" => "phone", "type" => "number"];
        $this->form[] = ["label" => "Password", "name" => "forgot_password", "type" => "text", "required" => $password, "validation" => "min:3|max:32", "help" => "Minimum 5 characters. Please leave empty if you did not change the password."];
        $this->form[] = ["label" => "Company", "name" => "company", "type" => "text"];
        $this->form[] = ["label" => "Departement", "name" => "departement", "type" => "text"];
        $this->form[] = ["label" => "Last Login", "name" => "last_login", "type" => "text", "disabled" => TRUE];
        # END FORM DO NOT REMOVE THIS LINE

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();
        $this->addaction[] = ['label' => '', 'url' => CRUDBooster::mainpath('send-email/[key]'), 'icon' => 'fa fa-envelope-o', 'color' => 'info', 'showIf' => "[last_login] == '' AND forgot_password != ''"];


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();
        if ($segment == '') {
            $this->index_button[] = ['label' => 'Blast Email', 'url' => CRUDBooster::mainpath("blast-email"), "icon" => "fa fa-envelope-o", 'confirm' => true];
        }


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        //remove 4, 5, 9, 10, 13, 14, 16,
        //fix nth =
        $js = '';
        if ($segment == 'import-data') {
            $url_document = asset('document/format_import_participant.xlsx');
            $sample_format = '<br>* You can download sample format xlxs <a href=\'' . $url_document . '\'>here</a>';
            $condition_broken = '<br>* If <b>data</b> in your file is broken system will continue record to import data';
            $validation = '<br>* Please dont empty column <b>Name</b>, <b>Email</b>';
            $js .= '
                $(document).ready(function(){
                
                    let box = $("#box_main").find(".callout.callout-success");
                    box.append("' . $sample_format . '");
                    box.append("' . $condition_broken . '");
                    box.append("' . $validation . '");
                    
                    let boxTable = $("#box_main").find(".table.table-bordered");
                    
                    //remove last_send_email
                    boxTable.find("thead > tr > th:nth-child(13)").remove()
                    boxTable.find("tbody > tr > td:nth-child(13)").remove()
                    
                    //remove send_email
                    boxTable.find("thead > tr > th:nth-child(12)").remove()
                    boxTable.find("tbody > tr > td:nth-child(12)").remove()
                    
                    //remove last_login
                    boxTable.find("thead > tr > th:nth-child(11)").remove()
                    boxTable.find("tbody > tr > td:nth-child(11)").remove()
                    
                    //remove id_number
                    boxTable.find("thead > tr > th:nth-child(10)").remove()
                    boxTable.find("tbody > tr > td:nth-child(10)").remove()
                    
                    //remove forgot_password
                    boxTable.find("thead > tr > th:nth-child(7)").remove()
                    boxTable.find("tbody > tr > td:nth-child(7)").remove()
                    
                    //remove password
                    boxTable.find("thead > tr > th:nth-child(6)").remove()
                    boxTable.find("tbody > tr > td:nth-child(6)").remove()
                    
                    //remove image
                    boxTable.find("thead > tr > th:nth-child(2)").remove()
                    boxTable.find("tbody > tr > td:nth-child(2)").remove()
                    
                    //remove code
                    boxTable.find("thead > tr > th:nth-child(1)").remove()
                    boxTable.find("tbody > tr > td:nth-child(1)").remove()
                    
                    //Set Capital Name
                    boxTable.find("thead > tr > th:nth-child(1)").text("Name");
                    boxTable.find("thead > tr > th:nth-child(2)").text("Email");
                    boxTable.find("thead > tr > th:nth-child(3)").text("Phone");
                    boxTable.find("thead > tr > th:nth-child(4)").text("Company");
                    boxTable.find("thead > tr > th:nth-child(5)").text("Departement");
                })
            ';
        }
        $this->script_js = '
            $(document).ready(function(){
                $("#blast-email").addClass("btn-info");
                $("#blast-email").removeClass("btn-primary");
            })
        ' . $js;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here
        $postdata['code'] = Eventy::Code();
        $postdata['send_email'] = 0;
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here
        $participant = DB::table('member')
            ->where('id', $id)
            ->first();
        Eventy::RemoveFile($participant->image);

        $save['updated_at'] = Eventy::now();
        $save['image'] = Eventy::UploadFile('image', 'participant', $id);
        DB::table('member')->where('id', $id)->update($save);
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here
        if (Request::file('image')) {
            Eventy::RemoveFile($postdata['image']);

            $postdata['image'] = Eventy::UploadFile('image', 'participant', $id);
        }

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}