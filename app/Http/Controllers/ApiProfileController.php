<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use App\Helpers\Eventy;
use DB;
use Log;
use Hash;
use Request;

class ApiProfileController extends CoreController
{
    public function postIndex()
    {
        /**
         * CHECK EDITABLE FORM PROFILE
         * SHOWING @ TABLE SETTING
         */

        try {
            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $setting = DB::table('setting')
                ->where('type', 'LIKE', '%profile_%')
                ->get();
            foreach ($setting as $row) {
                $response[$row->type] = $row->value;
            }

            API::Log('Profile', 'Index : ' . Request::ip());
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Profile', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postEdit()
    {
        try {
//            $setting = DB::table('setting')
//                ->where('type', 'LIKE', '%profile_%')
//                ->get();
//            foreach ($setting as $row) {
//                if ($row->type == 'profile_image' || $row->type == 'profile_id_number') continue;
//                $type = str_replace('profile_', '', $row->type);
//                $validator[$type] = ($row->value == 'Yes' ? 'required|' : '') . 'string';
//            }
            $validator['name'] = 'required|string';
            $validator['email'] = 'required|string|email';
            API::validator($validator);

            $email = Request::input('email');

            #validate username using email, phone, id_number
            $validate_email = DB::table('member')
                ->whereNull('deleted_at')
                ->where('id', '!=', $this->member->id)
                ->where('email', $email)
                ->first();
            if (!empty($validate_email)) {
                $response['api_status'] = 0;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Update profile failed';
                $response['api_message'] = 'Email has been used';
            } else {
                $save['updated_at'] = API::now();
                $save['name'] = Request::input('name');
                $save['phone'] = Request::input('phone');
                $save['id_number'] = Request::input('id_number');
                $save['company'] = Request::input('company');
                $save['departement'] = Request::input('departement');
                $act = DB::table('member')->where('id', $this->member->id)->update($save);

                if ($act) {
                    $response['api_status'] = 1;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = 'Success';
                    $response['api_message'] = 'Profile has been updated';
                } else {
                    $response['api_status'] = 0;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = 'Update profile failed';
                    $response['api_message'] = 'Please try again';
                }
            }

            API::Log('Profile', 'Edit : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Profile', 'Edit Catch : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postPassword()
    {
        try {
            $validator['old_password'] = 'required|string|min:1|max:50';
            $validator['new_password'] = 'required|string|min:1|max:50';
            API::validator($validator);

            $old_password = Request::input('old_password');
            $new_password = Request::input('new_password');

            if (Hash::check($old_password, $this->member->password)) {
                $save['updated_at'] = API::now();
                $save['password'] = Hash::make($new_password);
                $act = DB::table('member')->where('id', $this->member->id)->update($save);

                if ($act) {
                    $response['api_status'] = 1;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = 'Success';
                    $response['api_message'] = 'Password has been changed';
                } else {
                    $response['api_status'] = 0;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = 'Change password failed';
                    $response['api_message'] = 'Please try again';
                }
            } else {
                $response['api_status'] = 0;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Old password is wrong';
                $response['api_message'] = 'Please try again';
            }

            API::Log('Profile', 'Password : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Profile', 'Password Catch : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postImage()
    {
        try {
            $validator['image'] = 'required|string';
            API::validator($validator);

            $image = Request::input('image');
            $image = API::uploadBase64($image, 'jpg', 'image_profile', $this->member->id);

            if ($image) {
                $save['updated_at'] = API::now();
                $save['image'] = $image;
                $act = DB::table('member')->where('id', $this->member->id)->update($save);

                if ($act) {
                    $response['api_status'] = 1;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = 'Success';
                    $response['api_message'] = 'Image profile has been changed';
                    $response['image'] = API::file($save['image']);
                } else {
                    $response['api_status'] = 0;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = 'Update image failed';
                    $response['api_message'] = 'Please try again';
                }
            } else {
                $response['api_status'] = 0;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Update image failed';
                $response['api_message'] = 'Please try again';
            }

            API::Log('Profile', 'Image : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Profile', 'Image Catch : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postSessionHistory()
    {
        try {
            $item = DB::table('scan_member')
                ->select('scan_member.created_at as datetime', 'scan_type.name')
                ->join('scan_type', 'scan_type.id', '=', 'scan_member.id_scan_type')
                ->whereNull('scan_member.deleted_at')
                ->where('scan_member.id_member', $this->member->id)
                ->orderBy('scan_member.created_at', 'ASC')
                ->get();

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['item'] = $item;

            API::Log('Profile', 'Session History : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Profile', 'Session History Catch : ' . Request::ip());
            return response()->json($response);
        }
    }
}