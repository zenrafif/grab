<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Config;
use Cache;
use Excel;
use App\Helpers\Eventy;

class AdminAgendaController extends \crocodicstudio\crudbooster\controllers\CBController
{

    public function postDoImportChunk()
    {
        if (Request::input('resume') == '') {
            set_time_limit(60 * 60); //1 hour
            ini_set('max_execution_time', 60 * 60); //1 hour

            $this->cbLoader();
            $file_md5 = md5(Request::get('file'));

            $select_column = Session::get('select_column');
            $select_column = array_filter($select_column);
            $table_columns = DB::getSchemaBuilder()->getColumnListing($this->table);
            $file = base64_decode(Request::get('file'));
            $file = storage_path('app/' . $file);

            $rows = Excel::load($file, function ($reader) {
            })->get();

            $data_import_column = [];
            foreach ($rows as $value) {
                $a = [];
                foreach ($select_column as $sk => $s) {
                    $colname = $table_columns[$sk];

                    if ($colname == 'date') {
                        $a[$colname] = date('Y-m-d', strtotime($value->$s));
                    } elseif ($colname == 'time_start') {
                        $a[$colname] = date('H:i', strtotime($value->$s));
                    } elseif ($colname == 'time_end') {
                        $a[$colname] = date('H:i', strtotime($value->$s));
                    } else {
                        $a[$colname] = $value->$s;
                    }
                }
                try {

                    $a['created_at'] = Eventy::now();

                    DB::table('agenda')->insert($a);
                    Cache::increment('success_' . $file_md5);
                } catch (\Exception $e) {
                    $e = (string)$e;
                    Cache::put('error_' . $file_md5, $e, 500);
                }
            }

            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => true]);
        }
    }

    public function cbInit()
    {
        $segment = Request::segment(3);
        $list_date = Eventy::getListDate();

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->table = "agenda";
        $this->title_field = "title";
        $this->limit = 20;
        $this->orderby = "date,asc";
        $this->show_numbering = FALSE;
        $this->global_privilege = FALSE;
        $this->button_table_action = TRUE;
        $this->button_action_style = "button_icon";
        $this->button_add = TRUE;
        $this->button_delete = TRUE;
        $this->button_edit = TRUE;
        $this->button_detail = TRUE;
        $this->button_show = TRUE;
        $this->button_filter = FALSE;
        $this->button_export = FALSE;
        $this->button_import = TRUE;
        $this->button_bulk_action = TRUE;
        $this->sidebar_mode = "normal"; //normal,mini,collapse,collapse-mini
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = array("label" => "Date", "name" => "date");
        $this->col[] = array("label" => "Time Start", "name" => "time_start");
        $this->col[] = array("label" => "Time End", "name" => "time_end");
        $this->col[] = array("label" => "Title", "name" => "title");
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ["label" => "Date", "name" => "date", "type" => "select", "dataenum" => implode(';', $list_date), "required" => TRUE, "validation" => "required|date"];
        $this->form[] = ["label" => "Time Start", "name" => "time_start", "type" => "time", "required" => TRUE,
            "validation" => "required"];
        $this->form[] = ["label" => "Time End", "name" => "time_end", "type" => "time", "required" => TRUE,
            "validation" => "required"];
        $this->form[] = ["label" => "Title", "name" => "title", "type" => "text", "required" => TRUE,
            "validation" => "required|string|min:3", "placeholder" => "You can only enter the letter only"];
        $this->form[] = ["label" => "Location", "name" => "location", "type" => "text", "required" => TRUE,
            "validation" => "required|min:1|max:255"];
        $this->form[] = ["label" => "Description", "name" => "description", "type" => "textarea"];

        $document[] = ['label' => 'Document', 'name' => 'id_document', 'type' => 'select', 'datatable' => "document,filename",
            "datatable_where" => "deleted_at is null", "readonly" => true, 'required' => true];
        $this->form[] = ['label' => 'Document', 'name' => 'agenda_document', 'type' => 'child', 'columns' => $document,
            'table' => 'agenda_document', 'foreign_key' => 'id_agenda'];

        $speaker[] = ['label' => 'Speaker', 'name' => 'id_speaker', 'type' => 'datamodal', 'datamodal_table' => "speaker",
            'datamodal_columns' => 'name,title', 'datamodal_columns_alias' => 'Name,Title', 'datamodal_where' => 'deleted_at is null',
            "readonly" => true, 'required' => true];
        $this->form[] = ['label' => 'Speaker', 'name' => 'agenda_speaker', 'type' => 'child', 'columns' => $speaker,
            'table' => 'agenda_speaker', 'foreign_key' => 'id_agenda'];
        # END FORM DO NOT REMOVE THIS LINE

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $js = '';
        if ($segment == 'import-data') {
            $url_document = asset('document/format_import_agenda.xlsx');
            $sample_format = '<br>* You can download sample format xlxs <a href=\'' . $url_document . '\'>here</a>';
            $condition_broken = '<br>* If <b>data</b> in your file is broken system will continues recrod to import data';
            $validation = '<br>* Please dont empty column <b>Date</b>, <b>Time Start</b>, <b>Title</b>, <b>Location</b>';
            $js .= '
                $(document).ready(function(){
                    let box = $("#box_main").find(".callout.callout-success");
                    box.append("' . $sample_format . '");
                    box.append("' . $condition_broken . '");
                    box.append("' . $validation . '");
                    
                    let boxTable = $("#box_main").find(".table.table-bordered");
                    boxTable.find("thead > tr > th:nth-child(1)").text("Date")
                    boxTable.find("thead > tr > th:nth-child(2)").text("Time Start")
                    boxTable.find("thead > tr > th:nth-child(3)").text("Time End")
                    boxTable.find("thead > tr > th:nth-child(4)").text("Title")
                    boxTable.find("thead > tr > th:nth-child(5)").text("Location")
                    boxTable.find("thead > tr > th:nth-child(6)").text("Description")
                })
            ';
        }
        $this->script_js = '
            
        ' . $js;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here
        $query->orderBy('date', 'ASC');
        $query->orderBy('time_start', 'ASC');
        $query->orderBy('time_end', 'ASC');
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}