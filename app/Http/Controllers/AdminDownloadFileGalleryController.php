<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use Config;
use App\Helpers\Eventy;
use Storage;

class AdminDownloadFileGalleryController extends \crocodicstudio\crudbooster\controllers\CBController
{

    private $gallery;
    private $export;
    private $banner;
    private $document;
    private $image_profile;
    private $image_speaker;
    private $maps;
    private $setting;

    public function getIndex()
    {
        $this->checkGallery();
        $this->checkExport();
        $this->checkBanner();
        $this->checkDocument();
        $this->checkImageProfile();
        $this->checkImageSpeaker();
        $this->checkMaps();
        $this->checkSetting();

        $data = [];
        $data[] = $this->gallery;
        $data[] = $this->export;
        $data[] = $this->banner;
        $data[] = $this->document;
        $data[] = $this->image_profile;
        $data[] = $this->image_speaker;
        $data[] = $this->maps;
        $data[] = $this->setting;

        $arr['data'] = $data;
        return view('admin.downloadfilegallery', $arr);
    }

    public function checkSetting()
    {
        $check_export = DB::table('zip_action')
            ->where('type', 'setting')
            ->first();
        $filename = 'Setting';
        $file_path = 'uploads/downloads/' . $filename . '.zip';

        Eventy::ZipDirectory($filename, 'setting');
        $save['updated_at'] = Eventy::now();
        $save['type'] = 'setting';
        if (!$check_export) {
            $save['created_at'] = Eventy::now();
            DB::table('zip_action')->insert($save);
        } else {
            DB::table('zip_action')->where('id', $check_export->id)->update($save);
        }

        $push['type'] = 'Setting';
        $push['filename'] = $filename . '.zip';
        $push['filesize'] = Eventy::formatBytes((!file_exists(storage_path('app/' . $file_path)) ? 0 : Storage::size($file_path)));
        $push['last_update'] = Eventy::now();
        $push['url'] = asset($file_path);
        $this->setting = json_decode(json_encode($push));
    }

    public function checkMaps()
    {
        $maps = DB::table('maps')
            ->whereNull('deleted_at')
            ->orderBy('updated_at', 'DESC')
            ->first();
        if (!$maps) {
            $this->maps = null;
        } else {
            $check_maps = DB::table('zip_action')
                ->where('type', 'maps')
                ->first();

            $filename = 'Maps';
            $file_path = 'uploads/downloads/' . $filename . '.zip';

            if ($maps->updated_at != $check_maps->updated_at) {
                Eventy::ZipDirectory($filename, 'maps');
                $save['updated_at'] = $maps->updated_at;
                $save['type'] = 'maps';
                if (!$check_maps) {
                    $save['created_at'] = $maps->updated_at;
                    DB::table('zip_action')->insert($save);
                } else {
                    DB::table('zip_action')->where('id', $check_maps->id)->update($save);
                }
            }

            $push['type'] = 'Maps';
            $push['filename'] = $filename . '.zip';
            $push['filesize'] = Eventy::formatBytes((!file_exists(storage_path('app/' . $file_path)) ? 0 : Storage::size($file_path)));
            $push['last_update'] = $maps->updated_at;
            $push['url'] = asset($file_path);
            $this->maps = json_decode(json_encode($push));
        }
    }

    public function checkImageSpeaker()
    {
        $speaker = DB::table('speaker')
            ->whereNull('deleted_at')
            ->orderBy('updated_at', 'DESC')
            ->first();
        if (!$speaker) {
            $this->image_speaker = null;
        } else {
            $check_speaker = DB::table('zip_action')
                ->where('type', 'image_speaker')
                ->first();
            $filename = 'ImageSpeaker';
            $file_path = 'uploads/downloads/' . $filename . '.zip';

            if ($speaker->updated_at != $check_speaker->updated_at) {
                Eventy::ZipDirectory($filename, 'image_speaker');
                $save['updated_at'] = $speaker->updated_at;
                $save['type'] = 'image_speaker';
                if (!$check_speaker) {
                    $save['created_at'] = $speaker->updated_at;
                    DB::table('zip_action')->insert($save);
                } else {
                    DB::table('zip_action')->where('id', $check_speaker->id)->update($save);
                }
            }

            $push['type'] = 'Speaker';
            $push['filename'] = $filename . '.zip';
            $push['filesize'] = Eventy::formatBytes((!file_exists(storage_path('app/' . $file_path)) ? 0 : Storage::size($file_path)));
            $push['last_update'] = $speaker->updated_at;
            $push['url'] = asset($file_path);
            $this->image_speaker = json_decode(json_encode($push));
        }
    }

    public function checkImageProfile()
    {
        $image_profile = DB::table('member')
            ->whereNull('deleted_at')
            ->orderBy('updated_at', 'DESC')
            ->first();
        if (!$image_profile) {
            $this->image_profile = null;
        } else {
            $check_image_profile = DB::table('zip_action')
                ->where('type', 'image_profile')
                ->first();
            $filename = 'ImageParticipant';
            $file_path = 'uploads/downloads/' . $filename . '.zip';

            if ($image_profile->updated_at != $check_image_profile->updated_at) {
                Eventy::ZipDirectory($filename, 'image_profile');
                $save['updated_at'] = $image_profile->updated_at;
                $save['type'] = 'image_profile';
                if (!$check_image_profile) {
                    $save['created_at'] = $image_profile->updated_at;
                    DB::table('zip_action')->insert($save);
                } else {
                    DB::table('zip_action')->where('id', $check_image_profile->id)->update($save);
                }
            }

            $push['type'] = 'Participant';
            $push['filename'] = $filename . '.zip';
            $push['filesize'] = Eventy::formatBytes((!file_exists(storage_path('app/' . $file_path)) ? 0 : Storage::size($file_path)));
            $push['last_update'] = $image_profile->updated_at;
            $push['url'] = asset($file_path);
            $this->document = json_decode(json_encode($push));
        }
    }

    public function checkDocument()
    {
        $document = DB::table('document')
            ->whereNull('deleted_at')
            ->orderBy('id', 'DESC')
            ->first();
        if (!$document) {
            $this->document = null;
        } else {
            $check_document = DB::table('zip_action')
                ->where('type', 'document')
                ->first();
            $filename = 'Document';
            $file_path = 'uploads/downloads/' . $filename . '.zip';

            if ($document->created_at != $check_document->updated_at) {
                Eventy::ZipDirectory($filename, 'document');
                $save['updated_at'] = $document->created_at;
                $save['type'] = 'document';
                if (!$check_document) {
                    $save['created_at'] = $document->created_at;
                    DB::table('zip_action')->insert($save);
                } else {
                    DB::table('zip_action')->where('id', $check_document->id)->update($save);
                }
            }

            $push['type'] = 'Document';
            $push['filename'] = $filename . '.zip';
            $push['filesize'] = Eventy::formatBytes((!file_exists(storage_path('app/' . $file_path)) ? 0 : Storage::size($file_path)));
            $push['last_update'] = $document->created_at;
            $push['url'] = asset($file_path);
            $this->document = json_decode(json_encode($push));
        }
    }

    public function checkBanner()
    {
        $slider = DB::table('slider')
            ->whereNull('deleted_at')
            ->orderBy('id', 'DESC')
            ->first();
        if (!$slider) {
            $this->slider = null;
        } else {
            $check_slider = DB::table('zip_action')
                ->where('type', 'slider')
                ->first();
            $filename = 'Banner';
            $file_path = 'uploads/downloads/' . $filename . '.zip';

            if ($slider->created_at != $check_slider->updated_at) {
                Eventy::ZipDirectory($filename, 'banner');
                $save['updated_at'] = $slider->created_at;
                $save['type'] = 'slider';
                if (!$check_slider) {
                    $save['created_at'] = $slider->created_at;
                    DB::table('zip_action')->insert($save);
                } else {
                    DB::table('zip_action')->where('id', $check_slider->id)->update($save);
                }
            }

            $push['type'] = 'Banner';
            $push['filename'] = $filename . '.zip';
            $push['filesize'] = Eventy::formatBytes((!file_exists(storage_path('app/' . $file_path)) ? 0 : Storage::size($file_path)));
            $push['last_update'] = $slider->created_at;
            $push['url'] = asset($file_path);
            $this->banner = json_decode(json_encode($push));
        }
    }

    public function checkExport()
    {
        $check_export = DB::table('zip_action')
            ->where('type', 'export')
            ->first();
        $filename = 'Export';
        $file_path = 'uploads/downloads/' . $filename . '.zip';

        Eventy::ZipDirectory($filename, 'export');
        $save['updated_at'] = Eventy::now();
        $save['type'] = 'export';
        if (!$check_export) {
            $save['created_at'] = Eventy::now();
            DB::table('zip_action')->insert($save);
        } else {
            DB::table('zip_action')->where('id', $check_export->id)->update($save);
        }

        $push['type'] = 'Export';
        $push['filename'] = $filename . '.zip';
        $push['filesize'] = Eventy::formatBytes((!file_exists(storage_path('app/' . $file_path)) ? 0 : Storage::size($file_path)));
        $push['last_update'] = Eventy::now();
        $push['url'] = asset($file_path);
        $this->export = json_decode(json_encode($push));
    }

    public function checkGallery()
    {
        $gallery = DB::table('gallery_image')
            ->whereNull('deleted_at')
            ->orderBy('id', 'DESC')
            ->first();
        if (!$gallery) {
            $this->gallery = null;
        } else {
            $check_gallery = DB::table('zip_action')
                ->where('type', 'gallery')
                ->first();
            $filename = 'Gallery';
            $file_path = 'uploads/downloads/' . $filename . '.zip';

            if ($gallery->created_at != $check_gallery->updated_at) {
                Eventy::ZipDirectory($filename, 'gallery', 'public_path');
                $save['updated_at'] = $gallery->created_at;
                $save['type'] = 'gallery';
                if (!$check_gallery) {
                    $save['created_at'] = $gallery->created_at;
                    DB::table('zip_action')->insert($save);
                } else {
                    DB::table('zip_action')->where('id', $check_gallery->id)->update($save);
                }
            }

            $push['type'] = 'Gallery';
            $push['filename'] = $filename . '.zip';
            $push['filesize'] = Eventy::formatBytes((!file_exists(storage_path('app/' . $file_path)) ? 0 : Storage::size($file_path)));
            $push['last_update'] = $gallery->created_at;
            $push['url'] = asset($file_path);
            $this->gallery = json_decode(json_encode($push));
        }
    }

    public function cbInit()
    {
        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->table = "cms_users";
        $this->title_field = "name";
        $this->limit = 20;
        $this->orderby = "id,desc";
        $this->show_numbering = FALSE;
        $this->global_privilege = FALSE;
        $this->button_table_action = TRUE;
        $this->button_action_style = "button_icon";
        $this->button_add = TRUE;
        $this->button_delete = TRUE;
        $this->button_edit = TRUE;
        $this->button_detail = TRUE;
        $this->button_show = TRUE;
        $this->button_filter = TRUE;
        $this->button_export = FALSE;
        $this->button_import = FALSE;
        $this->button_bulk_action = TRUE;
        $this->sidebar_mode = "normal"; //normal,mini,collapse,collapse-mini
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = array("label" => "Name", "name" => "name");
        $this->col[] = array("label" => "Photo", "name" => "photo", "image" => true);
        $this->col[] = array("label" => "Email", "name" => "email");
        $this->col[] = array("label" => "Privileges", "name" => "id_cms_privileges", "join" => "cms_privileges,name");

        # END COLUMNS DO NOT REMOVE THIS LINE
        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ["label" => "Name", "name" => "name", "type" => "text", "required" => TRUE, "validation" => "required|string|min:3|max:70", "placeholder" => "You can only enter the letter only"];
        $this->form[] = ["label" => "Photo", "name" => "photo", "type" => "upload", "required" => TRUE, "validation" => "required|image|max:3000", "help" => "File types support : JPG, JPEG, PNG, GIF, BMP"];
        $this->form[] = ["label" => "Email", "name" => "email", "type" => "email", "required" => TRUE, "validation" => "required|min:1|max:255|email|unique:cms_users", "placeholder" => "Please enter a valid email address"];
        $this->form[] = ["label" => "Password", "name" => "password", "type" => "password", "required" => TRUE, "validation" => "min:3|max:32", "help" => "Minimum 5 characters. Please leave empty if you did not change the password."];
        $this->form[] = ["label" => "Cms Privileges", "name" => "id_cms_privileges", "type" => "select2", "required" => TRUE, "validation" => "required|integer|min:0", "datatable" => "cms_privileges,name"];
        $this->form[] = ["label" => "Status", "name" => "status", "type" => "text", "required" => TRUE, "validation" => "required|min:1|max:255"];

        # END FORM DO NOT REMOVE THIS LINE

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}