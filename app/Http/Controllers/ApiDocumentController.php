<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiDocumentController extends CoreController
{
    public function postIndex()
    {
        try {
            $list_extension = ['docx', 'pdf', 'ppt', 'pptx', 'xls', 'xlsx'];
            $document = DB::table('document')
                ->select('document.id', 'document.file', 'document.filename', 'document.filesize', 'document.extension')
                ->whereNull('document.deleted_at')
                ->orderBy('sort', 'ASC')
                ->get();

            $item = [];
            foreach ($document as $row) {
                $row->file = API::file($row->file);
                $row->filesize = API::formatBytes($row->filesize);

                if ($row->file != '' && in_array($row->extension, $list_extension)) {
                    $item[] = $row;
                }
            }

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['item'] = $item;

            API::Log('Document', 'Index : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Document', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}