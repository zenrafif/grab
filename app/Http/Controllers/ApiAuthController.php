<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use DB;
use Log;
use Request;

class ApiAuthController extends CoreController
{
    public function postIndex()
    {
        try {
            $auth = DB::table('auth')
                ->select('type', 'value')
                ->get();
            $module = API::ListModule();

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['version_android'] = '1.0.0';
            $response['ios_version'] = '1.0';
            $response['redirect_title'] = 'Version Update';
            $response['redirect_message'] = 'Please update your application first';
            $response['text_login'] = "<center>Please <b>Login</b> to continue to your <b>event</b></center>";
            $response['text_forgot_password'] = '<center>Please enter your email to get a new password</center>';

            /**
             * looping from auth table
             */
            $list_image = ['splashscreen', 'logo'];
            foreach ($auth as $row) {
                $type = $row->type;
                $value = $row->value;

                if (in_array($type, $list_image)) {
                    $value = API::file($value);
                }

                $response[$type] = $value;
            }
            $response['module'] = $module;

            API::Log('Auth', 'Index : ' . Request::ip());
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Auth', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postForgotPassword()
    {
        try {
            $validator['email'] = 'required|email|min:1|max:255';
            API::validator($validator);

            $email = Request::input('email');

            $member = DB::table('member')
                ->where('email', $email)
                ->whereNull('deleted_at')
                ->first();

            if (empty($member)) {
                $response['api_status'] = 0;
                $response['api_title'] = 'Failed';
                $response['api_message'] = 'Email not found';
            } else {
                if ($member->forgot_password != ''){
                    $password = $member->forgot_password;
                }else{
                    $password = API::RandomString();

                    $save['updated_at'] = API::now();
                    $save['forgot_password'] = $password;
                    DB::table('member')->where('id',$member->id)->update($save);
                }

                $data = ['password' => $password];
                $send_email = CRUDBooster::sendEmail(['to' => $member->email, 'data' => $data, 'template' => 'forgot_password_apps']);

                if ($send_email){
                    $response['api_status'] = 1;
                    $response['api_title'] = 'Success, New password has been sent';
                    $response['api_message'] = 'Please check your email and login with new password';
                }else{
                    $response = API::failed('Cannot send email');
                }
            }

            API::Log('Auth', 'Forgot Password : ' . Request::ip());
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Auth', 'Forgot Password Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}