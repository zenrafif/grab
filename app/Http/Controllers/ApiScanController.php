<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiScanController extends CoreController
{
    public function postIndex()
    {
        $validator['qrcode'] = 'required';
        $validator['type'] = 'required';
        API::validator($validator);

        $type = Request::input('type');
        $qrcode = Request::input('qrcode');

        if ($type == 'Location') {
            $validator['id_scan_type'] = 'required';
            API::validator($validator);

            $id_scan_type = Request::input('id_scan_type');
            $member = DB::table('member')
                ->where('code', $qrcode)
                ->first();

            if ($member->deleted_at != '') {
                $response['api_status'] = 501;
                $response['api_title'] = 'Session Limit';
                $response['api_message'] = 'Please re-login to continue';
            } else {
                $check = DB::table('scan_member')
                    ->where('id_member', $member->id)
                    ->where('id_scan_type', $id_scan_type)
                    ->whereNull('deleted_at')
                    ->count();

                if ($check > 0) {
                    $response['api_status'] = 0;
                    $response['api_title'] = 'Failed';
                    $response['api_message'] = 'You have been scanned';
                } else {
                    $save['created_at'] = API::now();
                    $save['id_member'] = $member->id;
                    $save['id_scan_type'] = $id_scan_type;
                    DB::table('scan_member')->insert($save);

                    $response['api_status'] = 1;
                    $response['api_title'] = '';
                    $response['api_message'] = 'success';
                    $response['image'] = API::file($member->image);
                    $response['name'] = $member->name;
                }
            }

        } elseif ($type == 'Voucher') {
            $voucher_member = DB::table('voucher_member')
                ->select('voucher_member.id as id',
                    'member.image as image',
                    'member.name as name',
                    'voucher_member.redeem')
                ->join('member', 'member.id', '=', 'voucher_member.id_member')
                ->where('voucher_member.qrcode', $qrcode)
                ->whereNull('voucher_member.deleted_at')
                ->first();
            if (empty($voucher_member)) {
                $response['api_status'] = 0;
                $response['api_title'] = 'Failed';
                $response['api_message'] = 'Your voucher is not found';
            } elseif ($voucher_member->redeem == 'Yes') {
                $response['api_status'] = 0;
                $response['api_title'] = 'Failed';
                $response['api_message'] = 'Your voucher has been scanned';
            } else {
                $save['updated_at'] = API::now();
                $save['datetime_claim'] = API::now();
                $save['redeem'] = 'Yes';
                DB::table('voucher_member')->where('id', $voucher_member->id)->update($save);

                $data['title'] = 'Voucher has been scanned';
                $data['content'] = 'Thankyou for participation';
                $data['type'] = 3;
                $data['created_at'] = date('d F Y, H:iA', strtotime(API::now()));
                $regid = DB::table('member_regid')->pluck('token');
                API::SendFCM($regid, $data);

                $response['api_status'] = 1;
                $response['api_title'] = '';
                $response['api_message'] = 'success';
                $response['image'] = API::file($voucher_member->image);
                $response['name'] = $voucher_member->name;
            }
        } else {
            $response['api_status'] = 1;
            $response['api_title'] = '';
            $response['api_message'] = 'Success';
        }
        API::Log('Scan', 'Submit : ' . json_encode($response) . ' | ' . json_encode(Request::all()));
        return response()->json($response);
    }

    public function postType()
    {
        $scan_type = DB::table('scan_type')
            ->select('id', 'name')
            ->whereNull('deleted_at')
            ->orderBy('id', 'ASC')
            ->get();

        $response['api_status'] = 1;
        $response['api_title'] = '';
        $response['api_message'] = 'Success';
        $response['item'] = $scan_type;

        API::Log('Scan', 'Index : ' . Request::ip());
        return response()->json($response);
    }
}