<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Request;
use Hash;
use crocodicstudio\crudbooster\helpers\CRUDBooster;

class ApiQnaController extends CoreController
{
    public function postIndex()
    {
        try {
            $check = DB::table('qna')
                ->select('qna.id', 'qna.id_speaker', 'qna.created_at', 'qna.updated_at', 'speaker.name', 'speaker.title')
                ->join('speaker', 'speaker.id', '=', 'qna.id_speaker')
                ->where('qna.id_member', $this->member->id)
                ->whereNull('qna.deleted_at')
                ->whereNull('speaker.deleted_at')
                ->orderBy('updated_at', 'DESC')
                ->get();
            $item = [];
            foreach ($check as $row) {
                $last_message = DB::table('qna_detail')
                    ->orderBy('id', 'DESC')
                    ->where('id_qna', $row->id)
                    ->whereNull('deleted_at')
                    ->first();
                if (empty($last_message)) {
                    $time = date('H:i', ($row->updated_at == '' ? strtotime($row->created_at) : strtotime($row->updated_at)));
                } else {
                    if (date('Y-m-d') == date('Y-m-d', strtotime($last_message->created_at))) {
                        $time = date('H:i', strtotime($last_message->created_at));
                    } else {
                        $time = date('M d,Y H:i', strtotime($last_message->created_at));
                    }
                }

                if (@$last_message == '') continue;

                $push['id'] = $row->id;
                $push['id_speaker'] = $row->id_speaker;
                $push['time'] = $time;
                $push['name'] = $row->name;
                $push['title'] = $row->title;
                $push['last_message'] = (@$last_message->message == '' ? '' : @$last_message->message);

                $item[] = $push;
            }

            if (count($item) > 0) {
                $rest['api_status'] = 1;
                $rest['api_title'] = '';
                $rest['api_message'] = 'success';
                $rest['item'] = $item;
            } else {
                $rest['api_status'] = 0;
                $rest['api_title'] = 'Empty';
                $rest['api_message'] = 'QnA is empty';
                $rest['item'] = $item;
            }

            API::Log('QnA', 'Index : ' . Request::ip());
            return response()->json($rest);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('QnA', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postDetail()
    {
        try {
            $validator['id_speaker'] = 'required|integer';
            $validator['last_id'] = 'required|integer';
            API::validator($validator);

            $last_id = Request::input('last_id');
            $id_speaker = Request::input('id_speaker');
            $item = [];

            $qna = DB::table('qna')
                ->select('qna.id', 'qna.id_speaker', 'qna.created_at', 'qna.updated_at', 'speaker.name', 'speaker.title')
                ->join('speaker', 'speaker.id', '=', 'qna.id_speaker')
                ->where('qna.id_member', $this->member->id)
                ->where('qna.id_speaker', $id_speaker)
                ->whereNull('qna.deleted_at')
                ->whereNull('speaker.deleted_at')
                ->orderBy('updated_at', 'DESC')
                ->first();

            if (empty($qna)) {
                $speaker = DB::table('speaker')
                    ->where('id', $id_speaker)
                    ->first();

                $save['created_at'] = API::now();
                $save['updated_at'] = API::now();
                $save['id_member'] = $this->member->id;
                $save['id_speaker'] = $id_speaker;
                $act = DB::table('qna')->insertGetId($save);

                if ($act) {
                    $rest['api_status'] = 1;
                    $rest['api_title'] = '';
                    $rest['api_message'] = 'success';
                    $rest['name'] = $speaker->name;
                    $rest['title'] = $speaker->title;
                    $rest['item'] = $item;
                } else {
                    $rest['api_status'] = 0;
                    $rest['api_title'] = 'Failed';
                    $rest['api_message'] = 'Please try again';
                    $rest['name'] = $speaker->name;
                    $rest['title'] = $speaker->title;
                    $rest['item'] = $item;
                }
            } else {
                $detail = DB::table('qna_detail')
                    ->where('id_qna', $qna->id)
                    ->where(function ($q) use ($last_id) {
                        if ($last_id != 0 && $last_id > 0) {
                            $q->where('id', '<', $last_id);
                        }
                    })
                    ->orderBy('id', 'DESC')
                    ->limit(20)
                    ->pluck('id')
                    ->toArray();

                $detail = DB::table('qna_detail')
                    ->whereIn('id', $detail)
                    ->orderBy('id', 'ASC')
                    ->get();
                foreach ($detail as $row) {
                    if (date('Y-m-d') == date('Y-m-d', strtotime($row->created_at))) {
                        $datetime = date('H:i', strtotime($row->created_at));
                    } else {
                        $datetime = date('M d,Y H:i', strtotime($row->created_at));
                    }

                    $push['id'] = $row->id;
                    $push['position'] = ($row->type == 'Member' ? 'Right' : 'Left');
                    $push['datetime'] = $datetime;
                    $push['message'] = $row->message;

                    $item[] = $push;
                }

                $rest['api_status'] = 1;
                $rest['api_title'] = '';
                $rest['api_message'] = 'success';
                $rest['name'] = $qna->name;
                $rest['title'] = $qna->title;
                $rest['item'] = $item;
            }

            API::Log('QnA', 'Detail : ' . Request::ip());
            return response()->json($rest);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('QnA', 'Detail Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postAdd()
    {
        try {
            $validator['id_speaker'] = 'required|integer';
            $validator['message'] = 'required|string|min:1|max:255';
            API::validator($validator);

            $id_speaker = Request::input('id_speaker');
            $message = Request::input('message');

            $check = DB::table('qna')
                ->where('id_member', $this->member->id)
                ->where('id_speaker', $id_speaker)
                ->first();
            if (empty($check)) {
                $save['created_at'] = API::now();
                $save['updated_at'] = API::now();
                $save['id_member'] = $this->member->id;
                $save['id_speaker'] = $id_speaker;
                $act = DB::table('qna')->insertGetId($save);

                if ($act) {
                    $id_qna = $act;
                } else {
                    $rest['api_status'] = 0;
                    $rest['api_title'] = 'Failed';
                    $rest['api_message'] = 'Please try again';
                    return response()->json($rest);
                    exit();
                }
            } else {
                $id_qna = $check->id;
            }

            $save['created_at'] = API::now();
            $save['id_qna'] = $id_qna;
            $save['message'] = $message;
            $save['type'] = 'Member';
            $act = DB::table('qna_detail')->insert($save);

            $config['content'] = $this->member->name . ' - ' . substr($message, 0, 50);
            $config['to'] = CRUDBooster::adminPath('qna/detail/' . $id_qna);
            $config['id_cms_users'] = DB::table('cms_users')->pluck('id')->toArray();
            CRUDBooster::sendNotification($config);

            if ($act) {
                $rest['api_status'] = 1;
                $rest['api_title'] = '';
                $rest['api_message'] = 'success';
            } else {
                $rest['api_status'] = 0;
                $rest['api_title'] = 'Failed';
                $rest['api_message'] = 'Please try again';
            }

            API::Log('QnA', 'Add : ' . Request::ip());
            return response()->json($rest);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('QnA', 'Add Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postAdmin()
    {
        try {
            $validator['id_speaker'] = 'required';
            $validator['message'] = 'required|string|min:1|max:255';
            API::validator($validator);

            $id_speaker = Request::input('id_speaker');
            $message = Request::input('message');

            $check = DB::table('qna')
                ->where('id_member', $this->member->id)
                ->where('id_speaker', $id_speaker)
                ->first();
            if (empty($check)) {
                $rest['api_status'] = 0;
                $rest['api_title'] = 'Failed';
                $rest['api_message'] = 'Please try again';
                return response()->json($rest);
                exit();
            } else {
                $id_qna = $check->id;
            }

            $save['created_at'] = API::now();
            $save['id_qna'] = $id_qna;
            $save['message'] = $message;
            $save['type'] = 'Admin';
            $act = DB::table('qna_detail')->insert($save);

            if ($act) {

                $speaker = DB::table('speaker')
                    ->where('id', $id_speaker)
                    ->first();

                $regid = DB::table('member_regid')
                    ->where('id_member', $this->member->id)
                    ->whereNull('deleted_at')
                    ->pluck('token');

                $item['id'] = $id_qna;
                $item['id_speaker'] = $id_speaker;
                $item['message'] = $message;
                $item['position'] = 'Left';
                $item['datetime'] = date('H:i', strtotime($save['created_at']));

                $data['title'] = $speaker->name;
                $data['message'] = $message;
                $data['content'] = $message;
                $data['name'] = $speaker->name;
                $data['type'] = 4;
                $data['item'] = $item;
                $data['position'] = $speaker->title;

                $result = json_decode(API::SendFCM($regid, $data));

                $rest['api_status'] = 1;
                $rest['api_title'] = '';
                $rest['api_message'] = 'success';
                $rest['result'] = $result;
            } else {
                $rest['api_status'] = 0;
                $rest['api_title'] = 'Failed';
                $rest['api_message'] = 'Please try again';
            }

            API::Log('QnA', 'Admin : ' . Request::ip());
            return response()->json($rest);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('QnA', 'Admin Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}