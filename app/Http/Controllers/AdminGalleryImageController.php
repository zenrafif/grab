<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Config;
use Storage;
use File;
use App\Helpers\Eventy;

class AdminGalleryImageController extends \crocodicstudio\crudbooster\controllers\CBController
{

    public function cbInit()
    {
        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->table = "gallery_image";
        $this->title_field = "filename";
        $this->limit = 20;
        $this->orderby = "id,descactionButtonSelected";
        $this->show_numbering = FALSE;
        $this->global_privilege = FALSE;
        $this->button_table_action = TRUE;
        $this->button_action_style = "button_icon";
        $this->button_add = TRUE;
        $this->button_delete = TRUE;
        $this->button_edit = FALSE;
        $this->button_detail = FALSE;
        $this->button_show = TRUE;
        $this->button_filter = FALSE;
        $this->button_export = FALSE;
        $this->button_import = FALSE;
        $this->button_bulk_action = TRUE;
        $this->sidebar_mode = "normal"; //normal,mini,collapse,collapse-mini
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = array("label" => "File", "name" => "placeholder", "image" => true, "callback" => function ($row) {
            $placeholder = url($row->placeholder);
            $file = url($row->file);

            if ($row->type == 'image') {
                $html = '<a data-lightbox="roadtrip" rel="group_{gallery_image}" title="File: " href="' . $placeholder . '">
                <img width="auto" height="40px" src="' . $placeholder . '">
                </a>';
            } else {
                $html = '<a title="File: " href="' . $file . '" target="_blank">
                <img width="auto" height="40px" src="' . $placeholder . '">
                </a>';
            }
            return $html;
        });
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ["label" => "Gallery Panel", "name" => "id_gallery_panel", "type" => "select2", "required" => TRUE, "validation" => "required|integer|min:0", "datatable" => "gallery_panel,name"];
        $this->form[] = ["label" => "File", "name" => "file", "type" => "upload", "required" => TRUE, "validation" => "required", "validation" => "required|mimes:jpg,jpeg,png,mp4,zip", "help" => "Please make sure extension JPG, JPEG, PNG, MP4 (nb : You can upload multiple file using zip file)"];

        # END FORM DO NOT REMOVE THIS LINE

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here
        $query->addSelect('file');
        $query->addSelect('type');
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here
        $file = Request::file('file');
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $list_image = ['jpg', 'jpeg', 'png'];
        $list_video = ['mp4'];

        if ($extension == 'zip') {
            /**
             * dclare storage
             */
            $gallery_panel = DB::table('gallery_panel')
                ->where('id', $postdata['id_gallery_panel'])
                ->first();
            $date = $gallery_panel->date;
            $name_panel = strtolower(str_replace(' ', '', $gallery_panel->name));
            $directory = '';
            $directory .= ($date != '') ? $date . '/' : '';
            $directory .= ($name_panel != '') ? $name_panel : '';

            #destination
            $destination = 'gallery/' . $directory;
            $file_path = public_path($destination);

            #filename
            $filename = uniqid() . '.' . $extension;

            #upload file
            $file->move($file_path, $filename);

            #extractfile
            $zip = new \ZipArchive;
            $zip->open($file_path . '/' . $filename);
            $zip->extractTo($file_path);
            $zip->close();
            Eventy::RemoveFile($destination . '/' . $filename);

            #read directory and existing file
            $read_file = scandir($file_path);
            $data_old = DB::table('gallery_image')
                ->where('id_gallery_panel', $postdata['id_gallery_panel'])
                ->whereNull('deleted_at')
                ->pluck('file')
                ->toArray();
            $read_path = 'uploads/gallery/';
            $insert = [];
            foreach ($read_file as $key => $value) {
                if ($value == '.' || $value == '..') continue;
                if (strpos($value, 'DS_Store') !== false) continue;
                if (in_array($destination . '/' . $value, $data_old)) continue;
                if ($value == $filename) continue;
                if (strpos($value, '__MACOSX') !== false) {
                    Eventy::RemoveFile($value);
                    continue;
                }

                $extension = pathinfo($value, PATHINFO_EXTENSION);
                if (in_array(strtolower($extension), $list_image)) {
                    #image
                    $save['created_at'] = Eventy::now();
                    $save['id_gallery_panel'] = $postdata['id_gallery_panel'];
                    $save['file'] = $destination . '/' . $value;
                    $save['filename'] = basename($value);
                    $save['type'] = 'image';
                    $save['placeholder'] = $destination . '/' . $value;
                    $insert[] = $save;
                } elseif (in_array(strtolower($extension), $list_video)) {
                    #video
                    $save['created_at'] = Eventy::now();
                    $save['id_gallery_panel'] = $postdata['id_gallery_panel'];
                    $save['file'] = $destination . '/' . $value;
                    $save['filename'] = basename($value);
                    $save['type'] = 'image';
                    $save['placeholder'] = 'image/setting/video.png';
                    $insert[] = $save;
                } else {
                    Eventy::RemoveFile($value);
                }
            }
            DB::table('gallery_image')->insert($insert);
            CRUDBooster::redirectBack('Import data success', 'success');
        } else if ($extension == 'mp4' || $extension == 'MP4') {
            $type = 'video';
            $placeholder = 'image/setting/video.png';
        } else if (in_array($extension, $list_image)) {
            $type = 'image';
            $placeholder = $postdata['file'];
        } else {
            CRUDBooster::redirectBack('file format is invalid');
        }

        $postdata['filename'] = $filename;
        $postdata['type'] = $type;
        $postdata['placeholder'] = $placeholder;
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        #latest data
        $gallery_image = DB::table('gallery_image')
            ->where('id', $id)
            ->first();
        Eventy::RemoveFile($gallery_image->file);

        $gallery_panel = DB::table('gallery_panel')
            ->where('id', $gallery_image->id_gallery_panel)
            ->first();

        #declare Storage
        $date = $gallery_panel->date;
        $name_panel = strtolower(str_replace(' ', '', $gallery_panel->name));
        $directory = '';
        $directory .= ($date != '') ? $date . '/' : '';
        $directory .= ($name_panel != '') ? $name_panel : '';

        #destination
        $destination = 'gallery/' . $directory;
        $file_path = public_path($destination);

        #filename
        $file = Request::file('file');
        $extension = $file->getClientOriginalExtension();
        $filename = uniqid() . '.' . $extension;

        #upload file
        $file->move($file_path, $filename);

        $save['updated_at'] = Eventy::now();
        $save['file'] = $destination . '/' . $filename;
        $save['placeholder'] = ($gallery_image->type == 'image' ? $save['file'] : 'image/setting/video.png');
        DB::table('gallery_image')->where('id', $id)->update($save);
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here
        $gallery = DB::table('gallery_image')
            ->where('id', $id)
            ->first();

        Eventy::RemoveFile($gallery->file);
        DB::table('gallery_image')->where('id', $id)->delete();
    }


    //By the way, you can still create your own method in here... :)


}