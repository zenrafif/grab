<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiAboutController extends CoreController
{
    public function postIndex(){
        try{
            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['image'] = API::file(API::getSetting('about_event_image'));
            $response['content'] = API::getSetting('about_event_content');

            API::Log('Document', 'About : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Document', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}