<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class TestingController extends CoreController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function postNotification()
    {
        $validator['title'] = 'required';
        $validator['content'] = 'required';
        API::validator($validator);

        $title = Request::input('title');
        $content = Request::input('content');

        $save['created_at'] = API::now();
        $save['title'] = $title;
        $save['content'] = $content;
        $act = DB::table('notification')->insert($save);

        if ($act) {
            $data['id'] = $act;
            $data['title'] = $title;
            $data['content'] = $content;
            $data['type'] = 1;
            $data['created_at'] = date('d F Y, H:iA',strtotime(API::now()));
            $regid = DB::table('member_regid')->pluck('token');
            $notif = API::SendFCM($regid,$data);

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['notification'] = $notif;
        } else {
            $response['api_status'] = 0;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'Failed, Please try again';
        }

        API::Log('Testing', 'Notification : ' . json_encode($response));
        return response()->json($response);
    }

    public function postMessageToAdmin()
    {
        $validator['message'] = 'required';
        API::validator($validator);

        $save['created_at'] = API::now();
        $save['id_member'] = $this->member->id;
        $save['type'] = 'admin';
        $save['message'] = Request::input('message');
        $save['is_read'] = 0;
        $act = DB::table('message')->insertGetId($save);

        if ($act) {
            $data['id'] = $act;
            $data['title'] = $this->member->name;
            $data['content'] = $save['message'];
            $data['type'] = 2;
            $data['created_at'] = API::now();
            $regid = DB::table('member_regid')->where('id_member',$this->member->id)->pluck('token');
            $notif = API::SendFCM($regid,$data);

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['notification'] = $notif;
        } else {
            $response['api_status'] = 0;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'Failed, Please try again';
        }

        API::Log('Testing', 'Message To Admin : ' . json_encode($response));
        return response()->json($response);
    }

    public function postClearSurvey(){
        $save['deleted_at'] = API::now();
        $act = DB::table('survey_answer')->where('id_member',$this->member->id)->update($save);
        if ($act) {
            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'Success';
        } else {
            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'Failed';
        }

        API::Log('Testing', 'Clear Survey : ' . json_encode($response));
        return response()->json($response);
    }

    public function postClearFeedback(){
        $save['deleted_at'] = API::now();
        $act = DB::table('feedback')->where('id_member',$this->member->id)->update($save);
        if ($act) {
            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'Success';
        } else {
            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'Failed';
        }

        API::Log('Testing', 'Clear Feedback : ' . json_encode($response));
        return response()->json($response);
    }

    public function postClearQuiz(){
        $save['deleted_at'] = API::now();
        $act = DB::table('quiz_answer')->where('id_member',$this->member->id)->update($save);
        if ($act) {
            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'Success';
        } else {
            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'Failed';
        }

        API::Log('Testing', 'Clear Feedback : ' . json_encode($response));
        return response()->json($response);
    }
}
