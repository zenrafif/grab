<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use App\Helpers\Eventy;
use DB;
use Hash;
use Log;
use Request;

class ApiLoginController extends CoreController
{
    /**
     * SHOWING MEMBER DATA
     * - id
     * - image (validation file exist or not
     * - name
     * - email
     * - phone
     * - company
     * - departement
     * - division / job_title
     * - qrcode (using google qr generator)
     */

    public function postIndex()
    {
        try {
            $validator['username'] = 'required|string|email|min:1|max:255';
            $validator['password'] = 'required|min:1|max:255';
            $validator['regid'] = 'required|min:1|max:255';
            API::validator($validator);

            $username = Request::input('username');
            $password = Request::input('password');
            $regid = Request::input('regid');
            $platform = Request::input('platform');

            $member = DB::table('member')
                ->whereNull('deleted_at')
                ->where('email', $username)
                ->first();
            if ($member === null) {
                $response['api_status'] = 0;
                $response['code'] = API::ServerCode();
                $response['api_title'] = 'Login Failed';
                $response['api_message'] = 'Username not found';
            } else {
                if ($member->forgot_password == $password) {
                    $save['last_login'] = Eventy::now();
                    $save['updated_at'] = Eventy::now();
                    $save['password'] = Hash::make($password);
                    $save['forgot_password'] = null;
                    DB::table('member')->where('id', $member->id)->update($save);

                    $response['api_status'] = 1;
                    $response['code'] = API::ServerCode();
                    $response['api_title'] = '';
                    $response['api_message'] = 'success';
                    $response['id'] = $member->id;
                    $response['image'] = ($member->image) ? API::file($member->image) : '';
                    $response['name'] = ($member->name) ? $member->name : '';
                    $response['email'] = ($member->email) ? $member->email : '';
                    $response['phone'] = ($member->phone) ? $member->phone : '';
                    $response['id_number'] = ($member->id_number) ? $member->id_number : '';
                    $response['company'] = ($member->company) ? $member->company : '';
                    $response['departement'] = ($member->departement) ? $member->departement : '';
                    $response['qrcode'] = API::QRCode($member->code);

                    self::SaveRegid($member->id, $regid, $platform);
                } else {
                    if (!Hash::check($password, $member->password)) {
                        $response['api_status'] = 0;
                        $response['code'] = API::ServerCode();
                        $response['api_title'] = 'Login Failed';
                        $response['api_message'] = 'Your password is wrong';
                    } else {
                        $response['api_status'] = 1;
                        $response['code'] = API::ServerCode();
                        $response['api_title'] = '';
                        $response['api_message'] = 'success';
                        $response['id'] = $member->id;
                        $response['image'] = ($member->image) ? API::file($member->image) : '';
                        $response['name'] = ($member->name) ? $member->name : '';
                        $response['email'] = ($member->email) ? $member->email : '';
                        $response['phone'] = ($member->phone) ? $member->phone : '';
                        $response['id_number'] = ($member->id_number) ? $member->id_number : '';
                        $response['company'] = ($member->company) ? $member->company : '';
                        $response['departement'] = ($member->departement) ? $member->departement : '';
                        $response['qrcode'] = API::QRCode($member->code);

                        self::SaveRegid($member->id, $regid, $platform);
                    }
                }
            }

            API::Log('Login', 'Index : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Login', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function SaveRegid($id_member, $token, $platform)
    {
        try {
            $check = DB::table('member_regid')
                ->whereNull('deleted_at')
                ->where('token', $token)
                ->count();

            $save_last_login['updated_at'] = API::now();
            $save_last_login['last_login'] = API::now();
            DB::table('member')->where('id', $id_member)->update($save_last_login);

            if ($check) {
                $save['platform'] = $platform;
                $save['id_member'] = $id_member;
                $save['updated_at'] = API::now();
                DB::table('member_regid')->where('token', $token)->update($save);
            } else {
                $save['platform'] = $platform;
                $save['id_member'] = $id_member;
                $save['token'] = $token;
                $save['created_at'] = API::now();
                DB::table('member_regid')->insert($save);
            }
        } catch (\Exception $e) {
            $value['id_member'] = $id_member;
            $value['token'] = $token;
            $value['error'] = $e;
            API::Log('Login', 'Failed save regid : ' . $id_member . ' : ' . $token);
        }
    }
}