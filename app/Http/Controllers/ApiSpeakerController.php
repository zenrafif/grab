<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use File;
use Log;
use Request;

class ApiSpeakerController extends CoreController
{
    public function postIndex()
    {
        try {
            $data = DB::table('speaker')
                ->whereNull('deleted_at')
                ->orderBy('name', 'ASC')
                ->get();

            $item = [];
            foreach ($data as $row) {
                $push['id'] = $row->id;
                $push['image'] = API::file($row->image);
                $push['name'] = $row->name ? $row->name : '';
                $push['position'] = $row->title ? $row->title : '';
                $push['description'] = $row->description ? $row->description : '';

                $item[] = $push;
            }

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['detail'] = 'Yes';
            $response['item'] = $item;

            API::Log('Speaker', 'Index : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Speaker', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}