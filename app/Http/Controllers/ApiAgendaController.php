<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use File;
use Log;
use Request;

class ApiAgendaController extends CoreController
{
    public function postIndex()
    {
        try {
            #GET LIST DATE OF SCHEDULE
            $date = DB::table('agenda')
                ->whereNull('deleted_at')
                ->groupBy('date')
                ->orderBy('date', 'ASC')
                ->get();
            $item = [];
            $i = 1;
            foreach ($date as $row) {
                $list = [];
                #GET LIST AGENDA WHERE DATE IS $ROW
                $agenda = DB::table('agenda')
                    ->where('date', $row->date)
                    ->whereNull('deleted_at')
                    ->orderBy('time_start', 'ASC')
                    ->orderBy('time_end', 'ASC')
                    ->get();
                foreach ($agenda as $xrow) {
                    $xrest['id'] = $xrow->id;
                    $xrest['time_start'] = $xrow->time_start;
                    $xrest['time_end'] = $xrow->time_end;
                    $xrest['title'] = $xrow->title;
                    $xrest['location'] = $xrow->location;

                    $list[] = $xrest;
                }

                $rest['id'] = $i++;
                $rest['date'] = $row->date;
                $rest['list'] = $list;

                $item[] = $rest;
            }

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['item'] = $item;

            API::Log('Agenda', 'Index : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Agenda', 'Index Catch : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postDetail()
    {
        try {
            $validator['id_agenda'] = 'required|integer';
            API::validator($validator);

            $id_agenda = Request::input('id_agenda');

            $agenda = DB::table('agenda')
                ->where('id', $id_agenda)
                ->first();

            $speaker = DB::table('agenda_speaker')
                ->select('speaker.id', 'speaker.image', 'speaker.name', 'speaker.title', 'speaker.description')
                ->join('speaker', 'speaker.id', '=', 'agenda_speaker.id_speaker')
                ->where('agenda_speaker.id_agenda', $id_agenda)
                ->whereNull('agenda_speaker.deleted_at')
                ->whereNull('speaker.deleted_at')
                ->get();
            foreach ($speaker as $row) {
                $row->image = API::file($row->image);
            }

            $document = DB::table('agenda_document')
                ->select('document.id', 'document.file', 'document.filename', 'document.filesize', 'document.extension')
                ->join('document', 'document.id', '=', 'agenda_document.id_document')
                ->where('agenda_document.id_agenda', $id_agenda)
                ->whereNull('agenda_document.deleted_at')
                ->whereNull('document.deleted_at')
                ->orderBy('document.sort', 'ASC')
                ->get();
            $list_document = [];
            foreach ($document as $row) {
                $row->file = API::file($row->file);
                $row->filesize = API::formatBytes($row->filesize);

                if ($row->file) {
                    $rest_document['id'] = $row->id;
                    $rest_document['file'] = $row->file;
                    $rest_document['filename'] = $row->filename;
                    $rest_document['filesize'] = $row->filesize;
                    $rest_document['extension'] = $row->extension;

                    $list_document[] = $rest_document;
                }
            }

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['description'] = ($agenda->description == '' ? '' : $agenda->description);
            $response['speaker'] = $speaker;
            $response['document'] = $list_document;

            API::Log('Agenda', 'Detail : ' . Request::ip());
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Agenda', 'Detail Catch : ' . Request::ip());
            return response()->json($response);
        }
    }
}