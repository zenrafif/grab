<?php namespace App\Http\Controllers;

use function foo\func;
use Session;
use Request;
use DB;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use Config;
use App\Helpers\Eventy;

class AdminQnaController extends \crocodicstudio\crudbooster\controllers\CBController
{
    public function postAddMessages($id)
    {
        $qna = DB::table('qna')
            ->where('id', $id)
            ->first();
        $speaker = DB::table('speaker')
            ->where('id', $qna->id_speaker)
            ->first();
        $regid_android = DB::table('member_regid')
            ->where('id_member', $qna->id_member)
            ->where('platform', 'Android')
            ->pluck('token');
        $regid_ios = DB::table('member_regid')
            ->where('id_member', $qna->id_member)
            ->where('platform', 'IOS')
            ->pluck('token');
        $message = Request::input('message');

        $save['created_at'] = date('Y-m-d H:i:s');
        $save['id_qna'] = $id;
        $save['message'] = $message;
        $save['type'] = 'Admin';
        DB::table('qna_detail')->insert($save);

        $item['id'] = $id;
        $item['id_speaker'] = $qna->id_speaker;
        $item['message'] = $message;
        $item['position'] = 'Left';
        $item['datetime'] = date('H:i', strtotime($save['created_at']));

        $data['title'] = $speaker->name;
        $data['message'] = $message;
        $data['content'] = $message;
        $data['name'] = $speaker->name;
        $data['type'] = 4;
        $data['item'] = $item;
        $data['position'] = $speaker->title;
        CRUDBooster::sendFCM($regid_android, $data, 'Android');
        CRUDBooster::sendFCM($regid_ios, $data, 'IOS');

        CRUDBooster::redirectBack('Data has been added', 'success');
    }

    public function getDetail($id)
    {
        $path = CRUDBooster::mainpath('detail/'.$id);

        $save['is_read'] = 1;
        $save['updated_at'] = Eventy::now();
        DB::table('cms_notifications')
            ->where('id_cms_users',CRUDBooster::myId())
            ->where('is_read',0)
            ->where('url',$path)
            ->update($save);

        $message = DB::table('qna')
            ->where('id', $id)
            ->first();
        $member = DB::table('member')
            ->where('id', $message->id_member)
            ->first();
        $speaker = DB::table('speaker')
            ->where('id', $message->id_speaker)
            ->first();
        $admin_image = ($speaker->image == '' ? CRUDBooster::myPhoto() : asset($speaker->image));
        $admin_name = $speaker->name;
        $users_image = ($member->image == '' ? CRUDBooster::myPhoto() : asset($member->image));
        $users_name = $member->name;

        $messages = DB::table('qna_detail')
            ->where('id_qna', $message->id)
            ->orderBy('id', 'ASC')
            ->limit(100)
            ->get();
        foreach ($messages as $row) {
            if ($row->type == 'Admin') {
                $row->name = $admin_name;
                $row->image = $admin_image;
            } else {
                $row->name = $users_name;
                $row->image = $users_image;
            }
        }

        $arr = [];
        $arr['messages'] = $messages;
        return view('admin.qna', $arr);
    }

    public function cbInit()
    {
        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->table = "qna";
        $this->title_field = "id";
        $this->limit = 20;
        $this->orderby = "id,desc";
        $this->show_numbering = FALSE;
        $this->global_privilege = FALSE;
        $this->button_table_action = TRUE;
        $this->button_action_style = "button_icon";
        $this->button_add = FALSE;
        $this->button_delete = FALSE;
        $this->button_edit = FALSE;
        $this->button_detail = TRUE;
        $this->button_show = TRUE;
        $this->button_filter = FALSE;
        $this->button_export = FALSE;
        $this->button_import = FALSE;
        $this->button_bulk_action = FALSE;
        $this->sidebar_mode = "normal"; //normal,mini,collapse,collapse-mini
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = array("label" => "Speaker", "name" => "id_speaker", "join" => "speaker,name", "callback" => function ($row) {
            $url = CRUDBooster::adminPath('speaker/detail/' . $row->id_speaker);
            return '<a href="' . $url . '" target="_blank">' . $row->speaker_name . '</a>';
        });
        $this->col[] = array("label" => "Participant", "name" => "id_member", "join" => "member,name", "callback" => function ($row) {
            $url = CRUDBooster::adminPath('participant/detail/' . $row->id_member);
            return '<a href="' . $url . '" target="_blank">' . $row->member_name . '</a>';
        });
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $this->form = [];
        $this->form[] = ["label" => "Member", "name" => "id_member", "type" => "select2", "required" => TRUE, "validation" => "required|integer|min:0", "datatable" => "member,name"];
        $this->form[] = ["label" => "Speaker", "name" => "id_speaker", "type" => "select2", "required" => TRUE, "validation" => "required|integer|min:0", "datatable" => "speaker,name"];
        # END FORM DO NOT REMOVE THIS LINE

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $this->script_js = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here
        $detail = DB::table('qna_detail')
            ->whereNull('deleted_at')
            ->groupBy('id_qna')
            ->pluck('id_qna');

//        $query->join('qna_detail', 'qna_detail.id_qna', '=', 'qna.id');
//        $query->addSelect('qna_detail.message as message');
//        $query->where('qna_detail.type', 'Member');
        $query->whereIn('qna.id', $detail);
        $query->whereNull('member.deleted_at');
        $query->whereNull('speaker.deleted_at');
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}