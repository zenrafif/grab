<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiMessageToAdminController extends CoreController
{
    public function postIndex()
    {
        try {
            $validator['last_id'] = 'required|integer';
            API::validator($validator);

            $last_id = Request::input('last_id');

            $list_id = DB::table('message')
                ->where('id_member', $this->member->id)
                ->whereNull('deleted_at')
                ->where(function ($q) use ($last_id) {
                    if ($last_id > 0 && $last_id != '') {
                        $q->where('id', '<', $last_id);
                    }
                })
                ->limit(20)
                ->orderBy('id', 'DESC')
                ->pluck('id')
                ->toArray();

            $message = DB::table('message')
                ->select('id', 'created_at', 'type', 'message', 'is_read')
                ->whereIn('id', $list_id)
                ->orderBy('id', 'ASC')
                ->get();
            foreach ($message as $row) {
                $row->created_at = API::TimeAgo($row->created_at);
            }

            #MAKE READ MESSAGE
            $save['updated_at'] = API::now();
            $save['is_read'] = 1;
            DB::table('message')
                ->where('id_member', $this->member->id)
                ->where('type', 'admin')
                ->where('is_read', 0)
                ->update($save);

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['image'] = API::file(API::getSetting('about_event_image'));
            $response['name'] = 'Admin';
            $response['title'] = 'You can ask anything here';
            $response['last_id'] = (count($message) < 1 ? 0 : $message[0]->id);
            $response['item'] = $message;

            API::Log('Message To Admin', 'Index : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Message To Admin', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postSend()
    {
        try {
            $validator['message'] = 'required|string|min:1|max:255';
            API::validator($validator);

            $save['created_at'] = API::now();
            $save['id_member'] = $this->member->id;
            $save['type'] = 'member';
            $save['message'] = Request::input('message');
            $save['is_read'] = 0;
            $act = DB::table('message')->insert($save);

            if ($act) {
                $response['api_status'] = 1;
                $response['code'] = API::ServerCode();
                $response['api_title'] = '';
                $response['api_message'] = 'success';
            } else {
                $response['api_status'] = 0;
                $response['code'] = API::ServerCode();
                $response['api_title'] = '';
                $response['api_message'] = 'Failed, Please try again';
            }

            API::Log('Message To Admin', 'Send : ' . Request::ip());
            return response()->json($response);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Message To Admin', 'Send Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

}