<?php namespace App\Http\Controllers;

use App\Helpers\Eventy;
use Session;
use Request;
use DB;
use CRUDBooster;

class AdminSettingController extends \crocodicstudio\crudbooster\controllers\CBController
{

    public function cbInit()
    {
        $segment = Request::segment(3);
        $id = Request::segment(4);

        # START CONFIGURATION DO NOT REMOVE THIS LINE
        $this->table = "auth";
        $this->title_field = "id";
        $this->limit = 20;
        $this->orderby = "id,ASC";
        $this->show_numbering = FALSE;
        $this->global_privilege = FALSE;
        $this->button_table_action = TRUE;
        $this->button_action_style = "button_icon";
        $this->button_add = FALSE;
        $this->button_delete = FALSE;
        $this->button_edit = TRUE;
        $this->button_detail = TRUE;
        $this->button_show = TRUE;
        $this->button_filter = FALSE;
        $this->button_export = FALSE;
        $this->button_import = FALSE;
        $this->button_bulk_action = FALSE;
        $this->sidebar_mode = "normal"; //normal,mini,collapse,collapse-mini
        # END CONFIGURATION DO NOT REMOVE THIS LINE

        # START COLUMNS DO NOT REMOVE THIS LINE
        $this->col = [];
        $this->col[] = array("label" => "Type", "name" => "type");
        $this->col[] = array("label" => "Value", "name" => "value", "callback" => function ($row) {
            $list_id = [1, 2];
            $list_color = [3, 4, 5, 6];

            if (in_array($row->id, $list_id)) {
                if ($row->value != '') {
                    $url = asset($row->value);

                    return '<a data-lightbox="roadtrip" rel="group_{cms_users}" title="Photo: Super Admin" href="' . $url . '">
                        <img width="40px" height="40px" src="' . $url . '"></a>';
                }

            } elseif (in_array($row->id, $list_color)) {
                if ($row->value != '') {
                    return ' <div width="50px" height="10px" style="background-color: ' . $row->value . ';color: #FFF;
		            mix-blend-mode: darken;">' . $row->value . '</div>';
                }
            } else {
                return $row->value;
            }
        });
        # END COLUMNS DO NOT REMOVE THIS LINE

        # START FORM DO NOT REMOVE THIS LINE
        $list_id = [1, 2];
        $list_date = [8, 9];
        if (in_array($id, $list_id)) {
            $type = 'upload';
        } else if (in_array($id, $list_date)) {
            $type = 'date';
        } else {
            $type = 'text';
        }
        $this->form = [];
        $this->form[] = ["label" => "Type", "name" => "type", "type" => "text", "disabled" => TRUE, "validation" => "required|min:1|max:255"];
        $this->form[] = ["label" => "Value", "name" => "value", "type" => $type, "required" => TRUE, "validation" => "required"];
        # END FORM DO NOT REMOVE THIS LINE

        /*
        | ----------------------------------------------------------------------
        | Sub Module
        | ----------------------------------------------------------------------
        | @label          = Label of action
        | @path           = Path of sub module
        | @foreign_key 	  = foreign key of sub table/module
        | @button_color   = Bootstrap Class (primary,success,warning,danger)
        | @button_icon    = Font Awesome Class
        | @parent_columns = Sparate with comma, e.g : name,created_at
        |
        */
        $this->sub_module = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Action Button / Menu
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
        | @icon        = Font awesome class icon. e.g : fa fa-bars
        | @color 	   = Default is primary. (primary, warning, succecss, info)
        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
        |
        */
        $this->addaction = array();


        /*
        | ----------------------------------------------------------------------
        | Add More Button Selected
        | ----------------------------------------------------------------------
        | @label       = Label of action
        | @icon 	   = Icon from fontawesome
        | @name 	   = Name of button
        | Then about the action, you should code at actionButtonSelected method
        |
        */
        $this->button_selected = array();


        /*
        | ----------------------------------------------------------------------
        | Add alert message to this module at overheader
        | ----------------------------------------------------------------------
        | @message = Text of message
        | @type    = warning,success,danger,info
        |
        */
        $this->alert = array();


        /*
        | ----------------------------------------------------------------------
        | Add more button to header button
        | ----------------------------------------------------------------------
        | @label = Name of button
        | @url   = URL Target
        | @icon  = Icon from Awesome.
        |
        */
        $this->index_button = array();


        /*
        | ----------------------------------------------------------------------
        | Customize Table Row Color
        | ----------------------------------------------------------------------
        | @condition = If condition. You may use field alias. E.g : [id] == 1
        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
        |
        */
        $this->table_row_color = array();


        /*
        | ----------------------------------------------------------------------
        | You may use this bellow array to add statistic at dashboard
        | ----------------------------------------------------------------------
        | @label, @count, @icon, @color
        |
        */
        $this->index_statistic = array();


        /*
        | ----------------------------------------------------------------------
        | Add javascript at body
        | ----------------------------------------------------------------------
        | javascript code in the variable
        | $this->script_js = "function() { ... }";
        |
        */
        $list_id = [3, 4, 5, 6];
        if (in_array(Request::segment(4), $list_id)) {
            $js = '$(document).ready(function(){
                    $("#value").attr("readonly",true)
                    $("#value").colorpicker()
                })';
        } else {
            $js = '';
        }
        $this->script_js = '
	            
	        ' . $js;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code before index table
        | ----------------------------------------------------------------------
        | html code to display it before index table
        | $this->pre_index_html = "<p>test</p>";
        |
        */
        $this->pre_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include HTML Code after index table
        | ----------------------------------------------------------------------
        | html code to display it after index table
        | $this->post_index_html = "<p>test</p>";
        |
        */
        $this->post_index_html = null;


        /*
        | ----------------------------------------------------------------------
        | Include Javascript File
        | ----------------------------------------------------------------------
        | URL of your javascript each array
        | $this->load_js[] = asset("myfile.js");
        |
        */
        $this->load_js = array();
        $this->load_js[] = asset('vendor/crudbooster/assets/adminlte/plugins/colorpicker/bootstrap-colorpicker.min.js');


        /*
        | ----------------------------------------------------------------------
        | Add css style at body
        | ----------------------------------------------------------------------
        | css code in the variable
        | $this->style_css = ".style{....}";
        |
        */
        $this->style_css = NULL;


        /*
        | ----------------------------------------------------------------------
        | Include css File
        | ----------------------------------------------------------------------
        | URL of your css each array
        | $this->load_css[] = asset("myfile.css");
        |
        */
        $this->load_css = array();
        $this->load_css[] = asset('vendor/crudbooster/assets/adminlte/plugins/colorpicker/bootstrap-colorpicker.min.css');


    }


    /*
    | ----------------------------------------------------------------------
    | Hook for button selected
    | ----------------------------------------------------------------------
    | @id_selected = the id selected
    | @button_name = the name of button
    |
    */
    public function actionButtonSelected($id_selected, $button_name)
    {
        //Your code here

    }


    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate query of index result
    | ----------------------------------------------------------------------
    | @query = current sql query
    |
    */
    public function hook_query_index(&$query)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate row of index table html
    | ----------------------------------------------------------------------
    |
    */
    public function hook_row_index($column_index, &$column_value)
    {
        //Your code here
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before add data is execute
    | ----------------------------------------------------------------------
    | @arr
    |
    */
    public function hook_before_add(&$postdata)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after add public static function called
    | ----------------------------------------------------------------------
    | @id = last insert id
    |
    */
    public function hook_after_add($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for manipulate data input before update data is execute
    | ----------------------------------------------------------------------
    | @postdata = input post data
    | @id       = current id
    |
    */
    public function hook_before_edit(&$postdata, $id)
    {
        //Your code here
        $list_id = [1, 2];
        $list_date = [8, 9];
        if (in_array($id, $list_id) && Request::file('value')) {
            Eventy::RemoveFile($postdata['value']);

            $postdata['value'] = Eventy::UploadFile('value', 'setting');
        } elseif (in_array($id, $list_date)) {
            $save['updated_at'] = Eventy::now();
            $save['value'] = $postdata['value'];
            if($id == 8){
                DB::table('setting')->whereIn('type',['event_date_start','application_date_start'])->update($save);
            }else{
                DB::table('setting')->whereIn('type',['event_date_finish','application_date_end'])->update($save);
            }
        }
    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after edit public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_edit($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command before delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_before_delete($id)
    {
        //Your code here

    }

    /*
    | ----------------------------------------------------------------------
    | Hook for execute command after delete public static function called
    | ----------------------------------------------------------------------
    | @id       = current id
    |
    */
    public function hook_after_delete($id)
    {
        //Your code here

    }


    //By the way, you can still create your own method in here... :)


}