<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Request;
use Hash;

class ApiVoucherController extends CoreController
{
    public function postIndex()
    {
        try {
            $voucher = DB::table('voucher')
                ->select('voucher_member.id', 'voucher.image', 'voucher.title', 'voucher_member.qrcode')
                ->join('voucher_member', 'voucher_member.id_voucher', '=', 'voucher.id')
                ->where('voucher_member.id_member', $this->member->id)
                ->where('voucher_member.redeem', 'No')
                ->whereNull('voucher_member.deleted_at')
                ->whereNull('voucher.deleted_at')
                ->get();
            foreach ($voucher as $row) {
                $row->image = API::file($row->image);
                $row->qrcode = API::QRCode($row->qrcode);
            }

            if (count($voucher) > 0) {
                $rest['api_status'] = 1;
                $rest['api_title'] = 'Confirmation!';
                $rest['api_message'] = 'Show the QR Code to the committee for confirmation that you have exchanged the voucher';
                $rest['item'] = $voucher;
            } else {
                $rest['api_status'] = 0;
                $rest['api_title'] = 'Empty';
                $rest['api_message'] = 'You don\'t have voucher';
                $rest['item'] = $voucher;
            }

            API::Log('Vocher', 'Index : ' . Request::ip());
            return response()->json($rest);
        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Vocher', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }
}