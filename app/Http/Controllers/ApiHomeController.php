<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use DB;
use Log;
use Request;

class ApiHomeController extends CoreController
{
    public function postIndex()
    {
        try {
            #slider
            $slider = DB::table('slider')
                ->select('id', 'image')
                ->whereNull('deleted_at')
                ->get();
            foreach ($slider as $row) {
                $file = API::file($row->image);
                if ($file == '') continue;

                $row->image = $file;
            }

            #apps_name
            $auth = DB::table('auth')
                ->where('type', 'event_name')
                ->first();

            #notification_badge
            $notification_id = DB::table('notification_member')
                ->where('id_member', $this->member->id)
                ->whereNull('deleted_at')
                ->pluck('id_notification');
            $notif_badge = DB::table('notification')
                ->whereNotIn('id', $notification_id)
                ->where('created_at', '>', $this->member->created_at)
                ->where('is_send', 1)
                ->whereNull('deleted_at')
                ->count();

            #message_to_admin_badge
            $message_badge = DB::table('message')
                ->where('id_member', $this->member->id)
                ->where('type', 'admin')
                ->where('is_read', 0)
                ->whereNull('deleted_at')
                ->count();

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['apps_name'] = $auth->value;
            $response['notification_badge'] = $notif_badge;
            $response['message_to_admin_badge'] = $message_badge;
            $response['slider'] = $slider;
            $response['module'] = API::ListModule();
            API::Log('Home', 'Index : ' . json_encode($response));
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Home', 'Index Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postNotificationBadge()
    {
        try {
            /**
             * TRIGGER @ 1 MINUTES in page HOME
             */
            #notification_badge
            $notification_id = DB::table('notification_member')
                ->where('id_member', $this->member->id)
                ->whereNull('deleted_at')
                ->pluck('id_notification');
            $notif_badge = DB::table('notification')
                ->whereNotIn('id', $notification_id)
                ->where('created_at', '>', $this->member->created_at)
                ->whereNull('deleted_at')
                ->where('is_send', 1)
                ->count();

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['notification_badge'] = $notif_badge;
            API::Log('Home', 'Notification Badge : ' . Request::ip());
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Home', 'Notification Badge Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postMessageToAdminBadge()
    {
        try {
            /**
             * TRIGGER @ 1 MINUTES in page HOME
             */
            #message_to_admin_badge
            $message_badge = DB::table('message')
                ->where('id_member', $this->member->id)
                ->where('type', 'admin')
                ->where('is_read', 0)
                ->whereNull('deleted_at')
                ->count();

            $response['api_status'] = 1;
            $response['code'] = API::ServerCode();
            $response['api_title'] = '';
            $response['api_message'] = 'success';
            $response['message_to_admin_badge'] = $message_badge;
            API::Log('Home', 'Message To Admin Badge : ' . Request::ip());
            return response()->json($response);

        } catch (\Exception $e) {
            $response = API::failed($e->getMessage());
            API::Log('Home', 'Message To Admin Badge Exception : ' . Request::ip());
            return response()->json($response);
        }
    }

    public function postRefreshModule()
    {
        $response['api_status'] = 1;
        $response['code'] = API::ServerCode();
        $response['api_title'] = '';
        $response['api_message'] = 'success';
        $response['module'] = API::ListModule();
        API::Log('Home', 'Refresh Module : ' . Request::ip());
        return response()->json($response);
    }

    public function postRefreshSlider()
    {
        $slider = DB::table('slider')
            ->select('id', 'image')
            ->whereNull('deleted_at')
            ->get();
        foreach ($slider as $row) {
            $file = API::file($row->image);
            if ($file == '') continue;

            $row->image = $file;
        }

        $response['api_status'] = 1;
        $response['code'] = API::ServerCode();
        $response['api_title'] = '';
        $response['api_message'] = 'success';
        $response['slider'] = $slider;
        API::Log('Home', 'Refresh Slider : ' . Request::ip());
        return response()->json($response);
    }
}