<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
use DB;
use App\Helpers\Eventy;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BackupDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eventy:backaupdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup the database';

    protected $process;

    protected $backup_path;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $dbname = config('database.connections.mysql.database') . '_' . $date . '.sql';
        $path = 'uploads/backups';
        $this->backup_path = storage_path('app/' . $path . '/' . $dbname);
        Storage::makeDirectory($path);

        $command = sprintf(
            'mysqldump -u%s -p%s %s > %s',
            config('database.connections.mysql.username'),
            config('database.connections.mysql.password'),
            config('database.connections.mysql.database'),
            $this->backup_path
        );

        $this->process = new Process($command);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d');
        $check = DB::table('daily_backup')
            ->where('date', $date)
            ->orderBy('created_at', 'DESC')
            ->first();
        $run = false;
        if (!$check) {
            $run = true;
            $save['created_at'] = Eventy::now();
            $save['date'] = $date;
            DB::table('daily_backup')->insert($save);
        } else {
            $hour = date('H', strtotime($check->created_at));
            $hour_now = date('H', strtotime(Eventy::now()));
            if ($hour != $hour_now) {
                $run = true;
            }

            $save['created_at'] = Eventy::now();
            $save['date'] = $date;
            DB::table('daily_backup')->insert($save);
        }

        if ($run) {
            try {
                $this->process->mustRun();

                $this->info('The backup has been proceed successfully.');
            } catch (ProcessFailedException $exception) {
                $command = sprintf(
                    'mysqldump --column-statistics=0 -u%s -p%s %s > %s',
                    config('database.connections.mysql.username'),
                    config('database.connections.mysql.password'),
                    config('database.connections.mysql.database'),
                    $this->backup_path
                );
                $this->process = new Process($command);

                $this->process->mustRun();
                $this->info('Data has been exported but you have an error : ' . $exception->getMessage());

                //$this->error($exception->getMessage());
            }
        }
    }
}
