<?php

namespace App\Helpers;

use Illuminate\Support\Facades\App;
use DB;
use Hash;
use Log;
use Mail;
use Request;
use Route;
use Storage;
use Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

#LOGER
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class API
{
    public static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public static function Seconds($time_start, $time_end)
    {
        $mili_start = explode('.', $time_start)[1];
        $mili_end = explode('.', $time_end)[1];

        $number_start = (float)strtotime($time_start) . '.' . $mili_start;
        $number_end = (float)strtotime($time_end) . '.' . $mili_end;
        $second = $number_end - $number_start;
        if ($second < 0) {
            $second = $number_start - $number_end;
        }

        return $second;
    }

    public static function addDay($format, $date, $day)
    {
        return date($format, strtotime($date . ' ' . $day));
    }

    public static function getSetting($type)
    {
        $setting = DB::table('setting')
            ->where('type', $type)
            ->whereNull('deleted_at')
            ->first();

        if (empty($setting)) {
            return '';
        } else {
            return $setting->value;
        }
    }

    public static function formatBytes($bytes, $precision = 2)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public static function ListModule()
    {
        $module = DB::table('module')
            ->select('id', 'type', 'code', 'name', 'message as message_title', 'message as message_content', 'status')
            ->whereNull('deleted_at')
            ->where('admin_status', 'Yes')
            ->orderby('sorting', 'ASC')
            ->get();
        $message_title = API::getSetting('application_name');
        foreach ($module as $row) {
            $row->message_title = $message_title;
            $row->message_content = ($row->message_content == '' ? '' : $row->message_content);
        }

        return $module;
    }

    public static function now()
    {
        $date = date('Y-m-d H:i:s');
        return $date;
    }

    public static function TimeAgo($datetime, $full = false)
    {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second'
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        $time = implode(', ', $string);
        $ext = explode(' ', $time);

        if (isset($ext[1]) && ($ext[1] == 'minutes' || $ext[1] == 'seconds' || $ext[1] == 'hours')) {
            return date('H:i', strtotime($datetime));
        } else {
            return $string ? $time : date('H:i', strtotime($datetime));
        }
    }

    public static function RandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function SendFCM($regID = [], $data)
    {
        if (!$data['title'] || !$data['content']) return 'title , content null !';

        $apikey = 'AAAAo2UF2lY:APA91bFnvNWog4_hvRDQj83yGm89JrlMHbYgPsOG9RT6zpTcl2YIuqkymLJMd1G-y1RBSMP6nb8ujxe6NVAIL7oAZzByNMIDiYWal-ts9f-J6QPBKEKNcTZjxjxM1ECutvWTMZClNxp7';//'AIzaSyBTT9FF6yhkCcq5Zxhgd5araAaNBA3BS68';
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'registration_ids' => $regID,
            'data' => $data,
            'content_available' => true,
            'notification' => array(
                'sound' => 'default',
                'badge' => 0,
                'title' => trim(strip_tags($data['title'])),
                'body' => trim(strip_tags($data['content']))
            ),
            'priority' => 'high'
        );
        $headers = array(
            'Authorization:key=' . $apikey,
            'Content-Type:application/json'
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $chresult = curl_exec($ch);
        curl_close($ch);
        return $chresult;
    }

    public static function EliminateNull($arr)
    {
        $data = [];
        foreach ($arr as $key => $value) {
            $data[$key] = ($value == '' && $value !== 0 ? '' : $value);
        }
        return $data;
    }

    public static function BaseURL($code)
    {
        $domain = 'http://api.eventy.id/';
        $base = $domain . $code . '/public';

        return $base;
    }

    public static function QRCode($kode)
    {
        // $base = 'http://chart.googleapis.com/chart?chs=300x300&chld=L|1&cht=qr&chl=';
        $base = 'http://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=';

        return $base . $kode;
    }

    public static function file($path = null)
    {
        $check_public = base_path('public/' . $path);
        $check_storage = storage_path('app/' . $path);

        if ($path == null) {
            return '';
        } elseif (file_exists($check_public)) {
            return url($path);
        } elseif (file_exists($check_storage)) {
            return url($path);
        } else {
            return '';
        }
    }

    public static function RemoveFile($path)
    {
        if ($path != '') {
            $file_public = base_path('public/' . $path);
            $file_storage = storage_path('app/' . $path);


            if (file_exists($file_public)) {
                if (!unlink($file_public)) {
                    return false;
                } else {
                    return true;
                }
            } elseif (file_exists($file_storage)) {
                if (!unlink($file_storage)) {
                    return false;
                } else {
                    return true;
                }

            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static function uploadBase64($file, $extension = 'jpg', $directory_path = 'image_profile', $id = '')
    {
        $path = storage_path('app/uploads/' . $directory_path);
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $decode = base64_decode($file);
        $filename = ($id != '' ? $id . '-' : '') . strtotime(date('Y-m-d H:i:s')) . "." . $extension;
        $image_path = 'uploads/' . $directory_path . '/' . $filename;
        $upload_path = storage_path('app/' . $image_path);

        if (file_put_contents($upload_path, $decode)) {
            $url = url($image_path);
            try {
                if (!file($url)) {
                    return null;
                } else {
                    return $image_path;
                }
            } catch (\Exception $e) {
                API::RemoveFile($image_path);
                return null;
            }
        } else {
            return null;
        }
    }

    public static function Hari($date)
    {
        $day = date('D', strtotime($date));
        $dayList = [
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        ];

        return $dayList[$day];
    }

    public static function Bulan($date)
    {
        $hari = date('d', strtotime($date));
        $bulan = date('m', strtotime($date));
        $tahun = date('Y', strtotime($date));

        $month = [
            '01' => 'Januari',
            '02' => 'Febuari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];

        return $hari . ' ' . $month[$bulan] . ' ' . $tahun;
    }

    public static function failed($error)
    {
        $response['api_status'] = 0;
        $response['api_title'] = 'Something went wrong';
        $response['api_message'] = 'Please try again';
        $response['error'] = $error;
        $response['code'] = API::ServerCode();

        return $response;
    }

    public static function getInformationFile($file)
    {
        $path = base_path('public/' . $file);
        $filesize = filesize($path);
        $realsize = API::formatBytes($filesize);
        $info = pathinfo($path);
        $extension = (isset($info['extension']) ? $info['extension'] : '');
        $filename = ucwords(str_replace('.' . $extension, '', str_replace('_', ' ', basename($path))));
    }

    public static function Validator($data = [])
    {
        $validator = Validator::make(Request::all(), $data);
        if ($validator->fails()) {
            $result = array();
            $message = $validator->errors();
            $result['api_status'] = 0;
            $result['api_title'] = 'Request is not valid';
            $result['api_message'] = $message->all(':message')[0];
            $res = response()->json($result);
            $res->send();
            exit;
        }
    }

    public static function CheckKey($encryption, $time)
    {
        $select = DB::table('setting')
            ->where('type', 'api-key')
            ->first();
        $key = $select->value;

        return (md5($key . $time) == $encryption ? TRUE : FALSE);
    }

    public static function ServerCode()
    {
        return http_response_code();
    }

    public static function Log($filename, $content)
    {
        $all = Request::all();
        foreach ($all as $key => $value) {
            if (strlen($value) > 255) continue;
            $data[$key] = $value;
        }
        $data['ip'] = Request::ip();//$_SERVER['REMOTE_ADDR'];
        $data['code'] = API::ServerCode();

        $log = new Logger($filename);
        $log->pushHandler(new StreamHandler(base_path() . '/storage/logs/' . $filename . '.log', Logger::INFO));
        $log->info($content, $data);
    }

    public static function routeController($prefix, $controller, $namespace = null)
    {
        $prefix = trim($prefix, '/') . '/';
        $namespace = ($namespace) ?: 'App\Http\Controllers';

        try {
            Route::post($prefix, ['uses' => $controller . '@postIndex', 'as' => $controller . 'PostIndex']);
            $controller_class = new \ReflectionClass($namespace . '\\' . $controller);
            $controller_methods = $controller_class->getMethods(\ReflectionMethod::IS_PUBLIC);

            foreach ($controller_methods as $method) {
                if ($method->class != 'Illuminate\Routing\Controller' && $method->name != 'getIndex') {
                    if (substr($method->name, 0, 3) == 'get') {
                        $method_name = substr($method->name, 3);
                        $slug = array_filter(preg_split('/(?=[A-Z])/', $method_name));
                        $slug = strtolower(implode('-', $slug));
                        $slug = ($slug == 'index') ? '' : $slug;

                        Route::get($prefix . $slug, ['uses' => $controller . '@' . $method->name, 'as' => $controller . 'Get' . $method_name]);
                    } elseif (substr($method->name, 0, 4) == 'post') {
                        $method_name = substr($method->name, 4);
                        $slug = array_filter(preg_split('/(?=[A-Z])/', $method_name));
                        Route::post($prefix . strtolower(implode('-', $slug)), [
                            'uses' => $controller . '@' . $method->name,
                            'as' => $controller . 'Post' . $method_name,
                        ]);
                    }
                }
            }
        } catch (\Exception $e) {

        }
    }
}