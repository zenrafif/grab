<?php

namespace App\Helpers;

use App;
use Config;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use DB;
use File;
use Hash;
use Log;
use Mail;
use Request;
use Route;
use Storage;
use Validator;

#LOGER
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Eventy
{
    /**
     * zip directory
     */
    public static function ZipDirectory($filename, $path, $type = 'storage_path')
    {
        $rootPath = ($type == 'public_path') ? public_path($path) : storage_path('app/uploads/' . $path);
        $directory = storage_path('app/uploads/downloads');
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        /**
         * validate if not empty storage
         */
        $read_file = scandir($rootPath);
        $zip = false;
        foreach ($read_file as $key => $value) {
            if ($value == '.' || $value == '..') continue;
            if (strpos($value, 'DS_Store') !== false) continue;
            $zip = true;
        }

        if($zip){
            // Initialize archive object
            $zip = new \ZipArchive();
            $zip->open($directory . '/' . $filename . '.zip', \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

            /** @var SplFileInfo[] $files */
            $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($rootPath),
                \RecursiveIteratorIterator::LEAVES_ONLY
            );

            foreach ($files as $name => $file) {
                // Skip directories (they would be added automatically)
                if (!$file->isDir()) {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);

                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }

            // Zip archive will be created only after closing object
            $zip->close();
        }
    }

    /**
     * send notif trigger
     */
    public static function SendNotification()
    {
        $now = Eventy::now();
        $list_notif = DB::table('notification')
            ->whereNotNull('send_at')
            ->where('send_at', '<', $now)
            ->where('is_send', 0)
            ->whereNull('deleted_at')
            ->get();
        foreach ($list_notif as $row) {
            $data['id'] = $row->id;
            $data['title'] = $row->title;
            $data['body'] = $row->content;
            $data['content'] = $row->content;
            $data['type'] = 1;
            $data['created_at'] = date('d F Y, H:iA', strtotime(Eventy::now()));
            $regid_android = DB::table('member_regid')->where('platform', 'Android')->pluck('token');
            $regid_ios = DB::table('member_regid')->where('platform', 'IOS')->pluck('token');

            Eventy::sendFCM($regid_android, $data, 'Android');
            Eventy::sendFCM($regid_ios, $data, 'IOS');

            $save['updated_at'] = Eventy::now();
            $save['is_send'] = 1;
            DB::table('notification')->where('id', $row->id)->update($save);
        }
    }

    /**
     * SET QR CODE URL
     */
    public static function QRCode($kode)
    {
        // $base = 'http://chart.googleapis.com/chart?chs=300x300&chld=L|1&cht=qr&chl=';
        $base = 'http://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=';

        return $base . $kode;
    }

    /**
     * Cek file if exist or not
     */
    public static function file($path = null)
    {
        $check_public = base_path('public/' . $path);
        $check_storage = storage_path('app/' . $path);

        if ($path == null) {
            return '';
        } elseif (file_exists($check_public)) {
            return url($path);
        } elseif (file_exists($check_storage)) {
            return url($path);
        } else {
            return '';
        }
    }

    /**
     * CRONJOB SET EMAIL
     */
    public static function sendEmailQueue()
    {
        $email = DB::table('cms_email_queues')
            ->where('is_sent', 0)
            ->limit(10)
            ->get();

        foreach ($email as $row) {
            $act = CRUDBooster::sendEmailQueue($row);

            /**
             * update queue is sent
             */
            if ($act) {
                $save['updated_at'] = Eventy::now();
                $save['send_at'] = Eventy::now();
                $save['is_sent'] = 1;

                DB::table('cms_email_queues')->where('id', $row->id)->update($save);
            }
        }
    }

    /**
     * SEND NOTIFICATION
     */
    public static function SendFCM($regID = [], $data, $platform)
    {
        if (!$data['title'] || !$data['content']) return 'title , content null !';

        $apikey = 'AAAAo2UF2lY:APA91bFnvNWog4_hvRDQj83yGm89JrlMHbYgPsOG9RT6zpTcl2YIuqkymLJMd1G-y1RBSMP6nb8ujxe6NVAIL7oAZzByNMIDiYWal-ts9f-J6QPBKEKNcTZjxjxM1ECutvWTMZClNxp7';//'AIzaSyBTT9FF6yhkCcq5Zxhgd5araAaNBA3BS68';
        $url = 'https://fcm.googleapis.com/fcm/send';

        if ($platform == 'IOS') {
            $fields = array(
                'registration_ids' => $regID,
                'data' => $data,
                'content_available' => true,
                'notification' => array(
                    'sound' => 'default',
                    'badge' => 0,
                    'title' => trim(strip_tags($data['title'])),
                    'body' => trim(strip_tags($data['content']))
                ),
                'priority' => 'high'
            );
            $headers = array(
                'Authorization:key=' . $apikey,
                'Content-Type:application/json'
            );
        } else {
            $fields = array(
                'registration_ids' => $regID,
                'data' => $data,
                'priority' => 'high'
            );
            $headers = array(
                'Authorization:key=' . $apikey,
                'Content-Type:application/json'
            );
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $chresult = curl_exec($ch);
        curl_close($ch);
        return $chresult;
    }

    /**
     * List date from calendar
     */
    public static function getListDate()
    {
        $begin = new \DateTime(Eventy::getSetting('application_date_start'));
        $end = new \DateTime(date('Y-m-d', strtotime(Eventy::getSetting('application_date_end') . '+1 Day')));
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);

        $list_date = [];
        foreach ($period as $dt) {
            $list_date[] = $dt->format("Y-m-d");
        }

        return $list_date;
    }

    /**
     * get data auth
     */
    public static function getSetting($type)
    {
        $setting = DB::table('setting')
            ->where('type', $type)
            ->first();

        if (empty($setting)) {
            return '';
        } else {
            return $setting->value;
        }
    }

    /**
     * datetime now
     */
    public static function now()
    {
        $date = date('Y-m-d H:i:s');
        return $date;
    }

    /**
     * Remove file from public path or storage
     */
    public static function RemoveFile($path)
    {
        if ($path != '') {
            $file_public = public_path($path);
            $file_storage = storage_path('app/' . $path);

            if (file_exists($file_public)) {
                if (!unlink($file_public)) {
                    return false;
                } else {
                    return true;
                }
            } elseif (file_exists($file_storage)) {
                if (!unlink($file_storage)) {
                    return false;
                } else {
                    return true;
                }

            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * Upload File
     */
    public static function UploadFile($name, $type, $id = '', $path = '')
    {
        if (Request::hasFile($name)) {
            switch ($type) {
                case 'participant':
                    $directory = 'image_profile';
                    break;

                case 'speaker':
                    $directory = 'image_speaker';
                    break;

                case 'slider':
                    $directory = 'banner';
                    break;

                case 'setting':
                    $directory = 'setting';
                    break;

                case 'maps':
                    $directory = 'maps';
                    break;

                case 'gallery':
                    $directory = 'gallery';
                    break;

                case 'document':
                    $directory = 'document';
                    break;

                case 'export':
                    $directory = 'document';
                    break;

                default:
                    $directory = date('Y-m');
                    break;
            }

            $file = Request::file($name);
            $ext = $file->getClientOriginalExtension();
            $file_path = 'uploads/' . $directory . ($path != '' ? '/' . $path : '');

            //Create Directory & Generate filename
            Storage::makeDirectory($file_path);
            $filename = ($id != '' ? $id . '-' : '') . md5(str_random(5)) . '.' . $ext;

            if (Storage::putFileAs($file_path, $file, $filename)) {
                return $file_path . '/' . $filename;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * File Size to bytes
     */
    public static function formatBytes($bytes, $precision = 2)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    /**
     * RETURN FOR MEMBER QR CODE
     */
    public static function Code()
    {
        $code = Eventy::RandomCode();
        $check = DB::table('member')
            ->where('code', $code)
            ->count();
        if ($check > 0) {
            $code = Eventy::Code();
        }

        return $code;
    }

    /**
     * SET RANDOM CODE
     */
    public static function RandomCode($capital = true, $length = 6, $not_in = [])
    {
        $code = str_random($length);
        if ($capital) {
            $code = strtoupper($code);
        }

        if (in_array($code, $not_in)) {
            $code = Eventy::RandomCode();
        }

        return $code;
    }

    /**
     * COPY FILE FROM STORAGE AND PUT IN API PATH
     */
    public static function CopyFile($path)
    {
        if ($path == '') {
            return null;
        } else {
            $base_name = basename($path);
            $directory_name = str_replace($base_name, '', $path);
            $api_directory = base_path(Config::get('eventy.base_api_directory'));
            if (!file_exists($api_directory . $directory_name)) {
                mkdir($api_directory . $directory_name, 0777, true);
            }

            $storage = storage_path('app/' . $path);
            $public = storage_path($path);
            $api_path = $api_directory . $path;

            if (file_exists($storage)) {
                File::copy($storage, $api_path);
            } elseif (file_exists($public)) {
                File::copy($public, $api_path);
            } else {
                return null;
            }
        }
    }
}