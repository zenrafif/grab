<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin');
});

API::routeController('api_core/auth','ApiAuthController');
API::routeController('api_core/login','ApiLoginController');
API::routeController('api_core/home','ApiHomeController');
API::routeController('api_core/notification','ApiNotificationController');
API::routeController('api_core/profile','ApiProfileController');
API::routeController('api_core/agenda','ApiAgendaController');
API::routeController('api_core/maps','ApiMapsController');
API::routeController('api_core/gallery','ApiGalleryController');
API::routeController('api_core/documents','ApiDocumentController');
API::routeController('api_core/about','ApiAboutController');
API::routeController('api_core/committee','ApiCommitteeController');
API::routeController('api_core/message-to-admin','ApiMessageToAdminController');
API::routeController('api_core/survey','ApiSurveyController');
API::routeController('api_core/feedback','ApiFeedbackController');
API::routeController('api_core/quiz','ApiQuizController');
API::routeController('api_core/speaker','ApiSpeakerController');
API::routeController('api_core/qna','ApiQnaController');
API::routeController('api_core/voucher','ApiVoucherController');
API::routeController('api_core/scan','ApiScanController');

API::routeController('apix/testing','ApiTestingController');